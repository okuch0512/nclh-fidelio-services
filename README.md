Nclh fidelio ws service
=======================

## How to Run

```
~> sbt run
```
open http://localhost:9000/ at your browser

## REST API


###  Guest Picture

GET  /guest-picture/:folioId

```
curl -X GET 'http://localhost:9000/guest-picture/4'     

{"picture":"786875453237890345678908765"}⏎
```

###  Retrieve Guest

Get client by "clientId"  (by default)

GET  /client/:clientId

```
 ~> curl -X GET 'http://localhost:9000/guest/4'

{
  "nclClientId": 12,
  "name": {
    "givenName": "GivenName",
    "namePrefix": "NamePrefix",
    "nameTitle": "NameTitle",
    "middleName": "MiddleName",
    "surname": "Surname",
    "surnamePrefix": "SurnamePrefix",
    "nameSuffix": "NameSuffix"
  },
  "birthDate": 1565027419803,
  "nationality": "US",
  "gender": "male",
  "email": "some@some.io",
  "phone": "8900787977",
  "address": {
    "street": "street",
    "street2": "street2",
    "city": "city",
    "zip": "zip",
    "state": "state",
    "country": "country"
  },
  "latitudesLevel": "55",
  "fidelioAcctId": 6786,
  "reservationId": 879,
  "cabinNumber": "87979",
  "deck": "deck",
  "reservationStatus": "status",
  "minor": false,
  "embarkDate": 824238000000,
  "disembarkDate": 824238000000,
  "balance": 899879,
  "picture": "pic"
}
```

Or explicitly pass type as parameter ('clientId' or 'cardNumber')

```
curl -X GET 'http://localhost:9000/guest/4?type=clientId' | jq
```

```
curl -X GET 'http://localhost:9000/guest/4?type=cardNumber' | jq
```

###  Retrieve Reservation

GET  /reservation/:id

```
~> curl -X GET 'http://localhost:9000/reservation/4'
```

Or explicitly pass type as parameter ('reservationId' or 'cabinNumber')

```
curl -X GET 'http://localhost:9000/client/4?type=reservationId' | jq
```

```
curl -X GET 'http://localhost:9000/client/4?type=cabinNumber' | jq
```                                                 

```
{
  "id": 879,
  "clients": [
    {
      "nclClientId": 12,
      "name": {
        "givenName": "GivenName",
        "namePrefix": null,
        "nameTitle": "Surname",
        "middleName": null,
        "surname": "NamePrefix",
        "surnamePrefix": null,
        "nameSuffix": null
      },
      "birthDate": 980985600000,
      "nationality": "US",
      "gender": "male",
      "email": "some@some.io",
      "phone": "8900787977",
      "address": {
        "street": "street",
        "street2": "street2",
        "city": "city",
        "zip": "zip",
        "state": "state",
        "country": "country"
      },
      "latitudesLevel": "55",
      "fidelioAcctId": 6786,
      "reservationId": 879,
      "cabinNumber": "87979",
      "deck": "deck",
      "reservationStatus": "status",
      "minor": false,
      "embarkDate": -59136566400000,
      "disembarkDate": -59136566400000,
      "balance": 899879,
      "picture": "pic"
    }
  ]
}
```

###  Retrieve Packages

```
GET /packages/:reservationId/:transactionId
```                                        

```
curl -X GET 'http://localhost:9000/packages/1/4' | jq

{
  "reservation": {
    "id": 879,
    "clients": [
      {
        "nclClientId": 12,
        "name": {
          "givenName": "GivenName",
          "namePrefix": null,
          "nameTitle": "Surname",
          "middleName": null,
          "surname": "NamePrefix",
          "surnamePrefix": null,
          "nameSuffix": null
        },
        "birthDate": 980985600000,
        "nationality": "US",
        "gender": "male",
        "email": "some@some.io",
        "phone": "8900787977",
        "address": {
          "street": "street",
          "street2": "street2",
          "city": "city",
          "zip": "zip",
          "state": "state",
          "country": "country"
        },
        "latitudesLevel": "55",
        "fidelioAcctId": 6786,
        "reservationId": 879,
        "cabinNumber": "87979",
        "deck": "deck",
        "reservationStatus": "status",
        "minor": false,
        "embarkDate": -59136566400000,
        "disembarkDate": -59136566400000,
        "balance": 899879,
        "picture": "pic"
      }
    ]
  },
  "cruise": {
    "id": 111,
    "shipName": "shipName",
    "cruiseName": "cruiseName",
    "itineraryId": 22,
    "onboardCurrency": "onboardCurrency",
    "startDate": -59136566400000,
    "endDate": -59136566400000
  },
  "itinerary": {
    "items": []
  },
  "reservationId": "1",
  "transactionId": "4",
  "eventPackages": [
    {
      "package": {
        "id": "3",
        "typeCode": 4,
        "type": "ENTERTAINMENT",
        "name": "eventName",
        "code": "eventN",
        "comments": "eventName",
        "dataRange": {
          "packageStartDate": "0096-01-16T05:00:00.000+05:00",
          "packageEndDate": "0096-01-16T05:00:00.000+05:00"
        },
        "price": [
          {
            "guest": {
              "guestRefNumber": "1",
              "packagePrice": {
                "amount": "133",
                "currencyCode": "USD"
              }
            }
          }
        ],
        "location": {
          "from": {
            "code": "eve",
            "type": "SHIP"
          },
          "to": {
            "code": "",
            "type": ""
          }
        }
      }
    }
  ],
  "excursionPackages": [
    {
      "package": {
        "id": "111",
        "packageTypeCode": "4",
        "type": "SHORE EXCURSION",
        "name": "excursionName",
        "code": "RTBN03",
        "comments": "excursionName",
        "dateRange": {
          "packageStartDate": "0096-01-16T05:00:00.000+05:00",
          "packageEndData": "0096-01-16T05:00:00.000+05:00"
        },
        "price": [
          {
            "guest": {
              "guestRefNumber": "1",
              "packagePrice": {
                "amount": "21323",
                "currencyCode": "USD"
              }
            }
          }
        ],
        "location": {
          "from": {
            "code": "RTB",
            "type": "PORT",
            "name": ""
          },
          "to": {
            "code": "RTB",
            "type": "PORT",
            "name": ""
          }
        }
      }
    }
  ],
  "excFolios": [
    {
      "items": [
        {
          "id": 111,
          "excursionNumber": "RTBX0312",
          "excursionName": "excursionName",
          "startDate": -59136566400000,
          "endDate": -59136566400000,
          "bookingValue": 213.23,
          "numTicketsBooked": 10,
          "fidelioAcctId": 6786
        }
      ]
    }
  ],
  "excursions": []
}
```       

###   Retrieve Available Shorex Packages

GET /packages-shorex/:reservationId/:transactionId/:strStartDate/:strEndDate

```
curl -X GET 'http://localhost:9000/packages-shorex/1/4' | jq
```

```
{
  "ncl_CruiseGetAvailShorexPackagesRS": {
    "altLangID": "en-us",
    "echoToken": "String",
    "primaryLangID": "en-us",
    "sequenceNmbr": 1,
    "target": "Development",
    "transactionIndentifier": "4",
    "success": "",
    "packages": [
      {
        "id": 213,
        "type": "EXCURSION",
        "name": "name",
        "code": "RTBN03",
        "dataRange": {
          "packageStartDate": "1570-12-27T00:00:00.000+05:00",
          "packageEndDate": "1572-07-31T00:00:00.000+05:00"
        },
        "price": [
          {
            "guest": {
              "guestRefNumber": 1,
              "packagePrice": {
                "amount": 1000,
                "currencyCode": "USD"
              }
            }
          }
        ],
        "location": {
          "from": {
            "code": "RTB",
            "type": "PORT",
            "name": "portName"
          },
          "to": {
            "code": "RTB",
            "type": "PORT",
            "name": "portName"
          }
        }
      }
    ]
  }
}
```   

### Retrieve Available Entertainment Packages

GET /packages-entertainment/:reservationId/:transactionId

```
curl -X GET 'http://localhost:9000/packages-entertainment/1/4' | jq
```
```
{
  "NCL_CruiseGetAvailOnBoardPackagesRS": {
    "AltLangID": "en-us",
    "EchoToken": "String",
    "PrimaryLangID": "en-us",
    "SequenceNmbr": 1,
    "Target": "Development",
    "TransactionIdentifier": 4,
    "Version": "3.1",
    "Success": "",
    "Warnings": "",
    "Packages": {
      "Package": {
        "ID": 23,
        "Type": "ENTERTAINMENT",
        "Name": "eventName",
        "Code": "ENT031",
        "DateRange": {
          "PackageStartDate": "1569-06-29T00:00:00.000+05:00",
          "PackageEndDate": "1570-02-24T00:00:00.000+05:00"
        },
        "Price": {
          "Guest": {
            "GuestRefNumber": 1,
            "Amount": 10000,
            "CurrencyCode": "USD"
          }
        }
      }
    }
  }
}
```     

### Add Package

```
curl --location --request POST "http://localhost:9000/addPackage" \
--header "Content-Type: application/json" \
--data "{
    \"participant\": {
        \"clientId\": 1,
        \"fidelioAccountId\": 2,
        \"name\": \"Some Name\",
        \"isAdult\": true,
        \"quantity\": 1,
        \"embarkDate\": \"2019/10/11\",
        \"disembarkDate\": \"2019/10/22\"
    },
    \"PackageID\": \"23\",
    \"PackageType\": \"EXCURSION\",
    \"PackageCode\": \"00000\",
    \"PackageStartDate\": \"2019/10/11\",
    \"PackageStartTime\": \"2019/10/22\",
    \"RestaurantCode\": \"100\"
}"
``` 

```
{
    "packageId": "23"
}
```

### Add Entertainment Booking

POST /bookEntertainment
```
curl --location --request POST "http://localhost:9000/bookEntertainment" \
--header "Content-Type: application/json" \
--data "{
    \"participant\": {
        \"clientId\": 1,
        \"fidelioAccountId\": 2,
        \"name\": \"Some Name\",
        \"isAdult\": true,
        \"quantity\": 1,
        \"embarkDate\": \"2019/10/11\",
        \"disembarkDate\": \"2019/10/22\"
    },
    \"eventId\": 1
}"
```     

```
{
    "accid": 23,
    "balance": 100.5,
    "cruiseid": 3
}
```

###  GetFolio

GET /folio/:reservationId/:clientId

```
~> curl -X GET 'http://localhost:9000/folio/879/6786' | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   339  100   339    0     0     50      0  0:00:06  0:00:06 --:--:--    75
{
  "Onboard_RetrieveAccountFolioRS": {
    "ReservationID": 879,
    "GuestDetail": {
      "NamePrefix": "Surname",
      "GivenName": "GivenName",
      "Surname": "NamePrefix",
      "LoyaltyMembershipID": 12
    },
    "TotalDue": "899879.00",
    "FolioItems": {
      "FolioItem": {
        "Date": "1570-12-27 00:00:00",
        "Item": "department",
        "Subdepartment": "subdepartment",
        "Port": "port",
        "PostingTransactionID": 1
      }
    }
  }
}
```

### Post Folio Charge

```
curl --location --request POST "http://localhost:9000/folio/charge" \
--header "Content-Type: application/json" \
--data "{
    \"LoyaltyMembershipID\": \"23\",
    \"OutletID\": \"231389\",
    \"TotalAmount\": \"20.2\",
    \"Tip\": \"3.3\",
    \"Tax\": \"10.11\",
    \"DeviceUnitName\": \"theDevice\",
    \"Notes\": \"notes\",
    \"Image\": \"img\",
    \"UniquePostingID\": \"100\",
    \"AllowPosting\": true
}"
```

```
  {
      "bSuccess": true,
      "sTables": [
          {
              "gnAccID": 1,
              "geAccountType": 1,
              "gbAllowPosting": true,
              "gsName": "",
              "gdEmbDate": -59136566400000,
              "gdDisDate": -59136480000000,
              "gnBalance": 3123213,
              "gsFirstName": "first name",
              "gsLastName": "last name",
              "gsSalutation": "saluation",
              "gsTitle": "title",
              "gsGender": "female",
              "geAgeGroup": 2,
              "gsLanguage": "EN",
              "gsResStatus": "A",
              "gsHandicap": "handicap",
              "gsHandicapRemark": "handicap remark",
              "gsMusterStation": "muster station",
              "gbOnboard": false,
              "gePicture": 313324234,
              "gsFreqCardNo": "2314-1231-2312-3231",
              "gsPriceCategory": "price category",
              "gsCabinType": "cabin type",
              "gdSysdate": -59133888000000,
              "gdCurrentCruiseStartDate": -59131382400000,
              "gnPGID": 231,
              "gsCruiseItnID": "212",
              "gsResReference": "REF",
              "gsExternalID": "exId"
          }
      ],
      "sObj": "",
      "sErrCode": "",
      "sErrWord": ""
  }
```

### Get Current Cruise

GET  /cruise

```
curl -X GET 'http://localhost:9000/cruise' | jq
```                                                     

```
{
  "id": 111,
  "shipName": "shipName",
  "cruiseName": "cruiseName",
  "itineraryId": 22,
  "onboardCurrency": "onboardCurrency",
  "startDate": -59136566400000,
  "endDate": -59136566400000
}
```

### Get Tender Tickets

GET /tenderTicket/:reservationId/:packageId/:buyerClientID/ 

```
curl -X GET 'http://localhost:9000/tenderTicket/1/2/12/'
```

### Book Tender Ticket

```
curl -i -X POST 'http://localhost:9000/bookTenderTicket' -H "Content-Type: application/json" -d '
{
    "participant": {
        "clientId": 1,
        "fidelioAccountId": 2,
        "name": "Some Name",
        "isAdult": true,
        "quantity": 1,
        "embarkDate": "2019/10/11",
        "disembarkDate": "2019/10/22"
    },
    "packages": [{
        "id": 11,
        "eventNumber": "TND001",
        "eventName": "Tender Event Name",
        "startDate": "20191011051030",
        "endDate": "20191011053050",
        "price": 20.4,
        "priceCategoryCode": "00000",
        "personGroupCode": "11111111",
        "seatsCurrentlyBooked": 2,
        "maximumSeats": 30,
        "showInITV": true,
        "bookingStatus": "A"
    }],
    "index": 1
}'

```


### Lookup Reservation By Cabin

GET  /reservation/by-cabin/:id 

```
curl -X GET 'http://localhost:9000/reservation/by-cabin/11' | jq
``` 

```
{
  "id": 879,
  "clients": [
    {
      "nclClientId": 12,
      "name": {
        "givenName": "GivenName",
        "namePrefix": null,
        "nameTitle": "Surname",
        "middleName": null,
        "surname": "NamePrefix",
        "surnamePrefix": null,
        "nameSuffix": null
      },
      "birthDate": 980985600000,
      "nationality": "US",
      "gender": "male",
      "email": "some@some.io",
      "phone": "8900787977",
      "address": {
        "street": "street",
        "street2": "street2",
        "city": "city",
        "zip": "zip",
        "state": "state",
        "country": "country"
      },
      "latitudesLevel": "55",
      "fidelioAcctId": 6786,
      "reservationId": 879,
      "cabinNumber": "87979",
      "deck": "deck",
      "reservationStatus": "status",
      "minor": false,
      "embarkDate": -59136566400000,
      "disembarkDate": -59136566400000,
      "balance": 899879,
      "picture": "pic"
    }
  ]
}
```       

### Get Guest Key Card

GET /guest/key-card/:id 

```
curl --location --request GET "http://localhost:9000/guest/key-card/23"

  
{
    "FOLIO": "23",
    "TITLE": "Guest Key Card",
    "FNAME": "Some Name",
    "LNAME": "Some Last Name",
    "LATITUDES": "40.712776",
    "RES_STATUS": "A",
    "GUEST_TYPE": "0",
    "ASSEMBLY_STATION": "some assembly station",
    "CABIN": "10102",
    "IS_SPA_CABIN": false,
    "IS_HAVEN": false,
    "PACKAGES": "packages",
    "CAB_TYPE": "P",
    "HAS_PC": false,
    "IS_ADULT": false,
    "AGE": 30,
    "SHIP": " 6 MID SHIP",
    "EDEBARK": -59136566400000,
    "CLASSIFICATION": "VIP"
}

```


### Book Tender Ticket

#### V1

POST /bookTenderTicket

```
curl --location --request POST "http://localhost:9000/bookTenderTicket" \
--header "Content-Type: application/json" \
--data "{
    \"participant\": {
        \"clientId\": 1,
        \"fidelioAccountId\": 2,
        \"name\": \"Some Name\",
        \"isAdult\": true,
        \"quantity\": 1,
        \"embarkDate\": \"2019/10/11\",
        \"disembarkDate\": \"2019/10/22\"
    },
    \"packages\": [{
        \"id\": 23,
        \"eventNumber\": \"TND001\",
        \"eventName\": \"Tender Event Name\",
        \"startDate\": \"2019-10-11\",
        \"endDate\": \"2019-10-12\",
        \"price\": 20.4,
        \"priceCategoryCode\": \"00000\",
        \"personGroupCode\": \"11111111\",
        \"seatsCurrentlyBooked\": 2,
        \"maximumSeats\": 30,
        \"showInITV\": true,
        \"bookingStatus\": \"A\"
    }],
    \"index\": 0
}"
```     

```
{
    "packageId": 23,
    "packageName": "Tender Event Name"
}
```

## Get Guest Amenities

```
curl --location --request GET "{{url}}/guest/getAmenity/:id"
```

```
{
    "amenities": [
        {
            "QTY": "1",
            "CODE": "INT60",
            "DESCR": "INTERNET PACKAGE (60 MINUTES)",
            "FIDDESCR": "INTERNET PACKAGE (60 MINUTES)",
            "AMOUNT": "0",
            "DELIVERLOCATION": "CABIN",
            "DEPT": "37",
            "DEPTNAME": "Financial Department",
            "DELIVER": "20200105000000"
        },
        {
            "QTY": "1",
            "CODE": "CHSULM",
            "DESCR": "CHOICE SHORE EXCURSION CREDIT",
            "FIDDESCR": "CHOICE SHORE EXCURSION CREDIT",
            "AMOUNT": "0",
            "DELIVERLOCATION": "CABIN",
            "DEPT": "37",
            "DEPTNAME": "Financial Department",
            "DELIVER": "20200105000000"
        },
        {
            "QTY": "1",
            "CODE": "CHI250",
            "DESCR": "INTERNET PACKAGE (250 MINUTES OR 1000 MB)",
            "FIDDESCR": "INTERNET PACKAGE (250 MINUTES OR 1000 MB)",
            "AMOUNT": "0",
            "DELIVERLOCATION": "CABIN",
            "DEPT": "37",
            "DEPTNAME": "Financial Department",
            "DELIVER": "20200105000000"
        }
    ]
}
```

## Fidelio Mock API Calls

GET     /fidelio/FidelioSPMSWSJsonGet     controllers.fidelioMock.FidelioMockController.login(psFunction: Option[String], psParam: Option[String], psSessionId: Option[String])

```
~> curl -X GET 'http://localhost:9000/fidelio/FidelioSPMSWSJsonGet?psFunction=%22Login%22&psParam=%5B%22fidelio%22%2C%22e36819094979ec646d610d6095238f45%22%5D&psSessionId=%22%22' | jq

{
  "bSuccess": true,
  "sTables": [
    "51110001115"
  ]
}
```

if login pass is wrong it return HTTP/1.1 401 Unauthorized
```
login&pass incorrect
```


## Old V1 Routes


POST    /routing

```
~> curl -i -X POST 'http://localhost:9000/routing' -H "Content-Type: application/json" -d '{ "payerFidelioID":1, "buyerFidelioID":3, "buyerID": 1}'
HTTP/1.1 200 OK
Referrer-Policy: origin-when-cross-origin, strict-origin-when-cross-origin
X-Frame-Options: DENY
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Permitted-Cross-Domain-Policies: master-only
Date: Fri, 16 Aug 2019 00:35:54 GMT
Content-Type: text/plain; charset=UTF-8
Content-Length: 0

```         

### Booked Excursions

GET  /booked-excursions/:fidelioAcctId

```
 ~> curl -X GET 'http://localhost:9000/booked-excursions/2'
Got Id:2⏎
```

### Booked Onboard Events

GET  /booked-onboard-events/:fidelioAcctId

```
~> curl -X GET 'http://localhost:9000/booked-onboard-events/2'
Got Id:2⏎
```

### Sailing / Cruise

GET  /sailing

```
~> curl -X GET 'http://localhost:9000/sailing'
Ok⏎
```     


