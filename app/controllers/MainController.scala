package controllers

import controllers.bindables.ClientBindables._
import controllers.bindables.ReservationBindables._
import javax.inject._
import modules.data_mapper.model.FidelioGuest
import modules.data_mapper.service._
import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.model._
import modules.fidelio_io.util.DateUtil
import play.api.Logging
import play.api.libs.json.{Json, Xml, _}
import play.api.mvc.{Action, _}

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}
import scala.xml.NodeSeq

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class MainController @Inject()(cc: ControllerComponents)
    extends AbstractController(cc)
    with Logging {

  import FidelioClientAccount._
  import FidelioCruise._
  import FidelioEventFolio._
  import FidelioExcursionFolio._
  import FidelioReservation._

  /**
    * Expecting Json body with parameters:
    *   reservationId
    *   payerClientId
    *   buyerIDList
    */
  def routing: Action[JsValue] = Action(parse.json).async {
    request: Request[JsValue] =>
      val payerFidelioID: Int = (request.body \ "payerFidelioID").as[Int]
      val buyerFidelioID: Int = (request.body \ "buyerFidelioID").as[Int]
      val buyerID: Int = (request.body \ "buyerID").as[Int]

      val f: Future[Try[NodeSeq]] =
        OnboardSetRouting.setupRouting(payerFidelioID, buyerFidelioID, buyerID)

      f.map {
        case Success(_) =>
          Ok("")
        case Failure(e) =>
          InternalServerError(e.getMessage)
      }
  }

  def bookTenderTicket: Action[JsValue] = Action(parse.json).async {
    request: Request[JsValue] =>
      val buyerOpt: Option[Participant] =
        (request.body \ "participant").asOpt[Participant]
      val packages: Seq[FidelioEvent] =
        (request.body \ "packages").as[Seq[FidelioEvent]]
      val indexOpt: Option[Int] = (request.body \ "index").asOpt[Int]

      val buyer = Try(buyerOpt.get)
      val index = Try(indexOpt.get)

      val t = for {
        b <- buyer
        i <- index
      } yield {
        OnboardBookPackageProcessor.doTenderTicketBooking(b, packages, i).map {
          case Success(bookingResult) =>
            val json = JsObject(
              Seq(
                "packageId" -> JsNumber(bookingResult._1),
                "packageName" -> JsString(bookingResult._2)
              )
            )
            Ok(json)
          case Failure(e) =>
            InternalServerError(e.getMessage)
        }
      }

      t match {
        case Success(value) => value
        case Failure(e) =>
          Future(
            InternalServerError(
              s"An error occurred trying book Tender Tickets: " +
                s"$e \n ${e.getStackTrace.mkString("\n")}"
            )
          )
      }
  }

  /**
    * Expecting parameters:
    *   id: cardNumber or clientId
    *
    *   @param id The cardNumber or clientId as string.
    *   @param t type
    */
  def clientId(id: String, t: ClientIdType): Action[AnyContent] = Action.async {
    val f: Future[Try[FidelioClientAccount]] = t match {
      case ClientId()   => MessageUtil.retrieveClient(id)
      case CardNumber() => MessageUtil.retrieveClientByCardNumber(id)
    }
    f.map {
      case Success(value) =>
        Ok(Json.toJson(value))
      case Failure(e) =>
        InternalServerError(e.getMessage)
    }
  }

  def guestKeyCard(id: String): Action[AnyContent] = Action.async {
    val f: Future[Try[FidelioClientKeyCard]] = MessageUtil.getGuestKeyCard(id)

    f.map {
      case Success(value) =>
        Ok(Json.toJson(value))
      case Failure(e) =>
        InternalServerError(e.getMessage)
    }
  }

  /**
    * Expecting parameters:
    *   reservationId or cabinNumber
    */
  def reservation(id: String, t: ReservationIdType): Action[AnyContent] =
    Action.async {
      val f: Future[Try[FidelioReservation]] = t match {
        case ReservationId() => MessageUtil.retrieveReservation(id)
        case CabinNumber()   => MessageUtil.retrieveReservationByCabin(id)
      }

      f.map {
        case Success(value) =>
          Ok(Json.toJson(value))
        case Failure(e) =>
          InternalServerError(e.getMessage)
      }
    }

  /**
    * Expecting parameters:
    *  fidelioAcctId
    */
  def bookedExcursions(fidelioAcctId: Int): Action[AnyContent] = Action.async {
    MessageUtil.retrieveExcursionFolio(fidelioAcctId).map {
      case Success(value) =>
        Ok(Json.toJson(value))
      case Failure(e) =>
        InternalServerError(e.getMessage)
    }
  }

  /**
    * Expecting parameters:
    *  fidelioAcctId
    */
  def bookedOnBoardEvents(fidelioAcctId: Int): Action[AnyContent] =
    Action.async {
      MessageUtil.retrieveEventFolio(fidelioAcctId).map {
        case Success(value) =>
          Ok(Json.toJson(value))
        case Failure(e) =>
          InternalServerError(e.getMessage)
      }
    }

  /**
    * Expecting parameters:
    *  NONE
    */
  def sailing(): Action[AnyContent] = Action.async {
    MessageUtil.retrieveCruise.map {
      case Success(value) =>
        Ok(Json.toJson(value))
      case Failure(e) =>
        InternalServerError(e.getMessage)
    }
  }

  def guestPicture(folioId: String): Action[AnyContent] = Action.async {
    MessageUtil.retrieveGuestPicture(folioId).map {
      case Success(value) =>
        Ok(
          JsObject(
            Seq("picture" -> JsString(value))
          )
        )
      case Failure(e) =>
        InternalServerError(e.getMessage)
    }
  }

  def packages(reservationId: String,
               transactionId: String): Action[AnyContent] = Action.async {

    val reservationFuture = MessageUtil.retrieveReservation(reservationId)
    val cruiseFuture = MessageUtil.retrieveCruise

    val response = (for {
      reservationResult <- reservationFuture
      cruiseResult <- cruiseFuture
    } yield {
      (reservationResult, cruiseResult) match {
        case (Success(reservation), Success(cruise)) => {
          for {
            itineraryResult <- MessageUtil.retrieveItinerary(
              reservation.clients.head
            )
            eventFoliosResult <- MessageUtil.retrieveEventFolios(reservation)
            excFoliosResult <- MessageUtil.retrieveExcursionFolios(reservation)
            excursionsResult <- MessageUtil.retrieveExcursions(reservation)
            //// TODO: This call to SilverWebDriver
            //diningFoliosResult <- MessageUtil.retrieveDiningFolios(reservation)
          } yield {

            val reservationJson = Json.toJson(reservation)
            val cruiseJson = Json.toJson(cruise)

            val reservationIdJson = JsString(reservationId)
            val transactionIdJson = JsString(transactionId)

            (
              itineraryResult,
              eventFoliosResult,
              excFoliosResult,
              excursionsResult
            ) match {
              case (
                  Success(itinerary),
                  Success(eventFolios),
                  Success(excFolios),
                  Success(excursions)
                  ) => {
                val itineraryJson = Json.toJson(itinerary)
                val eventFoliosJson = Json.toJson(eventFolios)
                val excFoliosJson = Json.toJson(excFolios)
                val excursionsJson = Json.toJson(excursions)

                val processResult =
                  NCL_CruiseRetrievePackagesProcessor.createPackageResultMaps(
                    reservation,
                    eventFolios,
                    excFolios,
                    Some(excursions)
                  )

                // a map of guests keyed by the Fidelio account id
                val guestsMap: mutable.Map[Int, FidelioGuest] = processResult._1

                // a map of purchased events keyed by the event id
                val purchasedEventsMap
                    : mutable.Map[Int, FidelioEventFolioItem] =
                  processResult._2

                // a map of purchased excursions keyed by the excursion id
                val purchasedExcursionsMap
                    : mutable.Map[Int, FidelioExcursionFolioItem] =
                  processResult._3

                // a map of port names keyed by the excursion code
                val excCodePortNamesMap: mutable.Map[String, String] =
                  processResult._4

                val eventPackages =
                  JsArray(
                    PackagesProcessor
                      .buildEventPackages(purchasedEventsMap, guestsMap)
                      .map(s => Json.parse(s))
                      .toSeq
                  )

                val excursionPackages = JsArray(
                  PackagesProcessor
                    .buildExcursionPackages(
                      purchasedExcursionsMap,
                      guestsMap,
                      excCodePortNamesMap
                    )
                    .map(s => Json.parse(s))
                    .toSeq
                )

                val json = JsObject(
                  Seq(
                    "reservation" -> reservationJson,
                    "cruise" -> cruiseJson,
                    "itinerary" -> itineraryJson,
                    "reservationId" -> reservationIdJson,
                    "transactionId" -> transactionIdJson,
                    "eventPackages" -> eventPackages,
                    "excursionPackages" -> excursionPackages,
                    "excFolios" -> excFoliosJson,
                    "excursions" -> excursionsJson
                    // TODO: add Dining from SilverWebDriver here
                  )
                )

                Ok(json)
              }
              case _ =>
                InternalServerError(
                  s"""Error retrieving
                     | itineraryResult: $itineraryResult,
                     | eventFoliosResult: $eventFoliosResult,
                     | excFoliosResult: $excFoliosResult,
                     | excursionsResult: $excursionsResult
                     | for reservation id ${reservationId}""".stripMargin
                )
            }
          }
        }
        case _ =>
          Future {
            InternalServerError(
              s"""Error retrieving
                 | reservationResult: $reservationResult
                 | cruiseResult: $cruiseResult
                 | for reservation id ${reservationId}""".stripMargin
            )
          }
      }
    }).flatten

    response
  }

  def retrieveShorexPackages(reservationId: String,
                             transactionId: String,
                             strStartDate: String,
                             strEndDate: String): Action[AnyContent] =
    Action.async {

      val reservationFuture = MessageUtil.retrieveReservation(reservationId)

      val startDate =
        DateUtil.parseDate(strStartDate, DateUtil.FORMAT_NCL_PACKAGE_DATE)
      val endDate =
        DateUtil.parseDate(strEndDate, DateUtil.FORMAT_NCL_PACKAGE_DATE)

      def getExcResult(
          reservationResult: Try[FidelioReservation]
      ): Future[Try[Seq[FidelioExcursion]]] = {
        reservationResult match {
          case Success(reservation) => {
            MessageUtil.retrieveExcursions(startDate, endDate, reservation)
          }
          case Failure(ex) =>
            Future(Failure(ex))
        }
      }

      val resp = for {
        reservationResult <- reservationFuture
        excursionResult <- getExcResult(reservationResult)
      } yield {

        (excursionResult, reservationResult) match {
          case (Success(excursions), Success(reservation)) => {
            val guests = reservation.clients

            val numGuests = guests.size

            val packages: Seq[FidelioExcursion] = excursions
              .filter(_.showInITV == true)
              .filter(_.bookingStatus == MessageUtil.STATUS_AVAILABLE)
              .filter(MessageUtil.getNumAvailableSeats(_) >= numGuests)

            val jsonStr = PackagesProcessor.createShorexStr(
              transactionId: String,
              guests: Seq[FidelioClientAccount],
              packages: Seq[FidelioExcursion]
            )
            Ok(Json.parse(jsonStr))

          }
          case _ =>
            InternalServerError(
              "An error occurred retrieving the available shorex packages"
            )
        }

//      PackagesProcessor.createShorexNclResponse(excursionResult, reservationResult, transactionId)
      }

      resp
    }

  def retrieveEntertaimentPackages(reservationId: String,
                                   transactionId: String,
                                   packageType: String,
                                   strStartDate: String): Action[AnyContent] =
    Action.async {

      val startDate =
        DateUtil.parseDate(strStartDate, DateUtil.FORMAT_SILVERWEB_DATE)
      // Unlike in FCUI, the web service requires 1 extra day to return the events.
      // Otherwise, you'll get back no events.
      val endDate = DateUtil.addDays(startDate, 1)

      val reservationFuture = MessageUtil.retrieveReservation(reservationId)

      (for {
        reservationResult <- reservationFuture
      } yield {
        reservationResult match {
          case Success(reservation) =>
            val eventsFuture =
              MessageUtil.retrieveEvents(startDate, endDate, reservation)
            for (eventsResult <- eventsFuture) yield {
              eventsResult match {
                case Success(events) =>
                  val packages: NodeSeq =
                    NCL_CruiseGetAvailOnBoardPackagesProcessor
                      .createPackageNclResponse(
                        events,
                        reservation,
                        transactionId,
                        packageType
                      )
                  val json = Xml.toJson(packages)
                  Ok(json)
                case Failure(e) =>
                  InternalServerError(
                    s"An error occurred retrieving the events, $e"
                  )
              }
            }
          case Failure(e) =>
            Future(
              InternalServerError(
                s"An error occurred retrieving the reservation, $e"
              )
            )
        }
      }).flatten

    }

  def bookEntertainmentPackage: Action[JsValue] = Action(parse.json).async {
    request: Request[JsValue] =>
      val eventIdOpt: Option[Int] = (request.body \ "eventId").asOpt[Int]
      val buyerOpt: Option[Participant] =
        (request.body \ "participant").asOpt[Participant]

      bookEvent(eventIdOpt, buyerOpt)
  }

  private def bookEvent(eventIdOpt: Option[Int],
                        buyerOpt: Option[Participant]) = {
    val eventId = Try(eventIdOpt.get)
    val buyer = Try(buyerOpt.get)

    val t = for {
      id <- eventId
      b <- buyer
    } yield {
      OnboardBookPackageProcessor.bookEvent(id, b) map {
        case Success(bookingResult) => Ok(Json.toJson(bookingResult))
        case Failure(e) =>
          val msg =
            s"Failed to book event packageId ${id} for client ID: ${b.clientId}"
          logger.error(msg, e)
          InternalServerError(msg + "\n" + e.getMessage)
      }
    }

    t match {
      case Success(value) => value
      case Failure(e) =>
        Future(
          InternalServerError(
            s"An error occurred trying book event: " +
              s"$e \n ${e.getStackTrace.mkString("\n")}"
          )
        )
    }
  }

  def retrieveFolio(reservationId: String,
                    clientId: String): Action[AnyContent] = Action.async {
    val xml = <Onboard_RetrieveAccountFolioRS>
                <ReservationID>{reservationId}</ReservationID>
                <LoyaltyMembershipID>{clientId}</LoyaltyMembershipID>
              </Onboard_RetrieveAccountFolioRS>
    val folioFuture = OnboardRetrieveAccountFolioProcessor.process(xml)

    for (folioResult <- folioFuture) yield {
      folioResult match {
        case Success(folio) =>
          val json = Xml.toJson(folio)
          Ok(json)
        case Failure(e) =>
          InternalServerError(s"An error occurred retrieving the folio, $e")
      }
    }

  }

  def retrieveTenderTicket(reservationId: String,
                           packageId: String,
                           buyerClientID: String,
                           packageStartDate: String): Action[AnyContent] =
    Action.async {

      val packageType = MessageUtil.PACKAGE_TENDER_TICKET
//    val startDate =
//      DateUtil.parseDate(packageStartDate, DateUtil.FORMAT_NCL_PACKAGE_DATE)

      val nclRequest = <NCL_RetrieveTenderTicketRS>
      <ReservationID>{reservationId}</ReservationID>
      <PackageID>{packageId}</PackageID>
      <PackageType>{packageType}</PackageType>
      <BuyerClientID>{buyerClientID}</BuyerClientID>
      <PackageStartDate>{packageStartDate}</PackageStartDate>
    </NCL_RetrieveTenderTicketRS>

      val tenderTicketsFuture = OnboardBookPackageProcessor.process(nclRequest)

      for (tenderTicketsResult <- tenderTicketsFuture) yield {
        tenderTicketsResult match {
          case Success(tickets) =>
            val json = Xml.toJson(tickets)
            Ok(json)
          case Failure(e) =>
            InternalServerError(
              s"An error occurred retrieving the tender tickets, $e"
            )
        }
      }
    }

  def addPackage: Action[JsValue] = Action(parse.json).async {
    request: Request[JsValue] =>
      val buyer = Try((request.body \ "participant").asOpt[Participant].get)
      val packageId = Try((request.body \ "PackageID").asOpt[String].get)
      val packageType = Try((request.body \ "PackageType").asOpt[String].get)
      val packageCode = Try((request.body \ "PackageCode").asOpt[String].get)
      val packageStartDate =
        Try((request.body \ "PackageStartDate").asOpt[String].get)
      val packageStartTime =
        Try((request.body \ "PackageStartTime").asOpt[String].get)
      val restaurantNameCode =
        Try((request.body \ "RestaurantCode").asOpt[String].get)

      val t = for {
        b <- buyer
        pID <- packageId
        pkgT <- packageType
        pkgC <- packageCode
        pkgSD <- packageStartDate
        pkgST <- packageStartTime
        rNC <- restaurantNameCode
      } yield {
        pkgT match {
          case MessageUtil.PACKAGE_EXCURSION =>
            OnboardBookPackageProcessor
              .bookExcursion(pID.toInt, b.fidelioAccountId.get, b) map {
              case Success(_) =>
                val json = JsObject(
                  Seq(
                    "packageId" -> JsString(pID)
                  )
                )
                Ok(json)
              case Failure(e) =>
                val msg =
                  s"Failed to add package packageId ${pID} for client ID: ${b.clientId}, ${e.getStackTrace
                    .mkString("\n")}"
                InternalServerError(msg)
            }
          case MessageUtil.PACKAGE_ENTERTAINMENT =>
            OnboardBookPackageProcessor.bookEvent(pID.toInt, b) map {
              case Success(_) =>
                val json = JsObject(
                  Seq(
                    "packageId" -> JsString(pID)
                  )
                )
                Ok(json)

              case Failure(e) =>
                val msg =
                  s"Failed to add package packageId ${pID} for client ID: ${b.clientId}, ${e.getStackTrace
                    .mkString("\n")}"
                InternalServerError(msg)
            }

          case MessageUtil.PACKAGE_DINING =>
            OnboardBookPackageProcessor.bookDining(
              pkgC,
              rNC,
              pkgSD,
              pkgST,
              b.fidelioAccountId.get,
              b
            ) map {
              case Success(_) =>
                val json = JsObject(
                  Seq(
                    "packageId" -> JsString(pID)
                  )
                )
                Ok(json)

              case Failure(e) =>
                val msg =
                  s"Failed to add package packageId ${pID} for client ID: ${b.clientId}, ${e.getStackTrace
                    .mkString("\n")}"
                InternalServerError(msg)
            }
          case _ =>
            Future(InternalServerError(s"Invalid package type: ${packageType}"))
        }
      }

      t match {
        case Success(value) => value
        case Failure(e) =>
          Future(
            InternalServerError(
              s"An error occurred trying book event - is not enough parameters at input data: " +
                s"$e \n ${e.getStackTrace.mkString("\n")}"
            )
          )
      }
  }

  def postFolioCharge(): Action[JsValue] = Action(parse.json).async {
    request: Request[JsValue] =>
      //        val buyer = Try((request.body \ "participant").asOpt[Participant].get)

      def getDouble(v: Option[String]) = v match {
        case Some(v) if v == "" => 0.0
        case Some(v)            => v.toDouble
        case None               => 0.0
      }

      val clientId = (request.body \ "LoyaltyMembershipID").asOpt[String]
      val outletId = (request.body \ "OutletID").asOpt[String]

      val strTotalAmountOpt = (request.body \ "TotalAmount").asOpt[String]
      val totalAmount = getDouble(strTotalAmountOpt)

      val strTipOpt = (request.body \ "Tip").asOpt[String]
      val tip = getDouble(strTipOpt)

      val strTax = (request.body \ "Tax").asOpt[String]
      val tax = getDouble(strTax)

      val deviceUnitName = (request.body \ "DeviceUnitName").asOpt[String]
      val notes = (request.body \ "Notes").asOpt[String]

      val image = (request.body \ "Image").asOpt[String]

      val uniquePostingId = (request.body \ "UniquePostingID").asOpt[String]

      val allowPosting =
        (request.body \ "AllowPosting").asOpt[Boolean].getOrElse(false)

      (for {
        cID <- clientId
        oID <- outletId
        dUN <- deviceUnitName
        n <- notes
        i <- image
        uPID <- uniquePostingId
      } yield {
        val postingDetails = FidelioPostingDetails.createPostingDetails(
          totalAmount,
          tip,
          tax,
          oID,
          dUN,
          n,
          uPID,
          allowPosting,
          i
        )

        MessageUtil.makeGenericPosting(cID, postingDetails).map {
          case Success(v) =>
            Ok(v)
          case Failure(e) =>
            InternalServerError(
              s"unable to make generic posting ${e.getMessage} for $cID"
            )
        }
      }).getOrElse(
        Future(InternalServerError("Error during processing input parameters"))
      )
  }


  def getAmenity(accountId: Int): Action[AnyContent] = Action.async {
      MessageUtil.getAmenity(accountId).map {
        case Success(value) =>
          Ok(
            JsObject(
              Seq(
                "amenities" -> Json.toJson(value)
              )
            )
          )
        case Failure(e) =>
          InternalServerError(e.getMessage)
      }
  }

}
