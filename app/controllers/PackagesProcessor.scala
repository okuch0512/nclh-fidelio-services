package controllers

import modules.common.Settings
import modules.data_mapper.model.FidelioGuest
import modules.data_mapper.service.NCL_CruiseRetrievePackagesProcessor
import modules.data_mapper.util.{CurrencyUtil, MessageUtil}
import modules.fidelio_io.model._
import modules.fidelio_io.util.DateUtil
import play.api.libs.json.{JsArray, Json}

object PackagesProcessor {

  def buildEventPackages(
      purchasedEvents: scala.collection.mutable.Map[Int, FidelioEventFolioItem],
      guestsMap: scala.collection.mutable.Map[Int, FidelioGuest]
  ): Iterable[String] = {
    for ((eventId, ev) <- purchasedEvents) yield {

      s"""
         |{
         |  "package" : {
         |    "id" : "${eventId.toString}",
         |    "typeCode": 4,
         |    ${if (MessageUtil.isTenderTicket(ev))
           """"type": "TENDER_TICKET","""
         else
           """"type": "ENTERTAINMENT","""}
         |    "name": "${ev.eventName}",
         |    "code": "${MessageUtil.getEventCode(ev.eventNumber)}",
         |    "comments": "${ev.eventName}",
         |    "dataRange": {
         |       "packageStartDate": "${DateUtil
           .formatDate(ev.startDate, DateUtil.FORMAT_NCL_DATE)}",
         |       "packageEndDate": "${DateUtil
           .formatDate(ev.endDate, DateUtil.FORMAT_NCL_DATE)}"
         |    },
         |    "price" :
         |    ${
           val seq = for ((fidelioAccountId, guest) <- guestsMap) yield {
             val eventForGuest = NCL_CruiseRetrievePackagesProcessor
               .findEventInFolio(eventId, guest.eventFolio)

             if (eventForGuest.isDefined && eventId
                   .equals(eventForGuest.get.id)) {
               s"""{"guest": {
                "guestRefNumber": "${guest.guestRefNumber}",
                "packagePrice": {
                  "amount": "${CurrencyUtil
                    .convertDollarsToCents(eventForGuest.get.bookingValue)
                    .toString}",
                "currencyCode": "USD"
                }}}""".stripMargin
             } else "{}"
           }
           JsArray(seq.map(s => Json.parse(s)).toSeq)
         }
         |    ,
         |    "location": {
         |      "from": {
         |        "code": "${MessageUtil.getPortCodeFromEventOrExcursionNumber(
           ev.eventNumber
         )}",
         |        "type": "SHIP"
         |      },
         |      "to": {
         |        "code": "",
         |        "type": ""
         |      }
         |    }
         |  }
         |}""".stripMargin

    }
  }

  def buildExcursionPackages(
      purchasedExcursions: scala.collection.mutable.Map[
        Int,
        FidelioExcursionFolioItem
      ],
      guestsMap: scala.collection.mutable.Map[Int, FidelioGuest],
      excCodePortNamesMap: scala.collection.mutable.Map[String, String]
  ): Iterable[String] = {
    for ((excId, exc) <- purchasedExcursions) yield {
      s"""
         |{
         |  "package" : {
         |    "id" : "${excId.toString}",
         |    "packageTypeCode": "4",
         |    "type": "SHORE EXCURSION",
         |    "name": "${exc.excursionName}",
         |    "code": "${MessageUtil.getExcursionCode(exc.excursionNumber)}",
         |    "comments" : "${exc.excursionName}",
         |    "dateRange" : {
         |      "packageStartDate" : "${DateUtil
           .formatDate(exc.startDate, DateUtil.FORMAT_NCL_DATE)}",
         |      "packageEndData" : "${DateUtil
           .formatDate(exc.endDate, DateUtil.FORMAT_NCL_DATE)}"
         |    },
         |    "price" :
         |      ${
           val seq = for ((fidelioAccountId, guest) <- guestsMap) yield {
             val excForGuest = NCL_CruiseRetrievePackagesProcessor
               .findExcursionInFolio(excId, guest.excFolio)

             if (excForGuest != None && excId.equals(excForGuest.get.id)) {
               s"""
                  | {"guest": {
                  |      "guestRefNumber": "${guest.guestRefNumber}",
                  |      "packagePrice": {
                  |        "amount": "${CurrencyUtil
                    .convertDollarsToCents(excForGuest.get.bookingValue)
                    .toString}",
                  |      "currencyCode": "USD"
                  |      }}}
                  |""".stripMargin
             } else "{}"
           }
           JsArray(seq.map(s => Json.parse(s)).toSeq)
         }
         |     ,
         |     "location": {
         |       "from": {
         |         "code": "${MessageUtil.getPortCodeFromEventOrExcursionNumber(
           exc.excursionNumber
         )}",
         |         "type": "PORT",
         |         "name" : "${excCodePortNamesMap
           .getOrElse(exc.excursionNumber, "")}"
         |       },
         |       "to": {
         |         "code": "${MessageUtil.getPortCodeFromEventOrExcursionNumber(
           exc.excursionNumber
         )}",
         |         "type": "PORT",
         |         "name" : "${excCodePortNamesMap.getOrElse(
           exc.excursionNumber,
           ""
         )}"
         |       }
         |     }
         |  }
         |}
         |""".stripMargin
    }
  }

  def createNclResponse(reservation: FidelioReservation,
                        cruise: FidelioCruise,
                        itinerary: FidelioItinerary,
                        reservationId: String,
                        transactionId: String,
                        excursionPackages: Iterable[String],
                        eventPackages: Iterable[String]
                        //                        diningResult: Seq[NodeSeq]
  ): String = {

    val guestList = reservation.clients

    val startPort =
      MessageUtil.getPortCodeFromPort(itinerary.items.head.arrivalPortCode)
    val endPort =
      MessageUtil.getPortCodeFromPort(itinerary.items.last.arrivalPortCode)

    s"""
       |{
       |  "ncl_CruiseRetrievePackagesRS" : {
       |    "altLangID" : "en-us",
       |    "primaryLangID" : "en-us",
       |    "sequenceNmbr" : 1,
       |    "target" : "${Settings.environment}",
       |    "transactionIndentifier" : "${transactionId}",
       |    "success": "",
       |    "sailingInfo" : {
       |      "selectedSailing" : {
       |        "duration" : "${MessageUtil.getCruiseDurationFormatted(
         guestList.head
       )}",
       |        "end" : "${DateUtil.formatDate(
         guestList.head.disembarkDate,
         DateUtil.FORMAT_NCL_PACKAGE_DATE
       )}",
       |        "shipCode" : "${MessageUtil.getShipCode(cruise.shipName)}",
       |        "start" : "${DateUtil.formatDate(
         guestList.head.embarkDate,
         DateUtil.FORMAT_NCL_PACKAGE_DATE
       )}",
       |        "voyageID" : "0000",
       |        "CruiseLine" : {
       |          "VendorCode" : "NCL",
       |          "VendorName" : "Norwegian Cruise Lines"
       |        },
       |        "DeparturePort" : {
       |          "LocationCode" : "${startPort}"
       |        },
       |        "ArrivalPort" : {
       |          "LocationCode" : "${endPort}"
       |        }
       |      },
       |      "currency" : {
       |        "currencyCode" : "USD",
       |        "decimalPlaces" : 2
       |      },
       |      "sailingRemarksText" : {
       |        "text" : ${cruise.cruiseName}
       |      },
       |      "selectedCategory" : {
       |        "pricedCategoryCode" : "",
       |        "selectedCabin" : {
       |          "cabinNumber" : "${guestList.head.cabinNumber}"
       |        }
       |      }
       |    },
       |    "guestInfo" : {
       |      "reservationID" : {
       |        "id" : ${reservationId},
       |        "ResInitDateTime" : "",
       |         "StatusCode" : 39
       |      },
       |      "guestDetails" : {
        ${
         var index = 0
         guestList.map { guest =>
           val gender = guest.gender match {
             case "M" => "Male"
             case "F" => "Female"
             case _   => ""
           }
           index += 1
           s"""
           |"GuestDetail" : {
           |  "ContactInfo" : {
           |    "Age" : "",
           |    "BirthDate" : "${if (guest.birthDate.isDefined)
             DateUtil.formatDate(
               guest.birthDate.get,
               DateUtil.FORMAT_NCL_PACKAGE_DATE
             )
           else ""}",
           |"Gender" : "${gender}",
           |"GuestRefNumber" : "${index.toString}",
           |"LoyaltyMembershipID" : "${guest.nclClientId.toString}",
           |"PersonName" : {
           |  "GivenName" : "${guest.name.GivenName.getOrElse("")}",
           |  "Surname" : "${guest.name.Surname.getOrElse("")}",
           |  "NameTitle" : "${guest.name.NameTitle.getOrElse("")}"
           |}
            }
          }"""
         }
       }
       |    },
       |    "Packages" : [
                ${excursionPackages},
                ${eventPackages}
                ${ // TODO: generateDiningPackagesXML(diningResult, reservation)
       }
       |                 ],
       |     "amenityOrders" : "",
       |     "resTransportations" : "",
       |     "standardDinings" : "",
       |     "resTransfers" : ""
       |  }
       |}
       |""".stripMargin
  }

  /*  def createShorexNclResponse(excursions: Try[Seq[FidelioExcursion]],
                              reservation: Try[FidelioReservation],
                              transactionId: String): String = {
    val v = (excursions, reservation) match {
      case (Success(excursions), Success(reservation)) => {
        val guests = reservation.clients

        val numGuests = guests.size

        val packages: Seq[FidelioExcursion] = excursions
          .filter(_.showInITV == true)
          .filter(_.bookingStatus == MessageUtil.STATUS_AVAILABLE)
          .filter(MessageUtil.getNumAvailableSeats(_) >= numGuests)

        createShorexStr(transactionId: String, guests: Seq[FidelioClientAccount], packages: Seq[FidelioExcursion])

      }
      case _ =>
        Failure(
          new Exception(
            "An error occurred retrieving the available shorex packages"
          )
        )
    }
  }*/

  def createShorexStr(transactionId: String,
                      guests: Seq[FidelioClientAccount],
                      packages: Seq[FidelioExcursion]): String = {
    s"""
       |{
       |  "ncl_CruiseGetAvailShorexPackagesRS" : {
       |    "altLangID" : "en-us",
       |    "echoToken" : "String",
       |    "primaryLangID" : "en-us",
       |    "sequenceNmbr" : 1,
       |    "target" : "${Settings.environment}",
       |    "transactionIndentifier" : "${transactionId}",
       |    "success": "",
       |    "packages": ${
         val seq = for (pkg <- packages) yield {
           s"""
              |{
              |  "id": ${pkg.id.toString},
              |  "type": "EXCURSION",
              |  "name": "${pkg.excursionName}",
              |  "code": "${MessageUtil.getExcursionCode(pkg.excursionNumber)}",
              |  "dataRange": {
              |     "packageStartDate": "${DateUtil.formatDate(
                pkg.startDate,
                DateUtil.FORMAT_NCL_DATE
              )}",
              |     "packageEndDate": "${DateUtil.formatDate(
                pkg.endDate,
                DateUtil.FORMAT_NCL_DATE
              )}"
              |  },
              |  "price": ${
                val seq2 = for ((guest, i) <- guests.zipWithIndex) yield {
                  s"""
                     | {
                     |   "guest": {
                     |     "guestRefNumber": ${i + 1},
                     |     ${if (guest.minor == true)
                       s""""packagePrice": {
                                                 "amount": ${CurrencyUtil
                         .convertDollarsToCents(pkg.childPrice)
                         .toString},
                                                  "currencyCode":"USD"
                                               }
                                           """
                     else
                       s""""packagePrice": {
                                                 "amount": ${CurrencyUtil
                         .convertDollarsToCents(pkg.adultPrice)
                         .toString},
                                                  "currencyCode":"USD"
                                               }
                                           """}
                     |   }
                     | }
                     |""".stripMargin
                }
                JsArray(seq2.map(s => Json.parse(s)).toSeq)
              },
              |   "location": {
              |     "from": {
              |        "code": "${MessageUtil
                .getPortCodeFromEventOrExcursionNumber(pkg.excursionNumber)}",
              |        "type": "PORT",
              |        "name": "${pkg.portName}"
              |     },
              |     "to": {
              |       "code": "${MessageUtil
                .getPortCodeFromEventOrExcursionNumber(
                  pkg.excursionNumber
                )}",
              |       "type": "PORT",
              |       "name": "${pkg.portName}"
              |     }
              |   }
              |}
              |""".stripMargin
         }
         JsArray(seq.map(s => Json.parse(s)).toSeq)
       }
       |  }
       |}
        """.stripMargin
  }

}
