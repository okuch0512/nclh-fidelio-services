package controllers.bindables

import play.api.mvc.QueryStringBindable

object ReservationBindables {

  sealed trait ReservationIdType
  final case class ReservationId() extends ReservationIdType
  final case class CabinNumber() extends ReservationIdType

  implicit def queryStringBinder(
      implicit stringBinder: QueryStringBindable[String]
  ): QueryStringBindable[ReservationIdType] =
    new QueryStringBindable[ReservationIdType] {
      override def bind(
          key: String,
          params: Map[String, Seq[String]]
      ): Option[Either[String, ReservationIdType]] = {
        for {
          t <- stringBinder.bind("type", params)
        } yield {
          t match {
            case Right(t) =>
              if (t == "reservationId") Right(ReservationId())
              else if (t == "cabinNumber") Right(CabinNumber())
              else Left("Unable to bind a ReservationIdType: unknown type.")
            case _ =>
              Left(
                "Unable to bind a QueryStringBindable for ReservationBindables"
              )
          }
        }
      }

      override def unbind(key: String,
                          reservationType: ReservationIdType): String = {
        val t = reservationType match {
          case ReservationId() => "reservationId"
          case CabinNumber()   => "cabinNumber"
        }

        stringBinder.unbind("type", t)
      }
    }
}
