package controllers.bindables

import play.api.mvc.QueryStringBindable

object ClientBindables {

  sealed trait ClientIdType
  final case class ClientId() extends ClientIdType
  final case class CardNumber() extends ClientIdType

  implicit def queryStringBinder(
      implicit stringBinder: QueryStringBindable[String]
  ): QueryStringBindable[ClientIdType] =
    new QueryStringBindable[ClientIdType] {
      override def bind(
          key: String,
          params: Map[String, Seq[String]]
      ): Option[Either[String, ClientIdType]] = {
        for {
          t <- stringBinder.bind("type", params)
        } yield {
          t match {
            case Right(t) =>
              if (t == "clientId") Right(ClientId())
              else if (t == "cardNumber") Right(CardNumber())
              else Left("Unable to bind a ClientIdType: unknown type.")
            case _ =>
              Left("Unable to bind a QueryStringBindable for ClientBindables")
          }
        }
      }

      override def unbind(key: String, clientType: ClientIdType): String = {
        val t = clientType match {
          case ClientId()   => "clientId"
          case CardNumber() => "cardNumber"
        }

        stringBinder.unbind("type", t)
      }
    }
}
