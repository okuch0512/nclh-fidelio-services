package controllers.fidelioMock

import java.time.{LocalDate, ZoneOffset}
import java.util.Date

import javax.inject.Inject
import model.{FidelioAmenity, FidelioGuestInfo}
import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.model._
import play.api.Logging
import play.api.libs.json._
import play.api.mvc._

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class FidelioMockController @Inject()(cc: ControllerComponents)
    extends AbstractController(cc)
    with Logging {

  def newDate(y: Int, m: Int, d: Int): Date = {
    val z = ZoneOffset.UTC
    val localDate = LocalDate.of(y, m, d)
    val zdt = localDate.atStartOfDay(z)
    val instant = zdt.toInstant()
    Date.from(instant)
  }

  def JsonGet(psFunction: Option[String],
              psParam: Option[String],
              psSessionId: Option[String]): Action[AnyContent] = Action {
    logger.debug(
      s"FidelioMockController.JsonGet recieved: psFunction=$psFunction, psParam=${psParam.toString}, psSessionID=$psSessionId"
    )

    val params: Option[List[String]] = psParam.map(
      _.replace("[", "")
        .replace("]", "")
        .split(',')
        .map(_.replaceAll("\"", ""))
        .toList
    )

    logger.debug(s"FidelioMockController.JsonGet psFunction: $psFunction")

    psFunction.map(_.replaceAll("\"", "")) match {
      case Some(fn) if fn == FidelioMessageType.LOGIN => getSession(params)
      case Some(fn) if fn == FidelioMessageType.INQUIRE =>
        getFCUIGuestInquiry(params)
      case Some(fn) if fn == FidelioMessageType.EXCFOLIO => getExcFolio(params)
      case Some(fn) if fn == FidelioMessageType.EVENTS   => getEvents(params)
      case Some(fn) if fn == FidelioMessageType.EVENTFOLIO =>
        getEvtFolio(params)
      case Some(fn) if fn == FidelioMessageType.EXCURSIONS =>
        getExcurtion(params)
      case Some(fn) if fn == FidelioMessageType.EVENTBOOKING =>
        eventBooking(params)
      case Some(fn) if fn == FidelioMessageType.BOOKING =>
        eventBooking(params)
      case Some(fn) if fn == FidelioMessageType.GENERIC_PURCHASE =>
        genereticPurchase()
      case Some(fn)
          if fn == FidelioMessageType.WS_FUNCTION
            && params.count(v => v.head == FidelioMessageType.SYSMSGREQ) == 1 =>
        getWSFunction()
      case Some(fn)
          if fn == FidelioMessageType.WS_FUNCTION
            && params
              .count(v => v.head == FidelioMessageType.GUEST_PICTURE) == 1 =>
        getGuestPicture()
      case Some(fn)
          if fn == FidelioMessageType.WS_FUNCTION
            && params.count(
              v => v.head == FidelioMessageType.GET_CRUISE_ITINERARY
            ) == 1 =>
        getPortInfo()
      case Some(fn)
          if fn == FidelioMessageType.WS_FUNCTION
            && params.count(v => v.head == FidelioMessageType.FOLIO) == 1 =>
        getFolio()
      case Some(fn)
          if fn == FidelioMessageType.WS_FUNCTION
            && params
              .count(v => v.head == FidelioMessageType.PACKAGE_FOLIO) == 1 =>
        getPackageFolio()
      case Some(fn)
          if fn == FidelioMessageType.WS_FUNCTION
            && params
              .count(v => v.head == FidelioMessageType.GUEST_KEY_CARD_SCAN) == 1 =>
        getGuestKeyCard()
      case Some(fn) if fn == FidelioMessageType.ADD_ROUTING => getSuccess()
      case Some(fn) if fn == FidelioMessageType.GUEST_AMENITY => getAmenity()

      case _ => NotImplemented("this request is not supported yet")
    }
  }

  private def genereticPurchase(): Result = {
    val guestInfo = FidelioGuestInfo(
      gnAccID = 1,
      geAccountType = 1,
      gbAllowPosting = true,
      gsName = "",
      gdEmbDate = newDate(96, 1, 14),
      gdDisDate = newDate(96, 1, 15),
      gnBalance = 3123213,
      gsFirstName = "first name",
      gsLastName = "last name",
      gsSalutation = "saluation",
      gsTitle = "title",
      gsGender = "female",
      geAgeGroup = 2,
      gsLanguage = "EN",
      gsResStatus = "A",
      gsHandicap = "handicap",
      gsHandicapRemark = "handicap remark",
      gsMusterStation = "muster station",
      gbOnboard = false,
      gePicture = 313324234,
      gsFreqCardNo = "2314-1231-2312-3231",
      gsPriceCategory = "price category",
      gsCabinType = "cabin type",
      gdSysdate = newDate(96, 2, 14),
      gdCurrentCruiseStartDate = newDate(96, 3, 14),
      gnPGID = 231,
      gsCruiseItnID = "212",
      gsResReference = "REF",
      gsExternalID = "exId"
    )
    Ok(
      JsObject(
        Seq(
          "bSuccess" -> JsBoolean(true),
          "sTables" -> JsArray(
            IndexedSeq(
              Json.toJson(guestInfo)
            )
          )
//          "sObj" -> JsString(""),
//          "sErrCode" -> JsString(""),
//          "sErrWord" -> JsString("")
//            JsObject(Seq("PostingTransactionID" -> JsNumber(10)))
        )
      )
    )

  }

  private def getSuccess(): Result = {
    val json = JsObject(
      Seq("bSuccess" -> JsBoolean(true), "sErrMsg" -> JsString(""))
    )
    Ok(json)
  }

  private def getWSFunction(): Result = {
    val cruise = FidelioCruise(
      111,
      "shipName",
      "cruiseName",
      22,
      "onboardCurrency",
      newDate(96, 1, 14),
      newDate(96, 1, 14)
    )

    val json = FidelioCruise.toJson(cruise)

    Ok(json)
  }

  private def getGuestPicture(): Result = {
    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          IndexedSeq(
            "Table0" -> JsObject(
              IndexedSeq(
                "SEC_IMAGE" -> JsString("786875453237890345678908765")
              )
            )
          )
        )
      )
    )
    logger.debug("GUEST-PICTURE: " + json.toString())

    Ok(json)
  }

  private def getExcurtion(params: Option[scala.List[String]]): Result = {

    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          Seq(
            "Excursion" -> JsArray(
              IndexedSeq(
                Json.toJson(
                  FidelioExcursion(
                    213,
                    "RTBX0312",
                    "name",
                    newDate(2019, 9, 3),
                    newDate(2019, 10, 6),
                    10.0,
                    4.0,
                    "portName",
                    3,
                    12,
                    true,
                    "A"
                  )
                )
              )
            )
          )
        )
      )
    )
    Ok(json)
  }

  private def getPortInfo(): Result = {
    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          IndexedSeq(
            "Table0" -> JsArray(
              IndexedSeq(
                JsObject(
                  IndexedSeq(
                    "TYP_COMMENT" -> JsString("arrivalPortName"),
                    "SCD_PORT_ID" -> JsString("arrivalPortCode"),
                    "SCD_DATE" -> JsString("20181008055505")
                  )
                )
              )
            )
          )
        )
      )
    )
    Ok(json)
  }

  private def getEvents(params: Option[scala.List[String]]): Result = {
    val event =
      FidelioEvent(
        23,
        "ENT031",
        "eventName",
        newDate(2019, 9, 1),
        newDate(2019, 9, 2),
        100.0,
        "0000000",
        "1111111",
        2,
        30,
        showInITV = true,
        MessageUtil.STATUS_AVAILABLE
      )

    val tenderEvent =
      FidelioEvent(
        23,
        "TND031",
        "Tender Event Name",
        newDate(2019, 10, 11),
        newDate(2019, 10, 12),
        20.4,
        "0000000",
        "1111111",
        2,
        30,
        showInITV = true,
        MessageUtil.STATUS_AVAILABLE
      )

    val json =
      if (params.nonEmpty && params.get(4) != "") {
        JsObject(
          Seq(
            "bSuccess" -> JsBoolean(true),
            "sTables" ->
              JsObject(
                Seq(
                  "Event" ->
                    Json.toJson(event)
                )
              )
          )
        )
      } else {
        JsObject(
          Seq(
            "bSuccess" -> JsBoolean(true),
            "sTables" ->
              JsObject(
                Seq(
                  "Event" -> JsArray(
                    Seq(
                      Json.toJson(event),
                      Json.toJson(tenderEvent)
                    )
                  )
                )
              )
          )
        )
      }

    Ok(json)
  }

  private def getEvtFolio(params: Option[scala.List[String]]): Result = {
    logger.debug(
      "FidelioMockController.getEvtFolio got params" + params.toString()
    )
    val event = FidelioEventFolio(
      Seq(
        FidelioEventFolioItem(
          3,
          "eventNumber",
          "eventName",
          newDate(96, 1, 14),
          newDate(96, 1, 14),
          1.333,
          2,
          111
        )
      )
    )

    val json = FidelioEventFolio.toJson(event)

    Ok(json)
  }

  def getFolio(): Result = {
    val folio = FidelioFolio(
      ArrayBuffer(
        FidelioFolioItem(
          systemPostingDate = newDate(2019, 9, 3),
          department = "department",
          departmentType = "departmentType",
          subdepartment = "subdepartment",
          amount = 10.0,
          postingTransactionId = 1,
          port = Some("port")
        )
      )
    )

    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sTables" -> JsObject(
          Seq(
            "Table0" ->
              Json.toJson(folio)
          )
        )
      )
    )

    Ok(json)
  }

  def getPackageFolio(): Result = {
    val folio = FidelioFolio(
      ArrayBuffer(
        FidelioFolioItem(
          systemPostingDate = newDate(2019, 9, 3),
          department = "department",
          departmentType = "departmentType",
          subdepartment = "subdepartment2",
          amount = 10.0,
          postingTransactionId = 2,
          port = Some("port")
        )
      )
    )

    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sTables" -> JsObject(
          Seq(
            "Table0" ->
              Json.toJson(folio)
          )
        )
      )
    )

    Ok(json)
  }

  private def getExcFolio(params: Option[scala.List[String]]): Result = {
    logger.debug(
      "FidelioMockController.getExcfolio got params" + params.toString()
    )

    val folio = FidelioExcursionFolio(
      Seq(
        FidelioExcursionFolioItem(
          111,
          "RTBX0312",
          "excursionName",
          newDate(96, 1, 14),
          newDate(96, 1, 14),
          213.23,
          10,
          333
        )
      )
    )

    val json = FidelioExcursionFolio.toJson(folio)

    Ok(json)
  }

  private def getFCUIGuestInquiry(
      params: Option[scala.List[String]]
  ): Result = {
    logger.debug(
      "FidelioMockController.getFCUIGuestInquiry got params" + params.toString()
    )

    val account = FidelioClientAccount(
      12,
      PersonNameType(
        Some("GivenName"),
        Some("NamePrefix"),
        Some("NameTitle"),
        Some("MiddleName"),
        Some("Surname"),
        Some("SurnamePrefix"),
        Some("NameSuffix")
      ),
      Some(newDate(2001, 2, 1)),
      "US",
      "male",
      "some@some.io",
      "8900787977",
      Some(
        PersonAddress("street", "street2", "city", "zip", "state", "country")
      ),
      Some("55"),
      6786,
      879,
      "87979",
      "deck",
      "status",
      false,
      newDate(96, 1, 14),
      newDate(96, 1, 14),
      899879,
      Some("pic")
    )

    Ok(FidelioClientAccount.toJson(account))
  }

  private def getSession(params: Option[scala.List[String]]): Result = {
    val credentials: Option[Tuple2[String, String]] = params.map { x =>
      logger.debug(s"l: ${x(0)}, p: ${x(1)}")
      Tuple2(x(0), x(1))
    }

    val sessionId: String =
      new Random(System.currentTimeMillis()).nextInt().toString()

    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sTables" -> JsArray(
          IndexedSeq(
            JsString(sessionId)
          )
        )
      )
    )

    val results = for {
      c <- credentials
    } yield {
      logger.debug(s"""results l: ${c._1} eq: ${c._1.equals("fidelio")},
                               p: ${c._2} eq: ${c._2.equals(
        "e36819094979ec646d610d6095238f45"
      )}""")

      if (c._1 == "fidelio" && c._2 == "e36819094979ec646d610d6095238f45")
        Ok(json)
      else
        Unauthorized("login&pass incorrect")
    }

    results match {
      case Some(r) => r
      case None    => BadRequest("request parameters is not correct")
    }
  }

  def getGuestKeyCard(): Result = {
    val key = FidelioClientKeyCard(
      folioId = "23",
      title = "Guest Key Card",
      firstName = "Some Name",
      lastName = "Some Last Name",
      latitudes = "40.712776",
      resStatus = "A",
      guestType = "0",
      assemblyStation = "some assembly station",
      cabin = "10102",
      isSpaCabin = false,
      isHaven = false,
      packages = "packages",
      cabinType = "P",
      hasPC = false,
      isAdult = true,
      age = 30,
      ship = " 6 MID SHIP",
      debarkDate = newDate(96, 1, 14),
      classification = "VIP"
    )

    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sTables" -> JsObject(
          Seq(
            "Table0" ->
              FidelioClientKeyCard.toJson(key)
          )
        )
      )
    )

    Ok(json)
  }

  def eventBooking(params: Option[List[String]]): Result = {

    val bookingResp = FidelioBookingResponse(23, 100.50, 3)

    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          IndexedSeq(
            "ExcBooking" -> Json.toJson(bookingResp)
          )
        )
      )
    )

    Ok(json)
  }

  def getAmenity() = {

    val amenities = Seq(
      FidelioAmenity(
        qty = "1",
        code = "INT60",
        descr =  "INTERNET PACKAGE (60 MINUTES)",
        fiddescr = "INTERNET PACKAGE (60 MINUTES)",
        amount = "0",
        deliverLocation = "CABIN",
        dept =  "37",
        deptname = "Financial Department",
        deliver = "20200105000000"
      ),
      FidelioAmenity(
        qty = "1",
        code = "CHSULM",
        descr =  "CHOICE SHORE EXCURSION CREDIT",
        fiddescr = "CHOICE SHORE EXCURSION CREDIT",
        amount = "0",
        deliverLocation = "CABIN",
        dept =  "37",
        deptname = "Financial Department",
        deliver = "20200105000000"
      ),
      FidelioAmenity(
        qty = "1",
        code = "CHI250",
        descr =  "INTERNET PACKAGE (250 MINUTES OR 1000 MB)",
        fiddescr = "INTERNET PACKAGE (250 MINUTES OR 1000 MB)",
        amount = "0",
        deliverLocation = "CABIN",
        dept =  "37",
        deptname = "Financial Department",
        deliver = "20200105000000"
      )
    )

    val json = JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          Seq(
            "Table0" ->
              JsArray(
                amenities.map(Json.toJson(_))
              )
            )
          ),
        "nTotalPage" -> JsNumber(0),
        "sPackageInfo" -> JsString(""),
        "sCrewInvoice" -> JsString(""),
        "sObj" -> JsString(""),
        "sErrCode" -> JsString(""),
        "sErrWord" -> JsString("")
      )
    )

    Ok(json)
  }

}
