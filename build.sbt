import sbt.Keys.libraryDependencies

name := """Nclh-fidelio-ws-service"""
organization := "com.ncl"

version := "1.0-SNAPSHOT"

scalaVersion := "2.12.10"

scalacOptions ++= Seq(
  "-target:jvm-1.8",
  "-encoding",
  "UTF-8",
  "-unchecked",
  "-deprecation",
  "-feature",
  "-Xfuture",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard"
)

routesImport ++= Seq(
  "controllers.bindables.ClientBindables._",
  "controllers.bindables.ClientBindables.ClientIdType",
  "controllers.bindables.ClientBindables.ClientId",
  "controllers.bindables.ReservationBindables._",
  "controllers.bindables.ReservationBindables.ReservationId",
  "controllers.bindables.ReservationBindables.CabinNumber"
)

PlayKeys.devSettings += ("play.http.router", "fidelioMock.Routes")

val playVersion = "2.0.4"
lazy val playDeps = Seq(
  guice,
  caffeine,
  "com.typesafe.play" %% "play-json-joda" % "2.7.3",
  "com.typesafe.play" %% "play-ahc-ws-standalone" % playVersion,
  "com.typesafe.play" %% "play-ws-standalone-xml" % playVersion,
  "com.typesafe.play" %% "play-ws-standalone-json" % playVersion,
  "ai.x" %% "play-json-extensions" % "0.40.2",
  "org.micchon" %% "play-json-xml" % "0.3.0"
)

lazy val common = (project in file("modules/common"))
  .settings(
    libraryDependencies ++= playDeps
  )
  .enablePlugins(PlayScala)

lazy val root = (project in file("."))
  .settings(
    testOptions += Tests.Argument(TestFrameworks.JUnit, "-v", "-q", "-a"),
    testOptions in Test += Tests.Argument("-oD"),
    libraryDependencies ++= playDeps
  )
  .enablePlugins(PlayScala)
  .dependsOn(common)
  .aggregate(common)

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test
