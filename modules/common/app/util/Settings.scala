package modules.common

import com.typesafe.config.ConfigFactory

/**
  * Created by matt on 12/21/16.
  */
object Settings {

  val config = ConfigFactory.load()

  val fidelioURL = config.getString("fidelio-url")
  val fidelioUsername = config.getString("fidelio-username")
  val fidelioPassword = config.getString("fidelio-password")
  val fidelioTokenTimeout = config.getInt("fidelio-tokenTimeout")
  val environment = config.getString("environment")
  val cacheDefaultTimeout = config.getInt("cache-default-timeout")
  val cacheEphemeralTimeout = config.getInt("cache-ephemeral-timeout")
}
