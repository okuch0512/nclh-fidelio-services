package modules.data_mapper.service

import modules.common.Settings
import modules.data_mapper.model.{Address, FidelioGuest}
import modules.data_mapper.util.{CurrencyUtil, MessageUtil}
import modules.fidelio_io.model._
import modules.fidelio_io.util.DateUtil

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, NodeSeq}

/**
  * Created by matt on 12/22/15.
  */
object NCL_CruiseRetrievePackagesProcessor {

  def process(
      nclRequest: NodeSeq
  )(implicit ec: ExecutionContext): Future[Try[NodeSeq]] = {
    val reservationId = (nclRequest \ "ReservationID" \ "@ID").text.trim
    val transactionId = (nclRequest \ "@TransactionIdentifier").text.trim

    for {
      reservationResult <- MessageUtil.retrieveReservation(reservationId)

      cruiseResult <- MessageUtil.retrieveCruise

      finalResult <- retrievePackages(
        reservationResult,
        cruiseResult,
        reservationId,
        transactionId
      )

    } yield finalResult
  }

  private def retrievePackages(
      reservationResult: Try[FidelioReservation],
      cruiseResult: Try[FidelioCruise],
      reservationId: String,
      transactionId: String
  )(implicit ec: ExecutionContext): Future[Try[NodeSeq]] = {
    (reservationResult, cruiseResult) match {
      case (Success(reservation), Success(cruise)) => {
        for {
          itineraryResult <- MessageUtil.retrieveItinerary(
            reservation.clients.head
          )

          eventFoliosResult <- MessageUtil.retrieveEventFolios(reservation)

          excFoliosResult <- MessageUtil.retrieveExcursionFolios(reservation)

          excursionsResult <- MessageUtil.retrieveExcursions(reservation)

          diningFoliosResult <- MessageUtil.retrieveDiningFolios(reservation)

        } yield composeResponse(
          reservation,
          cruise,
          itineraryResult,
          eventFoliosResult,
          excFoliosResult,
          excursionsResult,
          diningFoliosResult,
          reservationId,
          transactionId
        )
      }
      case _ =>
        Future(
          Failure(
            new Exception(
              s"Error retrieving packages for reservation id ${reservationId}"
            )
          )
        )
    }
  }

  private def composeResponse(reservation: FidelioReservation,
                              cruise: FidelioCruise,
                              itineraryResult: Try[FidelioItinerary],
                              eventFoliosResult: Try[Seq[FidelioEventFolio]],
                              excFoliosResult: Try[Seq[FidelioExcursionFolio]],
                              excursionsResult: Try[Seq[FidelioExcursion]],
                              diningFoliosResult: Try[Seq[NodeSeq]],
                              reservationId: String,
                              transactionId: String): Try[NodeSeq] = {

    (
      itineraryResult,
      eventFoliosResult,
      excFoliosResult,
      excursionsResult,
      diningFoliosResult
    ) match {
      case (
          Success(itinerary),
          Success(eventFolios),
          Success(excFolios),
          Success(excursions),
          Success(diningFolios)
          ) => {
        val processResult = createPackageResultMaps(
          reservation,
          eventFolios,
          excFolios,
          Some(excursions)
        )

        // a map of guests keyed by the Fidelio account id
        val guestsMap: mutable.Map[Int, FidelioGuest] = processResult._1

        // a map of purchased events keyed by the event id
        val purchasedEventsMap: mutable.Map[Int, FidelioEventFolioItem] =
          processResult._2

        // a map of purchased excursions keyed by the excursion id
        val purchasedExcursionsMap
            : mutable.Map[Int, FidelioExcursionFolioItem] = processResult._3

        // a map of port names keyed by the excursion code
        val excCodePortNamesMap: mutable.Map[String, String] = processResult._4

        val eventPackages = buildEventPackages(purchasedEventsMap, guestsMap)
        val excursionPackages = buildExcursionPackages(
          purchasedExcursionsMap,
          guestsMap,
          excCodePortNamesMap
        )

        Success(
          createNclResponse(
            reservation,
            cruise,
            itinerary,
            reservationId,
            transactionId,
            excursionPackages,
            eventPackages,
            diningFolios
          )
        )
      }

      case _ =>
        Failure(
          new Exception(
            s"Error retrieving packages for reservation id ${reservationId}"
          )
        )
    }
  }

  def createPackageResultMaps(
      reservation: FidelioReservation,
      eventFoliosResult: Seq[FidelioEventFolio],
      excFoliosResult: Seq[FidelioExcursionFolio],
      excursions: Option[Seq[FidelioExcursion]]
  ): (mutable.Map[Int, FidelioGuest],
      mutable.Map[Int, FidelioEventFolioItem],
      mutable.Map[Int, FidelioExcursionFolioItem],
      mutable.Map[String, String]) = {
    // a map of guests keyed by the Fidelio account id
    val guestsMap = scala.collection.mutable.Map[Int, FidelioGuest]()

    // retrieve all the Fidelio accounts for the reservation
    val accounts = reservation.clients

    var guestRef = 1

    for (account <- accounts) {
      val guest = createGuest(guestRef, account)

      guestsMap(guest.fidelioAccountId) = guest

      guestRef += 1
    }

    // a map of purchased events keyed by the event id
    val purchasedEventsMap =
      scala.collection.mutable.Map[Int, FidelioEventFolioItem]()

    eventFoliosResult foreach { folio =>
      if (folio.items.size > 0) {
        val fidelioAccountId = folio.items(0).fidelioAcctId
        val guest = guestsMap(fidelioAccountId)
        guest.eventFolio = folio

        folio.items foreach { ev =>
          val eventId = ev.id

          if (!purchasedEventsMap.contains(eventId)) {
            purchasedEventsMap(eventId) = ev
          }
        }
      }
    }

    // a map of purchased excursions keyed by the excursion id
    val purchasedExcursionsMap =
      scala.collection.mutable.Map[Int, FidelioExcursionFolioItem]()

    excFoliosResult foreach { folio =>
      if (folio.items.size > 0) {
        val fidelioAccountId = folio.items(0).fidelioAcctId
        val guest = guestsMap(fidelioAccountId)
        guest.excFolio = folio

        folio.items foreach { exc =>
          val eventId = exc.id

          if (!purchasedExcursionsMap.contains(eventId)) {
            purchasedExcursionsMap(eventId) = exc
          }
        }
      }
    }

    // a map of port names keyed by the excursion code
    val excCodePortNamesMap = scala.collection.mutable.Map[String, String]()

    excursions match {
      case Some(excs) => {
        excs foreach { exc =>
          val portName = exc.portName
          val excCode = exc.excursionNumber

          if (!excCodePortNamesMap.contains(excCode)) {
            excCodePortNamesMap(excCode) = portName
          }
        }
      }
      case None => // do nothing
    }

    (guestsMap, purchasedEventsMap, purchasedExcursionsMap, excCodePortNamesMap)
  }

  private def createGuest(guestRef: Int,
                          account: FidelioClientAccount): FidelioGuest = {
    val fidelioAccountId = account.fidelioAcctId
    val firstName = account.name.GivenName.get
    val lastName = account.name.Surname.get
    val title = account.name.NameTitle.get
    val cabin = account.cabinNumber
    val gender = account.gender
    val telephone = if (account.phone.isEmpty) "" else account.phone

    var address1 = ""
    var address2 = ""
    var city = ""
    var state = ""
    var zip = ""
    var country = ""

    if (account.address.isDefined) {
      address1 = account.address.get.street
      address2 = account.address.get.street2
      city = account.address.get.city
      state = account.address.get.state
      zip = account.address.get.zip
      country = account.address.get.country
    }

    val address = new Address(address1, address2, city, state, zip, country)

    val email = account.email

    new FidelioGuest(
      fidelioAccountId,
      guestRef,
      firstName,
      lastName,
      title,
      cabin,
      gender,
      telephone,
      email,
      address
    )
  }

  def findEventInFolio(
      id: Int,
      folio: FidelioEventFolio
  ): Option[FidelioEventFolioItem] = {
    var matchingEvent: Option[FidelioEventFolioItem] = None
    var foundEvent = false

    for (e <- folio.items) {
      if (!foundEvent && id.equals(e.id)) {
        matchingEvent = Some(e)
        foundEvent = true
      }
    }

    matchingEvent
  }

  def findExcursionInFolio(
      id: Int,
      folio: FidelioExcursionFolio
  ): Option[FidelioExcursionFolioItem] = {
    var matchingExcursion: Option[FidelioExcursionFolioItem] = None
    var foundExcursion = false

    for (e <- folio.items) {
      if (!foundExcursion && id.equals(e.id)) {
        matchingExcursion = Some(e)
        foundExcursion = true
      }
    }

    matchingExcursion
  }

  def buildEventPackages(
      purchasedEvents: scala.collection.mutable.Map[Int, FidelioEventFolioItem],
      guestsMap: scala.collection.mutable.Map[Int, FidelioGuest]
  ): Iterable[Elem] = {
    for ((eventId, ev) <- purchasedEvents) yield {
      <Package ID={eventId.toString} PackageTypeCode="4">
                {
        if (MessageUtil.isTenderTicket(ev))
          <Type>TENDER_TICKET</Type>
        else
          <Type>ENTERTAINMENT</Type>
      }
                <Name>{ev.eventName}</Name>
                <Code>{MessageUtil.getEventCode(ev.eventNumber)}</Code>
                <Comments>{ev.eventName}</Comments>
                <DateRange>
                    <PackageStartDate>{
        DateUtil.formatDate(ev.startDate, DateUtil.FORMAT_NCL_DATE)
      }</PackageStartDate>
                    <PackageEndDate>{
        DateUtil.formatDate(ev.endDate, DateUtil.FORMAT_NCL_DATE)
      }</PackageEndDate>
                </DateRange>
                <Price>
                    {
        for ((fidelioAccountId, guest) <- guestsMap) yield {
          val eventForGuest = findEventInFolio(eventId, guest.eventFolio)

          if (eventForGuest != None && eventId.equals(eventForGuest.get.id)) {
            <Guest>
                                <GuestRefNumber>{guest.guestRefNumber}</GuestRefNumber>
                                <PackagePrice Amount={
              CurrencyUtil
                .convertDollarsToCents(eventForGuest.get.bookingValue)
                .toString
            } CurrencyCode="USD"/>
                            </Guest>
          }
        }
      }
                </Price>
                <Location>
                    <From>
                        <Code>{
        MessageUtil.getPortCodeFromEventOrExcursionNumber(ev.eventNumber)
      }</Code>
                        <Type>SHIP</Type>
                    </From>
                    <To>
                        <Code></Code>
                        <Type></Type>
                    </To>
                </Location>
            </Package>
    }
  }

  def buildExcursionPackages(
      purchasedExcursions: scala.collection.mutable.Map[
        Int,
        FidelioExcursionFolioItem
      ],
      guestsMap: scala.collection.mutable.Map[Int, FidelioGuest],
      excCodePortNamesMap: scala.collection.mutable.Map[String, String]
  ): Iterable[Elem] = {
    for ((excId, exc) <- purchasedExcursions) yield {
      <Package ID={excId.toString} PackageTypeCode="4">
                <Type>SHORE EXCURSION</Type>
                <Name>{exc.excursionName}</Name>
                <Code>{MessageUtil.getExcursionCode(exc.excursionNumber)}</Code>
                <Comments>{exc.excursionName}</Comments>
                <DateRange>
                    <PackageStartDate>{
        DateUtil.formatDate(exc.startDate, DateUtil.FORMAT_NCL_DATE)
      }</PackageStartDate>
                    <PackageEndDate>{
        DateUtil.formatDate(exc.endDate, DateUtil.FORMAT_NCL_DATE)
      }</PackageEndDate>
                </DateRange>
                <Price>
                    {
        for ((fidelioAccountId, guest) <- guestsMap) yield {
          val excForGuest = findExcursionInFolio(excId, guest.excFolio)

          if (excForGuest != None && excId.equals(excForGuest.get.id)) {
            <Guest>
                                <GuestRefNumber>{guest.guestRefNumber}</GuestRefNumber>
                                <PackagePrice Amount={
              CurrencyUtil
                .convertDollarsToCents(excForGuest.get.bookingValue)
                .toString
            } CurrencyCode="USD"/>
                            </Guest>
          }
        }
      }
                </Price>
                <Location>
                    <From>
                        <Code>{
        MessageUtil.getPortCodeFromEventOrExcursionNumber(exc.excursionNumber)
      }</Code>
                        <Type>PORT</Type>
                        <Name>{excCodePortNamesMap.get(exc.excursionNumber).get}</Name>
                    </From>
                    <To>
                        <Code>{
        MessageUtil.getPortCodeFromEventOrExcursionNumber(exc.excursionNumber)
      }</Code>
                        <Type>PORT</Type>
                        <Name>{excCodePortNamesMap.get(exc.excursionNumber).get}</Name>
                    </To>
                </Location>
            </Package>
    }
  }

  def generateDiningPackagesXML(
      diningResultList: Seq[NodeSeq],
      reservation: FidelioReservation
  ): Seq[NodeSeq] = {
    diningResultList map { diningResult =>
      val reservations = (diningResult \ "SWCalendarResult" \ "Reservations" \ "Reservation" \ "Reservation")

      reservations map { res =>
        val forDateStr = (res \ "ReservationDate").text.trim
        var resTime = (res \ "ReservationTime").text.trim
        val restaurant = (res \ "Restuarant").text.trim
        val comment = (res \ "RestaurtName").text.trim
        val partySize = (res \ "Guests").text.trim
        val mealPeriod = (res \ "MealPeriod").text.trim
        val packageId = "0"
        val year = forDateStr.substring(0, 4)
        val month = forDateStr.substring(4, 6)
        val day = forDateStr.substring(6, 8)
        val packageCode = "%s%03d".format(restaurant, partySize.toInt)
        val guestId = (res \ "GuestID").text.trim

        val guestRefNumber =
          MessageUtil.findGuestRefNbrForFidelioAcctId(guestId, reservation)

        //Sometimes the time is 3 digits if before 10AM
        if (resTime.length == 3) {
          resTime = "0" + resTime
        }
        //Sometimes its just one digit
        if (resTime.length == 1) {
          resTime = "000" + resTime
        }

        val hour = resTime.substring(0, 2)
        val minute = resTime.substring(2, 4)

        <Package ID={packageId} PackageTypeCode="4">
                    <Type>DINING</Type>
                    <Name>{restaurant}</Name>
                    <Code>{packageCode}</Code>
                    <Comments>{comment}</Comments>
                    <DateRange>
                        <PackageStartDate>{year}-{month}-{day}T{hour}:{minute}:00.000-05:00</PackageStartDate>
                        <PackageEndDate>{year}-{month}-{day}T{hour}:{minute}:00.000-05:00</PackageEndDate>
                    </DateRange>
                    <Price>
                        <Guest>
                            <GuestRefNumber>{guestRefNumber.get}</GuestRefNumber>
                            <PackagePrice Amount="0" CurrencyCode="USD"/>
                        </Guest>
                    </Price>
                    <TableSize>{partySize}</TableSize>
                </Package>
      }
    }
  }

  private def createNclResponse(reservation: FidelioReservation,
                                cruise: FidelioCruise,
                                itinerary: FidelioItinerary,
                                reservationId: String,
                                transactionId: String,
                                excursionPackages: Iterable[Elem],
                                eventPackages: Iterable[Elem],
                                diningResult: Seq[NodeSeq]): NodeSeq = {

    val guestList = reservation.clients

    val startPort =
      MessageUtil.getPortCodeFromPort(itinerary.items.head.arrivalPortCode)
    val endPort =
      MessageUtil.getPortCodeFromPort(itinerary.items.last.arrivalPortCode)

    <NCL_CruiseRetrievePackagesRS xmlns="http://nclapi/schemas" AltLangID="en-us" PrimaryLangID="en-us" SequenceNmbr="1" Target={
      Settings.environment
    } TransactionIdentifier={transactionId} Version="3.1">
            <Success/>
            <SailingInfo>
                <SelectedSailing Duration={
      MessageUtil.getCruiseDurationFormatted(guestList.head)
    } End={
      DateUtil.formatDate(
        guestList.head.disembarkDate,
        DateUtil.FORMAT_NCL_PACKAGE_DATE
      )
    } ShipCode={MessageUtil.getShipCode(cruise.shipName)} Start={
      DateUtil.formatDate(
        guestList.head.embarkDate,
        DateUtil.FORMAT_NCL_PACKAGE_DATE
      )
    } VoyageID="0000">
                    <CruiseLine VendorCode="NCL" VendorName="Norwegian Cruise Lines"/>
                    <DeparturePort LocationCode={startPort}/>
                    <ArrivalPort LocationCode={endPort}/>
                </SelectedSailing>
                <Currency CurrencyCode="USD" DecimalPlaces="2"/>
                <SailingRemarksText>
                    <Text>{cruise.cruiseName}</Text>
                </SailingRemarksText>
                <SelectedCategory PricedCategoryCode="">
                    <SelectedCabin CabinNumber={guestList.head.cabinNumber}/>
                </SelectedCategory>
            </SailingInfo>
            <GuestInfo>
                <ReservationID ID={reservationId} ResInitDateTime="" StatusCode="39"/>
                <GuestDetails>
                    {
      var index = 0
      guestList.map { guest =>
        val gender = guest.gender match {
          case "M" => "Male"
          case "F" => "Female"
          case _   => ""
        }
        index += 1
        <GuestDetail>
                        <ContactInfo Age="" BirthDate={
          if (guest.birthDate.isDefined)
            DateUtil.formatDate(
              guest.birthDate.get,
              DateUtil.FORMAT_NCL_PACKAGE_DATE
            )
          else ""
        } Gender={gender} GuestRefNumber={index.toString} LoyaltyMembershipID={
          guest.nclClientId.toString
        }>
                            <PersonName>
                                <GivenName>{guest.name.GivenName.getOrElse("")}</GivenName>
                                <Surname>{guest.name.Surname.getOrElse("")}</Surname>
                                <NameTitle>{guest.name.NameTitle.getOrElse("")}</NameTitle>
                            </PersonName>
                        </ContactInfo>
                    </GuestDetail>
      }
    }
                </GuestDetails>
            </GuestInfo>
            <Packages>
                {excursionPackages}
                {eventPackages}
                {generateDiningPackagesXML(diningResult, reservation)}
            </Packages>
            <AmenityOrders/>
            <ResTransportations/>
            <StandardDinings/>
            <ResTransfers/>
        </NCL_CruiseRetrievePackagesRS>
  }
}
