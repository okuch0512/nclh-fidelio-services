package modules.fidelio_io.util

/**
  * Created by matt on 1/16/15.
  */
object FidelioStringGenerator {

  def randomNumericString(length: Int) = {
    val r = new scala.util.Random
    val sb = new StringBuilder

    for (i <- 1 to length) {
      sb.append(r.nextInt(9))
    }

    sb.toString
  }
}
