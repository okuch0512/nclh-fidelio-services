package modules.common.core.api.communication

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.util.ByteString
import play.api.Logger
import play.api.libs.json.{JsValue, _}
import play.api.libs.ws.JsonBodyReadables._
import play.api.libs.ws.JsonBodyWritables._
import play.api.libs.ws._
import play.api.libs.ws.ahc._

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class HTTPDriver {}

object HTTPDriver {

  private val logger = Logger(classOf[HTTPDriver])

  implicit val writeableOf_Map: BodyWritable[Map[String, Seq[String]]] = {
    BodyWritable(
      m => InMemoryBody(ByteString.fromString(Json.toJson(m).toString())),
      "application/json"
    )
  }

  // Create Akka system for thread and streaming management
  implicit val system = ActorSystem()
  system.registerOnTermination {
    System.exit(0)
  }
  implicit val materializer = ActorMaterializer()

  // Create the standalone WS client
  // no argument defaults to a AhcWSClientConfig created from
  // "AhcWSClientConfigFactory.forConfig(ConfigFactory.load, this.getClass.getClassLoader)"
  val ws = StandaloneAhcWSClient()

  def getJSON(url: String, useCache: Boolean = false): Future[Try[JsValue]] = {

    try {
      ws.url(url).get.map { response =>
        try {
          Success(response.body[JsValue])
        } catch {
          case ex: Throwable => Failure(ex)
        }
      }
    } catch {
      case ex: Throwable => {
        logger.error(
          "getJSON is failing for the following URL: " + url.toString
        )
        Future.successful(Failure(ex))
      }
    }
  }

  def postAndGetJSON(url: String,
                     params: Map[String, Seq[String]]): Future[JsValue] = {
    ws.url(url).post(params).map { res =>
      res.body[JsValue]
    }
  }

  def postJSON(url: String, params: JsValue): Future[Try[JsValue]] = {
    ws.url(url).post(params).map { response =>
      try {
        Success(response.body[JsValue])
      } catch {
        case ex: Throwable => Failure(ex)
      }
    }
  }
}
