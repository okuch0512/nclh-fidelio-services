package util

import akka.actor.ActorSystem
//import com.newrelic.api.agent.NewRelic
import javax.inject.Singleton
import modules.fidelio_io.model.FidelioMessage
//import modules.newRelic.NewRelicFidelioEvent
//import util.FidelioGuardian.Protocol.FidelioRequest

case class FidelioFailureException(msg: String,
                                   failureMsg: FidelioMessage,
                                   failureId: Option[String])
    extends IllegalArgumentException(msg)

@Singleton
class FidelioDriver {

  var tcpConnectionCounter = 0

  val actorSystem = ActorSystem("FidelioActorSystem")
//    val fidelioGuardian:ActorRef = actorSystem.actorOf(Props[FidelioGuardian],"Fidelio-Guardian")

  /*    def sendMessage(msg: FidelioMessage)(implicit executionContext: ExecutionContext): Future[Try[FidelioMessage]] = {
		val promise = Promise[FidelioMessage]()
        fidelioGuardian ! FidelioRequest(msg, promise)
        val start = new org.joda.time.DateTime

        //Convert the promise to Future[Try[FidelioMessage]]
        promise.future.map({ responseMsg =>
            val end = new org.joda.time.DateTime
            val elapsed = new org.joda.time.Duration(start, end).getMillis

            if (responseMsg.msgType.equals("Failure")) {
                val failureId = responseMsg.get(FidelioMessageUnit.FID)
                val errorMessage:String = responseMsg.get("INF").getOrElse("Received a Failure response.")

                if(!Settings.errorMessagesToIgnore.contains(errorMessage)) {
                    Logger.error(s"${msg.msgType}: " + elapsed.toString + "ms " + msg.units.toString())
                    Logger.error(s"Fidelio returned failure response for: ${msg.msgType}. ${errorMessage}")
                    logFidelioError(msg.msgType, msg.toString, "false", new Exception(s"Fidelio returned failure response for: ${msg.msgType}. ${errorMessage}"))
                }
                Logger.debug( s"${msg.msgType}: " + elapsed.toString + "ms " + msg.units.toString()  )
                Failure(new FidelioFailureException(
                    errorMessage,
                    responseMsg,
                    failureId
                ))
            } else {
                recordNewRelicMetrics(msg, responseMsg, elapsed)
                Logger.debug( s"${msg.msgType}: " + elapsed.toString + "ms " + msg.units.toString()  )
                Success(responseMsg)
            }
        }).recover {
            case ex =>
                logFidelioError(msg.msgType, msg.toString, "false", ex)
                Failure(ex)
        }
    }*/

  /*    private def logFidelioError(msgName: String,
                                request: String,
                                response: String,
                                ex: Throwable):Unit = {

        val map: java.util.Map[String, String] = new java.util.HashMap[String, String]
        map.put("msgName", msgName)
        map.put("request", request)
        map.put("response", response)

        NewRelic.noticeError(ex, map)
    }*/

  /*    private def recordNewRelicMetrics(inputMessage: FidelioMessage, outputMessage: FidelioMessage, elapsed: Long):Unit = {

        try {
            NewRelic.recordResponseTimeMetric(s"Custom/${Settings.ship}AdapterFIDELIO/${inputMessage.msgType}", elapsed)
            val event = NewRelicFidelioEvent(inputMessage, outputMessage, elapsed)
            NewRelic.getAgent.getInsights.recordCustomEvent(s"${Settings.ship}-ADAPTER-FIDELIO", event.getAttributesMap)

        } catch {
            case ex: Throwable =>
                play.api.Logger.error(s"Can't send metric to New Relic error=${ex.toString}")
        }

    }*/
}
