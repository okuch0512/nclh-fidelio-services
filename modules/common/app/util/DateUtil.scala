package modules.fidelio_io.util

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import org.joda.time.format.{DateTimeFormat, DateTimeFormatterBuilder}

/**
  * Created by matt on 1/4/16.
  */
object DateUtil {

  val FORMAT_FIDELIO_DATE = "yyyyMMddHHmmss"
  val FORMAT_FIDELIO_TIME = "HHmm"
  val FORMAT_NCL_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
  val FORMAT_NCL_DATE_WITH_SLASHES = "MM/dd/yyyy"
  val FORMAT_NCL_TIME = "HH:mm:ss"
  val FORMAT_NCL_TIME_WITH_AM_PM = "h:mm a"
  val FORMAT_NCL_PACKAGE_DATE = "yyyy-MM-dd"
  val FORMAT_NCL_TRANSACTION_ID_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ss"
  val FORMAT_SILVERWEB_DATE = "yyyyMMdd"
  val FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss"
  val FORMAT_FIDELIO_PACKAGE_DATE = "yyyy-MM-dd"
  val FORMAT_NCL_DATE_JODA = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
  val FORMAT_FIDELIO_ENTERTAINMENT_DATE = "yyyy-MM-dd HH:mm:ss.S"

  /**
    * Converts date in one format to another format
    *
    * @param date date
    * @param fromFormat
    * @param toFormat
    * @return date in another format
    */
  def convertDate(date: String,
                  fromFormat: String,
                  toFormat: String): String = {
    if (date == null || date.length == 0) {
      return date
    }

    val formatter = new SimpleDateFormat(fromFormat)
    val dt = formatter.parse(date)

    formatDate(dt, toFormat)
  }

  /**
    * Converts date to supplied format
    *
    * @param date date
    * @param format format
    * @return date in supplied format
    */
  def formatDate(date: Date, format: String): String = {
    val formatter = new SimpleDateFormat(format)
    formatter.format(date)
  }

  def parseDate(dateStr: String, format: String): Date = {
    val formatter = new SimpleDateFormat(format)

    formatter.parse(dateStr)
  }

  /**
    * Adds number of days passed in to date passed in.
    *
    * @param date
    * @param numDays
    * @return new date
    */
  def addDays(date: Date, numDays: Int): Date = {
    val calendar = Calendar.getInstance()
    calendar.setTime(date)
    calendar.add(Calendar.DATE, numDays)
    calendar.getTime
  }

  val parsers = Array(
    DateTimeFormat.forPattern(FORMAT_FIDELIO_DATE).getParser,
    DateTimeFormat.forPattern(FORMAT_FIDELIO_PACKAGE_DATE).getParser,
    DateTimeFormat.forPattern(FORMAT_NCL_DATE_JODA).getParser,
    DateTimeFormat.forPattern(FORMAT_NCL_DATE_WITH_SLASHES).getParser,
    DateTimeFormat.forPattern(FORMAT_NCL_TRANSACTION_ID_TIMESTAMP).getParser,
    DateTimeFormat.forPattern(FORMAT_FIDELIO_ENTERTAINMENT_DATE).getParser
  )

  val ultimateFormatter = new DateTimeFormatterBuilder()
    .append(null, parsers)
    .toFormatter

}
