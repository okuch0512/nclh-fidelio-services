package modules.data_mapper.service

import modules.data_mapper.util.{CurrencyUtil, MessageUtil}
import modules.fidelio_io.model.{
  FidelioClientAccount,
  FidelioFolio,
  FidelioPostingType
}
import modules.fidelio_io.util.DateUtil
import play.api.Logger

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.xml.NodeSeq

class OnboardRetrieveAccountFolioProcessor {}

/**
  * Created by matt on 2/23/16.
  */
object OnboardRetrieveAccountFolioProcessor {

  private val logger = Logger(classOf[OnboardRetrieveAccountFolioProcessor])

  def process(
      nclRequest: NodeSeq
  )(implicit ec: ExecutionContext): Future[Try[NodeSeq]] = {
    val reservationId = (nclRequest \ "ReservationID").text.trim
    val clientId = (nclRequest \ "LoyaltyMembershipID").text.trim

    for {
      clientResult <- MessageUtil.retrieveClient(clientId)

      folioResult <- {
        clientResult match {
          case Success(client) => {
            if (client.reservationId != reservationId.toInt) {
              val msg =
                s"ReservationID does not match reservation id for client with LoyaltyMembershipID ${clientId}."

              logger.error(msg)
              Future(Failure(new IllegalArgumentException(msg)))
            } else {
              val folio = MessageUtil.retrieveFolio(client.fidelioAcctId)
              folio
            }
          }

          case Failure(ex) =>
            Future(Failure(ex.fillInStackTrace()))
        }
      }

    } yield createNclResponse(
      folioResult,
      clientResult,
      reservationId,
      clientId
    )
  }

  def createNclResponse(folioMsg: Try[FidelioFolio],
                        clientMsg: Try[FidelioClientAccount],
                        reservationId: String,
                        clientId: String): Try[NodeSeq] = {

    (folioMsg, clientMsg) match {
      case (Success(folioMsg), Success(clientMsg)) => {
        val xml = <Onboard_RetrieveAccountFolioRS>
                    <ReservationID>{clientMsg.reservationId}</ReservationID>
                    <GuestDetail>
                        <NamePrefix>{clientMsg.name.NameTitle.getOrElse("")}</NamePrefix>
                        <GivenName>{clientMsg.name.GivenName.getOrElse("")}</GivenName>
                        <Surname>{clientMsg.name.Surname.getOrElse("")}</Surname>
                        <LoyaltyMembershipID>{clientMsg.nclClientId.toString}</LoyaltyMembershipID>
                    </GuestDetail>
                    <TotalDue>{CurrencyUtil.formatAmount(clientMsg.balance)}</TotalDue>
                    <FolioItems>{
          folioMsg.items.map { item =>
            <FolioItem>
                            <Date>{
              DateUtil.formatDate(
                item.systemPostingDate,
                DateUtil.FORMAT_DATE_TIME
              )
            }</Date>
                            <Item>{
              if (item.department.isEmpty) "" else item.department
            }</Item>
                            {
              val postingType =
                if (item.departmentType == null) FidelioPostingType.DEBIT
                else item.departmentType

              if (FidelioPostingType.DEBIT.equals(postingType)) {
                <Amount>{CurrencyUtil.formatAmount(item.amount)}</Amount>
                                  <PostingType>DEBIT</PostingType>
              } else if (FidelioPostingType.CREDIT.equals(postingType)) {
                <Amount>{item.amount}</Amount>
                                  <PostingType>CREDIT</PostingType>
              }
            }
                            <Subdepartment>{item.subdepartment}</Subdepartment>
                            <Port>{item.port.getOrElse("").trim}</Port>
                            <PostingTransactionID>{item.postingTransactionId}</PostingTransactionID>
                        </FolioItem>
          }
        }</FolioItems>
                </Onboard_RetrieveAccountFolioRS>

        Success(xml)
      }
      case (Failure(e1), Failure(e2)) =>
        Failure(
          new Exception(
            s"An error occurred retrieving the folio " +
              s"${e1.getStackTrace.mkString("\n")}" +
              s"${e2.getStackTrace.mkString("\n")}"
          )
        )
      case (_, Failure(e)) =>
        Failure(
          new Exception(
            s"An error occurred retrieving the folio " +
              s"${e.getStackTrace.mkString("\n")}"
          )
        )
      case (Failure(e), _) =>
        Failure(
          new Exception(
            s"An error occurred retrieving the folio " +
              s"${e.getStackTrace.mkString("\n")}"
          )
        )

    }
  }
}
