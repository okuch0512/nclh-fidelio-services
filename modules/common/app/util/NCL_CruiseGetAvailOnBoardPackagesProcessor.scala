package modules.data_mapper.service

import java.util.{Calendar, Date}

import modules.common.Settings
import modules.data_mapper.util.{CurrencyUtil, MessageUtil}
import modules.fidelio_io.model.{FidelioEvent, FidelioReservation}
import modules.fidelio_io.util.DateUtil

import scala.xml.NodeSeq

/**
  * Created by matt on 12/22/15.
  */
object NCL_CruiseGetAvailOnBoardPackagesProcessor {

  def createPackageNclResponse(events: Seq[FidelioEvent],
                               reservation: FidelioReservation,
                               transactionId: String,
                               packageType: String): NodeSeq = {
    val guests = reservation.clients

    val numGuests = guests.size

    // only return packages which are supposed to be displayed in an external booking system,
    // have an Available status, and have enough seats available for all the guests in the reservation
    var packages: Seq[FidelioEvent] = events
      .filter(_.showInITV == true)
      .filter(_.bookingStatus == MessageUtil.STATUS_AVAILABLE)
      .filter(
        MessageUtil.getNumAvailableSeats(_) >= numGuests
      )

    var packageCode = ""

    if (packageType == MessageUtil.PACKAGE_ENTERTAINMENT) {
      //Exclude tender tickets
      packages = packages.filter(!MessageUtil.isTenderTicket(_))
    } else if (packageType == MessageUtil.PACKAGE_TENDER_TICKET) {
      //Include tender tickets
      packages = packages.filter(MessageUtil.isTenderTicket(_))
    }

    <NCL_CruiseGetAvailOnBoardPackagesRS AltLangID="en-us" EchoToken="String" PrimaryLangID="en-us" SequenceNmbr="1" Target={
      Settings.environment
    } TransactionIdentifier={transactionId} Version="3.1" xmlns="http://nclapi/schemas">
            <Success/>
            <Warnings/>
            <Packages>
                {
      packages.map { pkg =>
        // Making sure the package code for all tender tickets on the same day
        // is identical, so the tender tickets can be grouped together.
        if (packageType == MessageUtil.PACKAGE_TENDER_TICKET) {
          packageCode = "TND" + DateUtil.formatDate(
            pkg.startDate,
            DateUtil.FORMAT_SILVERWEB_DATE
          )
        } else {
          packageCode = MessageUtil.getEventCode(pkg.eventNumber)
        }

        <Package ID={pkg.id.toString}>
                    <Type>{packageType}</Type>
                    <Name>{pkg.eventName}</Name>
                    <Code>{packageCode}</Code>
                    <DateRange>
                        <PackageStartDate>{
          DateUtil.formatDate(pkg.startDate, DateUtil.FORMAT_NCL_DATE)
        }</PackageStartDate>
                        <PackageEndDate>{
          DateUtil.formatDate(pkg.endDate, DateUtil.FORMAT_NCL_DATE)
        }</PackageEndDate>
                    </DateRange>
                    <Price>
                        {
          for ((guest, i) <- guests.zipWithIndex) yield <Guest>
                            <GuestRefNumber>{i + 1}</GuestRefNumber>
                            <PackagePrice Amount={
            CurrencyUtil.convertDollarsToCents(pkg.price).toString
          } CurrencyCode="USD"/>
                        </Guest>
        }
                    </Price>
                </Package>
      }
    }
            </Packages>
        </NCL_CruiseGetAvailOnBoardPackagesRS>
  }

  def generateDiningPackageXML(diningResult: NodeSeq,
                               reservation: FidelioReservation,
                               transactionId: String,
                               forDate: Date,
                               tableSize: String): NodeSeq = {
    val guests = reservation.clients
    val diningList = (diningResult \ "SWAvailResult" \ "Availability" \ "Availability" \ "Availability")
    val forDateStr =
      DateUtil.formatDate(forDate, DateUtil.FORMAT_SILVERWEB_DATE)
    val cal = Calendar.getInstance()

    for (pkg <- diningList) yield {
      val restaurant = (pkg \ "Restaurant").text.trim
      val mealPeriod = (pkg \ "MealPeriod").text.trim
      val availTime = (pkg \ "AvailableTime").text.trim
      /*
            Silver Where does not have a notion of a unique package ID.
            Embed various identifying information in package ID. This makes
            it unique. We can also deconstruct the fields from the ID before booking.
       */
      val packageId = "0"
      val packageCode = "%s%03d".format(restaurant, tableSize.toInt)

      //Split the hour and time of available dining
      val h = availTime.substring(0, 2)
      val m = availTime.substring(2, 4)

      cal.setTime(forDate)
      cal.set(Calendar.HOUR_OF_DAY, h.toInt)
      cal.set(Calendar.MINUTE, m.toInt)
      cal.set(Calendar.SECOND, 0)

      val nclDate = DateUtil.formatDate(cal.getTime, DateUtil.FORMAT_NCL_DATE)

      <Package ID={packageId}>
                <Type>DINING</Type>
                <Name>{(pkg \ "Restaurant").text.trim}</Name>
                <Code>{packageCode}</Code>
                <DateRange>
                    <PackageStartDate>{nclDate}</PackageStartDate>
                    <PackageEndDate>{nclDate}</PackageEndDate>
                </DateRange>
                <TableSize>{tableSize}</TableSize>
                <Price>
                    {for (i <- 0 until guests.size) yield <Guest>
                        <GuestRefNumber>{i + 1}</GuestRefNumber>
                        <PackagePrice Amount="0" CurrencyCode="USD"/>
                    </Guest>}
                </Price>
            </Package>
    }
  }

  def createEmptyNclResponse(transactionId: String,
                             packageType: String): NodeSeq = {
    val target = "Production"

    <NCL_CruiseGetAvailOnBoardPackagesRS AltLangID="en-us" EchoToken="String" PrimaryLangID="en-us" SequenceNmbr="1" Target={
      Settings.environment
    } TransactionIdentifier={transactionId} Version="3.1" xmlns="http://nclapi/schemas">
            <Success/>
            <Warnings/>
            <Packages>
            </Packages>
        </NCL_CruiseGetAvailOnBoardPackagesRS>
  }
}
