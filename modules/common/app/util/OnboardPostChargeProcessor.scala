package util

import modules.common.core.api.communication.HTTPDriver
import modules.fidelio_io.model.FidelioMessage
import modules.fidelio_io.model.FidelioMessageUnit._

import scala.util.{Failure, Success}
import scala.xml.NodeSeq

/**
  * Created by matt on 5/22/17.
  */
class OnboardPostChargeProcessor {}

object OnboardPostChargeProcessor {
//@Inject()/*(val fidelioDriver: FidelioDriver)*/(val cache: AsyncCacheApi,/* val WS: WSClient,*/ val syncCache: SyncCacheApi)(implicit val executionContext: ExecutionContext) extends MessageUtil {
//    MessageUtil =>

  val WS = HTTPDriver.ws

  final val POSTING_ID_ALREADY_EXISTS = 105.toString
  final val PAYMENT_ALREADY_EXISTS = 109.toString

  /*    def process(nclRequest: NodeSeq)(implicit ec: ExecutionContext): Future[Try[NodeSeq]] = {
        // Block all postings on disembark day.
        for {
            cruiseResult <- MessageUtil.retrieveCruise
            postingResult <- {
                val endDate = cruiseResult.get.endDate.toInstant
                  .atZone(ZoneOffset.UTC)
                  .toLocalDate

                val today = LocalDate.now()
                if (endDate.isEqual(today)) {
                    Future(Success(XmlUtil.createPostingsDisabledResponse(nclRequest)))
                } else {
                    postChargeUnchecked(nclRequest)
                }
            }
        } yield postingResult
    }*/

  /*    private def postChargeUnchecked(nclRequest: NodeSeq)(implicit ec: ExecutionContext) = {
        val clientId = (nclRequest \ "LoyaltyMembershipID").text.trim
        val outletId = (nclRequest \ "OutletID").text.trim

        val strTotalAmount = (nclRequest \ "TotalAmount").text.trim
        val totalAmount = if (strTotalAmount.isEmpty) 0.0 else strTotalAmount.toDouble

        val strTip = (nclRequest \ "Tip").text.trim
        val tip = if (strTip.isEmpty) 0.0 else strTip.toDouble

        val strTax = (nclRequest \ "Tax").text.trim
        val tax = if (strTax.isEmpty) 0.0 else strTax.toDouble

        val deviceUnitName = (nclRequest \ "DeviceUnitName").text.trim
        val notes = (nclRequest \ "Notes").text.trim

        val image = (nclRequest \ "Image").text.trim

        val uniquePostingId = (nclRequest \ "UniquePostingID").text.trim

        val allowPosting = Try((nclRequest \ "AllowPosting").text.trim.toBoolean).getOrElse(false)

        val postingDetails = FidelioPostingDetails.createPostingDetails(totalAmount, tip, tax, outletId, deviceUnitName, notes, uniquePostingId, allowPosting, image)

        MessageUtil.makeGenericPosting(clientId, postingDetails) flatMap {
            case Success(posting) =>
                val postingMessage = posting.as[FidelioMessage]
                Future.successful(buildResponse(clientId, strTotalAmount, postingMessage))

            case req@Failure(e) =>
                e match {
                    case x@FidelioFailureException(msg, fidMessage, fid) if fid.getOrElse("-1").equals(POSTING_ID_ALREADY_EXISTS) || fid.getOrElse("-1").equals(PAYMENT_ALREADY_EXISTS) => //could not find inherited objects or case classes
                        Logger.error(s"Fidelio returned failure (${fid}) response for: ${/*req.map(_.msgType).getOrElse("")*/}. $fidMessage")
                        Future.successful(Failure(x))
                    case _ =>

                        MessageUtil.makeGenericCrewPosting(clientId, postingDetails) map {
                            case Success(posting) =>
                                buildResponse(clientId, strTotalAmount, posting)
                            case Failure(ex) =>
                                Failure(ex)
                        }

                }
        }
    }*/

  private def buildResponse(clientId: String,
                            strTotalAmount: String,
                            posting: FidelioMessage) = {
    posting.getAll(Seq(PTI)).headOption match {
      case Some(postingHead) => {
        val postingTransactionId = postingHead.get(PTI).get

        Success(createNclResponse(postingTransactionId))
      }
      case None => {
        Failure(
          new Exception(
            s"Error occurred posting charge for client ID ${clientId} for amount ${strTotalAmount}"
          )
        )
      }
    }
  }

  def createNclResponse(postingTransactionId: String): NodeSeq = {
    <Onboard_PostChargeRS>
            <Success PostingTransactionID={postingTransactionId}/>
        </Onboard_PostChargeRS>
  }
}
