package modules.fidelio_io.util

import java.net.URLEncoder

import modules.common.Settings
import modules.common.core.api.communication.HTTPDriver
import modules.fidelio_io.service.FidelioLoginService
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.Logger
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class FidelioFailureException(msg: String, val failureMsg: JsValue)
    extends IllegalArgumentException(msg)

class FidelioServiceHelper {}

object FidelioServiceHelper {

  private val logger = Logger(classOf[FidelioServiceHelper])

  def validate(response: JsValue): Option[Exception] = {
    logger.debug(
      s"FidelioServiceHelper.validate got json ${Json.prettyPrint(response)}"
    )

    val result = (response \ "bSuccess").asOpt[Boolean]

    result match {
      case Some(successful) => {
        if (successful) {
          logger.debug("Fidelio response is valid.")

          None
        } else {
          logger.debug("Fidelio response validation found error: " + response)
          val errorMessage = (response \ "sErrMsg").as[String]
          Some(new FidelioFailureException(errorMessage, response))
        }
      }
      case None => {
        val errorMsg = (response \ "Message").asOpt[String]

        logger.debug(
          "Fidelio response validation found error: " + errorMsg
            .getOrElse("Error processing the request")
        )

        Some(
          new FidelioFailureException(
            errorMsg.getOrElse("Error processing the request"),
            response
          )
        )
      }
    }
  }

  def getJSON(psFunction: String, psParam: Seq[Any])(
      implicit ec: ExecutionContext
  ): Future[Try[JsValue]] = {

    FidelioLoginService.getSessionIdCached.flatMap { psSessionId =>
      psSessionId match {
        case Success(psSessionId) => {
          val baseUrl: String = Settings.fidelioURL + "/FidelioSPMSWSJsonGet"
          val url = buildUrl(baseUrl, psFunction, psParam, psSessionId)
          logger.debug("Fidelio service URL: " + url)

          HTTPDriver.getJSON(url).map {
            _ match {
              case Failure(ex) => {
                logger
                  .error(s"Fidelio service call failed ${ex.getStackTrace}", ex)
                Failure(ex)
              }
              case Success(js) => {
                logger.debug("Fidelio service call completed. Will validate.")
                logger.debug("Fidelio service request body: " + js.toString)

                FidelioServiceHelper.validate(js) match {
                  case None => {
                    Success(js)
                  }
                  case Some(ex) => {
                    Failure(ex)
                  }
                }
              }
            }
          }
        }
        case Failure(ex) => Future(Failure(ex))
      }
    }
  }

  def postJSON(psFunction: String, psParam: Seq[Any])(
      implicit ec: ExecutionContext
  ): Future[Try[JsValue]] = {

    FidelioLoginService.getSessionIdCached.flatMap { psSessionId =>
      psSessionId match {
        case Success(psSessionId) => {
          val url: String = Settings.fidelioURL + "/FidelioSPMSWSJsonPost"
          val body = constructJsonBody(psFunction, psParam, psSessionId)

          logger.debug("Fidelio service URL: " + url)
          logger.debug("Fidelio service request body: " + body)

          HTTPDriver.postJSON(url, body).map {
            _ match {
              case Failure(ex) => {
                logger
                  .error(s"Fidelio service call failed ${ex.getStackTrace}", ex)
                Failure(ex)
              }
              case Success(js) => {
                logger.debug("Fidelio service call completed. Will validate.")

                FidelioServiceHelper.validate(js) match {
                  case None => {
                    Success(js)
                  }
                  case Some(ex) => {
                    Failure(ex)
                  }
                }
              }
            }
          }
        }
        case Failure(ex) => Future(Failure(ex))
      }
    }
  }

  def buildUrl(baseUrl: String,
               psFunction: String,
               psParam: Seq[Any],
               psSessionId: String): String = {
    val url = baseUrl +
      "?psFunction=" +
      encode(surroundWithDoubleQuotes(psFunction)) +
      "&psParam=" +
      encode(createParamString(psParam)) +
      "&psSessionId=" +
      encode(surroundWithDoubleQuotes(psSessionId))

    url
  }

  def constructJsonBody(psFunction: String,
                        psParam: Seq[Any],
                        psSessionId: String): JsObject = {
    val msg = Seq(
      ("psFunction" -> JsString(psFunction)),
      ("psSessionID" -> JsString(psSessionId)),
      ("psParam" -> createParamJson(psParam))
    )

    return JsObject(msg)
  }

  private def encode(value: String): String = {
    URLEncoder.encode(value, "UTF-8");
  }

  private def surroundWithDoubleQuotes(value: String): String = {
    "\"" + value + "\""
  }

  private def convertToJsonDate(value: DateTime): String = {
    val formatter = DateTimeFormat.forPattern("yyyyMMddhhmmss")

    formatter.print(value)
  }

  private def createParamJson(psParam: Seq[Any]): JsArray = {
    val jsList = psParam.map(param => {
      param match {
        case d: DateTime     => new JsString(convertToJsonDate(d))
        case s: String       => new JsString(s)
        case aList: Seq[Any] => createParamJson(aList)
        case intVal: Int     => new JsNumber(intVal)
        case dVal: Double    => new JsNumber(dVal)
        case bVal: Boolean   => Json.toJson(bVal) // new JsBoolean(bVal)
        case anyVal          => new JsString(anyVal.toString)
      }
    })

    return JsArray(jsList)
  }

  private def createParamString(psParam: Seq[Any]): String = {
    val jsArray = createParamJson(psParam)
    val jsString = Json.stringify(jsArray)

    logger.debug("Fidelio psParam = " + jsString)

    jsString
  }
}
