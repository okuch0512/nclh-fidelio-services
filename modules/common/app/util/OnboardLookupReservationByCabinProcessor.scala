package modules.data_mapper.service

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.model.FidelioReservation
import modules.fidelio_io.util.DateUtil

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.xml.NodeSeq

object OnboardLookupReservationByCabinProcessor {

  def process(
      nclRequest: NodeSeq
  )(implicit ec: ExecutionContext): Future[Try[NodeSeq]] = {
    val cabinNumber = (nclRequest \ "CabinNumber").text.trim

    MessageUtil.retrieveReservationByCabin(cabinNumber) map {
      reservationResult =>
        reservationResult match {
          case Success(res) => Success(createResponse(res, cabinNumber))
          case Failure(ex)  => Failure(ex)
        }
    }
  }

  def createResponse(reservation: FidelioReservation,
                     cabinNumber: String): NodeSeq = {
    val guestList = reservation.clients

    <Onboard_LookupReservationByCabinRS>
            <ReservationID>{guestList(0).reservationId}</ReservationID>
            <CabinNumber>{guestList(0).cabinNumber}</CabinNumber>
            <SailStartDate>{
      DateUtil.formatDate(
        guestList(0).embarkDate,
        DateUtil.FORMAT_NCL_PACKAGE_DATE
      )
    }</SailStartDate>
            <SailEndDate>{
      DateUtil.formatDate(
        guestList(0).disembarkDate,
        DateUtil.FORMAT_NCL_PACKAGE_DATE
      )
    }</SailEndDate>
            <GuestDetails>
                {
      guestList.map { guest =>
        <GuestDetail>
                    <NamePrefix>{guest.name.NameTitle.getOrElse("")}</NamePrefix>
                    <GivenName>{guest.name.GivenName.getOrElse("")}</GivenName>
                    <Surname>{guest.name.Surname.getOrElse("")}</Surname>
                    <DateOfBirth>{
          if (guest.birthDate.isDefined)
            DateUtil.formatDate(
              guest.birthDate.get,
              DateUtil.FORMAT_NCL_PACKAGE_DATE
            )
          else ""
        }</DateOfBirth>
                    <LoyaltyMembershipID>{guest.nclClientId}</LoyaltyMembershipID>
                    <ReservationID>{guest.reservationId}</ReservationID>
                </GuestDetail>
      }
    }
            </GuestDetails>
        </Onboard_LookupReservationByCabinRS>
  }
}
