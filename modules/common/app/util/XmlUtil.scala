package modules.fidelio_io.util

import scala.xml.NodeSeq

/**
  * Created by bibhas on 1/4/16.
  */
object XmlUtil {

  val defaultErrorCode = "0000"
  val postingsDisabledCode = "5000"
  val hardErrorType = "3"

  /**
    * Returns an XML document representing an error situation.
    *
    * @param rootElement The name of the root element. This is usually a response document root element.
    * @param error The error that took place. If it is a FidelioFailureException then the actual error message
    *              from Fidelio is extracted and set as the warning message text.
    * @return
    */
  def warning(rootElement: String, error: Throwable) = {
    val longMsg = error match {
      case e => e.getMessage
    }

    <rootElement>
            <Errors>
            <Error Code="0000" ShortText={error.getMessage} Type="3">{longMsg}</Error>
            </Errors>
        </rootElement>.copy(label = rootElement)
  }

  def createPostingsDisabledResponse(requestBody: NodeSeq): NodeSeq = {
    val rootRequestElement = requestBody.head.label
    val rootResponseElement = rootRequestElement.replaceAll("RQ$", "RS")
    val message =
      "Postings and charges are disabled until the new sailing starts."

    <rootResponseElement>
      <Errors>
        <Error Code={postingsDisabledCode} ShortText={message} Type={
      hardErrorType
    }>{message}</Error>
      </Errors>
    </rootResponseElement>.copy(label = rootResponseElement)
  }

}
