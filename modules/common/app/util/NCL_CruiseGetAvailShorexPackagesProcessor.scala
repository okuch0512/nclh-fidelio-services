package modules.data_mapper.service

import modules.common.Settings
import modules.data_mapper.util.{CurrencyUtil, MessageUtil}
import modules.fidelio_io.model.{FidelioExcursion, FidelioReservation}
import modules.fidelio_io.util.DateUtil

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.xml.NodeSeq

/**
  * Created by matt on 12/22/15.
  */
object NCL_CruiseGetAvailShorexPackagesProcessor {

  def process(
      nclRequest: NodeSeq
  )(implicit ec: ExecutionContext): Future[Try[NodeSeq]] = {
    val reservationId = (nclRequest \ "ReservationID" \ "@ID").text.trim
    val transactionId = (nclRequest \ "@TransactionIdentifier").text.trim

    for {
      reservationResult <- MessageUtil.retrieveReservation(reservationId)

      excursionResult <- {
        reservationResult match {
          case Success(reservation) => {
            val strStartDate =
              (nclRequest \ "PackageDateRange" \ "PackageStartDate").text.trim
            val strEndDate =
              (nclRequest \ "PackageDateRange" \ "PackageEndDate").text.trim

            val startDate =
              DateUtil.parseDate(strStartDate, DateUtil.FORMAT_NCL_PACKAGE_DATE)
            val endDate =
              DateUtil.parseDate(strEndDate, DateUtil.FORMAT_NCL_PACKAGE_DATE)

            MessageUtil.retrieveExcursions(startDate, endDate, reservation)
          }
          case Failure(ex) => Future(Failure(ex))
        }
      }
    } yield createNclResponse(excursionResult, reservationResult, transactionId)
  }

  def createNclResponse(excursions: Try[Seq[FidelioExcursion]],
                        reservation: Try[FidelioReservation],
                        transactionId: String): Try[NodeSeq] = {

    (excursions, reservation) match {
      case (Success(excursions), Success(reservation)) => {
        val guests = reservation.clients

        val numGuests = guests.size

        val packages: Seq[FidelioExcursion] = excursions
          .filter(_.showInITV == true)
          .filter(_.bookingStatus == MessageUtil.STATUS_AVAILABLE)
          .filter(MessageUtil.getNumAvailableSeats(_) >= numGuests)

        val xml =
          <NCL_CruiseGetAvailShorexPackagesRS AltLangID="en-us" EchoToken="String" PrimaryLangID="en-us" SequenceNmbr="1" Target={
            Settings.environment
          } TransactionIdentifier={transactionId} Version="3.1" xmlns="http://nclapi/schemas">
                    <Success/>
                    <Packages>
                        {
            for (pkg <- packages) yield <Package ID={pkg.id.toString}>
                            <Type>EXCURSION</Type>
                            <Name>{pkg.excursionName}</Name>
                            <Code>{
              MessageUtil.getExcursionCode(pkg.excursionNumber)
            }</Code>
                            <DateRange>
                                <PackageStartDate>{
              DateUtil.formatDate(pkg.startDate, DateUtil.FORMAT_NCL_DATE)
            }</PackageStartDate>
                                <PackageEndDate>{
              DateUtil.formatDate(pkg.endDate, DateUtil.FORMAT_NCL_DATE)
            }</PackageEndDate>
                            </DateRange>
                            <Price>
                                {
              for ((guest, i) <- guests.zipWithIndex) yield <Guest>
                                    <GuestRefNumber>{i + 1}</GuestRefNumber>
                                    {
                if (guest.minor == true)
                  <PackagePrice Amount={
                    CurrencyUtil.convertDollarsToCents(pkg.childPrice).toString
                  } CurrencyCode="USD"/>
                else
                  <PackagePrice Amount={
                    CurrencyUtil.convertDollarsToCents(pkg.adultPrice).toString
                  } CurrencyCode="USD"/>
              }
                                </Guest>
            }
                            </Price>
                            <Location>
                                <From>
                                    <Code>{
              MessageUtil.getPortCodeFromEventOrExcursionNumber(
                pkg.excursionNumber
              )
            }</Code>
                                    <Type>PORT</Type>
                                    <Name>{pkg.portName}</Name>
                                </From>
                                <To>
                                    <Code>{
              MessageUtil.getPortCodeFromEventOrExcursionNumber(
                pkg.excursionNumber
              )
            }</Code>
                                    <Type>PORT</Type>
                                    <Name>{pkg.portName}</Name>
                                </To>
                            </Location>
                        </Package>
          }
                    </Packages>
                </NCL_CruiseGetAvailShorexPackagesRS>

        Success(xml)
      }

      case _ =>
        Failure(
          new Exception(
            "An error occurred retrieving the available shorex packages"
          )
        )
    }
  }
}
