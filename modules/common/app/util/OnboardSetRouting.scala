package modules.data_mapper.service

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.model.{FidelioClientAccount, FidelioMessageType}
import modules.fidelio_io.util.FidelioServiceHelper

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import scala.xml.NodeSeq

object OnboardSetRouting {
  def process(nclRequest: NodeSeq): Future[Try[NodeSeq]] = {
    val reservationId = (nclRequest \ "ReservationId").text.trim
    val payerClientId = (nclRequest \ "PayerClientId").text.trim.toInt
    val buyerIDList = (nclRequest \ "Buyers" \ "BuyerClientId")

    MessageUtil.retrieveReservation(reservationId).flatMap { result =>
      result match {
        case Success(reservation) => {
          val payerFidelioID =
            nclClientIdToFidelioAccountID(payerClientId, reservation.clients)
          //Iterate through all buyers and setup routing
          val allFutures = buyerIDList.map { buyerIDNode =>
            val buyerID = buyerIDNode.text.trim.toInt
            val buyerFidelioID =
              nclClientIdToFidelioAccountID(buyerID, reservation.clients)

            setupRouting(payerFidelioID, buyerFidelioID, buyerID)
          }

          createResponse(allFutures)
        }
        case Failure(ex) => Future(Failure(ex))
      }
    }
  }

  def createResponse(
      allFutures: Seq[Future[Try[NodeSeq]]]
  ): Future[Try[NodeSeq]] = {
    Future.sequence(allFutures) map { allResults =>
      val xml =
        <Onboard_SetRoutingRS>
          {
          allResults map { result =>
            result match {
              case Success(nodes) => nodes
              case Failure(ex)    => NodeSeq.Empty
            }
          }
        }
        </Onboard_SetRoutingRS>

      Success(xml)
    }
  }

  def setupRouting(payerFidelioID: Int,
                   buyerFidelioID: Int,
                   buyerID: Int): Future[Try[NodeSeq]] = {
    val psFunction = FidelioMessageType.ADD_ROUTING
    val pnWindow = if (payerFidelioID == buyerFidelioID) 1 else 0
    val psParam = Seq(buyerFidelioID, payerFidelioID, pnWindow, "", "[]")

    FidelioServiceHelper.getJSON(psFunction, psParam).map { result =>
      result match {
        case Success(_) => Success(<Success BuyerID={buyerID.toString}/>)
        case Failure(ex) =>
          Success(<Error BuyerID={buyerID.toString} Message={ex.getMessage}/>)
      }
    }
  }

  private def nclClientIdToFidelioAccountID(
      nclId: Int,
      guests: Seq[FidelioClientAccount]
  ): Int = {
    guests.find({ guest =>
      guest.nclClientId == nclId
    }) match {
      case Some(guest) => guest.fidelioAcctId
      case _ =>
        throw new IllegalArgumentException(s"Invalid client ID: ${nclId}")
    }
  }
}
