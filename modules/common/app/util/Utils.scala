package modules.data_mapper.util

import play.api.libs.json.Json

import scala.io.Source.fromFile

object Utils {

  def getLocalFile(
      path: String
  )(implicit environment: play.api.Environment): String = {
    val file = environment.getFile(path)
    if (file.exists)
      fromFile(file).getLines.mkString
    else
      "N/A"
  }

  def getJsonFile(path: String)(implicit environment: play.api.Environment) =
    Json.parse(getLocalFile(path))
}
