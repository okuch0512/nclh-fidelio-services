package modules.data_mapper.service

import java.util.UUID

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.model._
import modules.fidelio_io.util.{DateUtil, FidelioServiceHelper}
import play.api.Logger
import play.api.libs.json.{Json, Reads}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import scala.xml.NodeSeq

class OnboardBookPackageProcessor {}

/**
  * Created by matt on 2/2/16.
  */
object OnboardBookPackageProcessor {

  private val logger = Logger(classOf[OnboardBookPackageProcessor])

  def process(nclRequest: NodeSeq): Future[Try[NodeSeq]] = {

    val reservationId = (nclRequest \ "ReservationID").text.trim

    val strPackageId = (nclRequest \ "PackageID").text.trim

    val packageId = if (strPackageId.isEmpty) 0 else strPackageId.toInt

    val packageType = (nclRequest \ "PackageType").text.trim
    val strBuyerClientId = (nclRequest \ "BuyerClientID").text.trim

    if (strBuyerClientId == null || strBuyerClientId.trim.length == 0) {
      Future(Failure(new IllegalArgumentException("BuyerClientID is missing.")))
    } else {
      val buyerClientId = strBuyerClientId.toInt

      MessageUtil.retrieveReservation(reservationId) flatMap {
        reservationResult =>
          val reservation = reservationResult.get

          // retrieve all the Fidelio accounts for the reservation
          val guests = reservation.clients

          val allBookingFutures = ArrayBuffer[Future[Try[NodeSeq]]]()

          val participants = buildParticipants(nclRequest, guests)

          // To emulate the all or nothing behavior that Seaware has in Fidelio, we verify there are enough seats (spots)
          // available before attempting to book a shorex or entertainment package.
          // (e.g., try to book a shorex or entertainment package for 3 guests and there are only 2 spots available.
          // In this case, none of the guests get booked.)
          if (packageType == MessageUtil.PACKAGE_EXCURSION || packageType == MessageUtil.PACKAGE_ENTERTAINMENT) {
            verifyEnoughSeatsAvailable(packageId, packageType, guests) flatMap {
              result =>
                result match {
                  case Success((enoughSeatsAvailable, numAvailableSeats)) => {
                    if (enoughSeatsAvailable) {
                      participants foreach { participant =>
                        val future = doBooking(nclRequest, guests, participant)
                        allBookingFutures += future
                      }

                      createBookingResponse(allBookingFutures)
                    } else {
                      participants foreach { participant =>
                        val future = Future(
                          Success(
                            createNclFailureElement(
                              participant,
                              s"Booking failed. Only ${numAvailableSeats} booking(s) available."
                            )
                          )
                        )
                        allBookingFutures += future
                      }

                      createBookingResponse(allBookingFutures)
                    }
                  }
                  case Failure(ex) => {
                    participants foreach { participant =>
                      val future = Future(
                        Success(
                          createNclFailureElement(participant, ex.getMessage)
                        )
                      )
                      allBookingFutures += future
                    }

                    createBookingResponse(allBookingFutures)
                  }
                }
            }
          } else if (packageType == MessageUtil.PACKAGE_TENDER_TICKET) {
            processTenderTicketRequest(
              nclRequest,
              guests,
              reservation,
              participants,
              buyerClientId
            )
          } else {
            participants foreach { participant =>
              val future = doBooking(nclRequest, guests, participant)
              allBookingFutures += future
            }

            createBookingResponse(allBookingFutures)
          }
      }
    }
  }

  def processTenderTicketRequest(nclRequest: NodeSeq,
                                 guests: Seq[FidelioClientAccount],
                                 reservation: FidelioReservation,
                                 participants: Seq[Participant],
                                 buyerClientId: Int): Future[Try[NodeSeq]] = {
    val packageStartDate = (nclRequest \ "PackageStartDate").text.trim

    if (packageStartDate.isEmpty) {
      Future(
        Failure(
          new IllegalArgumentException("PackageStartDate can't be empty.")
        )
      )
    } else {
      val startDate = DateUtil.parseDate(
        packageStartDate,
        DateUtil.FORMAT_NCL_DATE_WITH_SLASHES
      )

      for {
        eventsResult <- {
          // Unlike in FCUI, the web service requires 1 extra day to return the events.
          // Otherwise, you'll get back no events.
          val endDate = DateUtil.addDays(startDate, 1)

          MessageUtil.retrieveEvents(startDate, endDate, reservation)
        }

        bookingsResult <- {

          val numParticipants = participants.size

          // Create a participant for the buyer, since that's what the bookEvent method is expecting.
          // Book a quantity corresponding with the number of participants.
          val buyer = buildParticipant(buyerClientId, numParticipants, guests)

          // only obtain events which are tender tickets, are supposed to be displayed in an external
          // booking system, have an Available status, and have enough seats available for all the
          // guests in the reservation
          val tenderTicketPackages =
            eventsResult.get.filter(
              pkg =>
                MessageUtil.isTenderTicket(pkg) &&
                  pkg.showInITV &&
                  pkg.bookingStatus == MessageUtil.STATUS_AVAILABLE &&
                  MessageUtil.getNumAvailableSeats(pkg) >= numParticipants
            )

          // Sort tender groups by name
          val sortedPackages = tenderTicketPackages.sortBy(_.eventName)

          val elements = new ArrayBuffer[NodeSeq]()

          val startingIndex = 0

          // Book tender tickets for the participants in an all or nothing matter.
          // Try to book the first tender group.  If it fails, then try to book
          // the next tender group, and so on and so forth.  doTenderTicketBooking
          // is a recursive method.
          doTenderTicketBooking(buyer, sortedPackages, startingIndex).map {
            result =>
              result match {
                case Success(bookingResult) => {
                  participants.foreach { participant =>
                    // Create a success element for each participant containing their
                    // participant id and the booked package id.
                    elements += createNclSuccessElement(
                      participant,
                      bookingResult._1
                    )
                  }

                  elements
                }
                case Failure(ex) => {
                  participants.foreach { participant =>
                    elements += createNclFailureElement(
                      participant,
                      ex.getMessage
                    )
                  }

                  elements
                }
              }
          }
        }
      } yield {
        Success(createBookingResponseNoFutures(bookingsResult))
      }
    }
  }

  /**
    * This is a recursive method which attempts to book the current tender group package,
    * corresponding with the index passed in.  If successful, it returns a tuple containing
    * the package id and package name.  If it fails, it calls itself again. Recursion
    * ends when either the booking succeeds or there are no more packages to try.
    *
    * @param buyer
    * @param packages
    * @param index
    * @return
    */
  def doTenderTicketBooking(buyer: Participant,
                            packages: Seq[FidelioEvent],
                            index: Int): Future[Try[(Int, String)]] = {

    if (packages.isEmpty) {
      Future(
        Failure(
          new IllegalStateException(
            "We are sorry. We can not find any tender group with enough available seats. Try booking with fewer guests."
          )
        )
      )
    } else {
      val pkg = packages(index)
      val pkgId = pkg.id

      bookEvent(pkgId, buyer) map {
        case Success(bookingResult) => {
          Success((pkgId, pkg.eventName))
        }
        case Failure(ex) => {
          val msg =
            s"Failed to book ${pkg.eventName} for client ID: ${buyer.clientId}"
          logger.error(msg, ex)

          // try to book the next package
          return doTenderTicketBooking(buyer, packages, index + 1)
        }
      }
    }
  }

  def createBookingResponse(
      allBookingFutures: Seq[Future[Try[NodeSeq]]]
  ): Future[Try[NodeSeq]] = {
    //Wait for all futures to complete
    Future.sequence(allBookingFutures) map { allBookingResults =>
      //Look for any failures. Note: Any booking failure indicated by Fidelio
      //would have been converted to a Success(NodeSeq) by now containing custom error element.
      //So a true Failure(ex) will indicate an unexpected error such as invalid product type.
      allBookingResults.find(_.isFailure) match {
        case Some(Failure(ex)) => Failure(ex)
        case _ => {
          val xml =
            <Onboard_BookPackageRS>{
              allBookingResults map { result =>
                result match {
                  case Success(nodes) => nodes
                  case Failure(ex)    => NodeSeq.Empty
                }
              }
            }</Onboard_BookPackageRS>

          Success(xml)
        }
      }
    }
  }

  def createBookingResponseNoFutures(bookingElements: Seq[NodeSeq]): NodeSeq = {
    val xml =
      <Onboard_BookPackageRS>{
        bookingElements map { element =>
          element
        }
      }</Onboard_BookPackageRS>

    xml
  }

  def verifyEnoughSeatsAvailable(
      packageId: Int,
      packageType: String,
      guests: Seq[FidelioClientAccount]
  ): Future[Try[(Boolean, Int)]] = {
    for {
      pkgResult <- if (packageType == MessageUtil.PACKAGE_EXCURSION)
        MessageUtil.retrieveExcursion(guests.head, packageId)
      else MessageUtil.retrieveEvent(guests.head, packageId)
    } yield {
      pkgResult match {
        case Success(pkg: FidelioExcursion) => {
          val numAvailableSeats = MessageUtil.getNumAvailableSeats(pkg)
          val numGuests = guests.length

          if (numAvailableSeats >= numGuests) {
            Success((true, numAvailableSeats))
          } else {
            Success((false, numAvailableSeats))
          }
        }
        case Success(pkg: FidelioEvent) => {
          val numAvailableSeats = MessageUtil.getNumAvailableSeats(pkg)
          val numGuests = guests.length

          if (numAvailableSeats >= numGuests) {
            Success((true, numAvailableSeats))
          } else {
            Success((false, numAvailableSeats))
          }
        }
        case Success(v) =>
          Failure(new Exception(s"not implemented for $v"))
        case Failure(ex) => {
          Failure(ex)
        }
      }

    }
  }

  def doBooking(nclRequest: NodeSeq,
                guests: Seq[FidelioClientAccount],
                participant: Participant,
                pkgId: Option[Int] = None): Future[Try[NodeSeq]] = {
    // either the excursion id, event id, or dining package id (always 0 for dining)
    var packageId: Int = 0

    if (pkgId.isEmpty) {
      packageId = (nclRequest \ "PackageID").text.trim.toInt
    } else {
      packageId = pkgId.get
    }

    val packageType = (nclRequest \ "PackageType").text.trim
    val packageCode = (nclRequest \ "PackageCode").text.trim
    val packageStartDate = (nclRequest \ "PackageStartDate").text.trim
    val packageStartTime = (nclRequest \ "PackageStartTime").text.trim
    val reservationId = (nclRequest \ "ReservationID").text.trim
    val restaurantNameCode = (nclRequest \ "RestaurantCode").text.trim

    // Seaware client id of the person booking the package (i.e., buyer)
    val buyerClientId = (nclRequest \ "BuyerClientID").text.trim

    val buyerFidelioAcctId = getFidelioBuyerAccountId(nclRequest, guests)

    logger.debug(
      s"Buyer client ID: ${buyerClientId} participant ID: ${participant.clientId}"
    )
    logger.debug(
      s"Buyer Fidelio account ID: ${buyerFidelioAcctId.getOrElse("")} participant Fidelio ID: ${participant.fidelioAccountId
        .getOrElse("")}"
    )

    if (buyerFidelioAcctId.isEmpty || participant.fidelioAccountId.isEmpty) {
      Future(
        Success(
          createNclFailureElement(
            participant,
            "Invalid client id and/or participant client id"
          )
        )
      )
    } else {
      packageType match {
        case MessageUtil.PACKAGE_EXCURSION =>
          bookExcursion(packageId, buyerFidelioAcctId.get, participant) map {
            result =>
              result match {
                case Success(bookingResult) =>
                  Success(createNclSuccessElement(participant, packageId))

                case Failure(ex) =>
                  logger.error(
                    s"Failed to book excursion for client ID: ${participant.clientId}",
                    ex
                  )
                  Success(createNclFailureElement(participant, ex.getMessage))
              }
          }

        case MessageUtil.PACKAGE_ENTERTAINMENT =>
          /*
                For events we use the participant as the buyer. Because fidelio only takes
                a single client ID that is doing the booking. By using participant ID
                as the buyer we can later identify who the booking was for.
           */
          bookEvent(packageId, participant) map { result =>
            result match {
              case Success(bookingResult) =>
                Success(createNclSuccessElement(participant, packageId))

              case Failure(ex) => {
                logger.error(
                  s"Failed to book ${packageType} for client ID: ${participant.clientId}",
                  ex
                )
                Success(createNclFailureElement(participant, ex.getMessage))
              }
            }
          }

        case MessageUtil.PACKAGE_DINING =>
          bookDining(
            packageCode,
            restaurantNameCode,
            packageStartDate,
            packageStartTime,
            buyerFidelioAcctId.get,
            participant
          ) map { result =>
            result match {
              case Success(bookingResult) =>
                Success(createNclSuccessElement(participant, packageId))

              case Failure(ex) =>
                logger.error(
                  s"Failed to book excursion for client ID: ${participant.clientId}",
                  ex
                )
                Success(createNclFailureElement(participant, ex.getMessage))
            }
          }

        case badType =>
          Future(
            Failure(
              new IllegalArgumentException(s"Invalid package type: ${badType}")
            )
          )
      }
    }
  }

  def bookExcursion(
      excursionId: Int,
      buyerFidelioAcctId: Int,
      participant: Participant
  ): Future[Try[FidelioBookingResponse]] = {

    val psFunction = FidelioMessageType.BOOKING

    val numAdultTickets = if (participant.isAdult) participant.quantity else 0
    val numChildTickets = if (!participant.isAdult) participant.quantity else 0

    /**
      *  Fidelio Parameters:
      *  1. pnAccID (Unique Guest ID) = Buyer's Fidelio Account ID (value passed in)
      *  2. psUniqID (Unique Excursion Booking ID) = generated value (up to 30 characters in length)
      *  3. pnExcID (Excursion ID) = value passed in
      *  4. pnAdultTicket (Number of Adult Tickets) = value passed in
      *  5. pnChildTicket (Number of Child Tickets) = value passed in
      *  6. pnAdultPromoID (Adult Promotion ID) = 0 (default)
      *  7. pnChildPromotID (Child Promotion ID) = 0 (default)
      *  8. psLanguage (ISO Language Code) = empty (default)
      *  9. psNotes (Notes) = empty (default)
      *  10. psPin (Pin Code) = empty (default)
      *  11. psSource (Booking Source) = empty (default)
      *  12. pbForceBooking (Whether or not to allow normal posting) = false (normal posting)
      * 13. pbKiosk (Kiosk Flag) = false
      * 14. pbySigImage (Signature Byte Array) = 0
      * 15. pbPercentageDiscount (true = Percentage Discount, false = Value Discount) = false (Value Discount)
      * 16. pnDiscount (Discount Percentage Rate or Discount Value depending on pbPercentageDiscount) = 0
      * 17. pnOvewriteSalesPrice (User defined price) = 0
      * 18. pnWeight (Weight, 0 if overweight is not used, > 0 it will charge overweight depend on the setup) = 0
      * 19. pnHeight (Height, for info only) = 0
      * 20. pbAllowWaitList (true = Allow Wait List when shorex is fully booked, false = Return error when shorex is fully booked) = false
      * 21. pnParticipant (Unique Guest ID for the person who will take part in the excursion) = Participant's Fidelio Account ID (value passed in)
      */
//        val psParam = Seq(buyerFidelioAcctId, generateBookingId, excursionId, numAdultTickets, numChildTickets, 0, 0, "", "", "", "", false, false, 0, false, 0, 0, 0, 0, false, participant.fidelioAccountId.get)

    /**
      * For now, we are supplying the Fidelio Account ID of the participant as both the pnAccID and pnParticipant,
      * so that the excursion shows up on the participant's excursion folio and not the buyer's account folio.
      *
      * This change was instituted, since there appears to be a bug in the GetExcursion function,
      * where the pnParticipant parameter is ignored. Instead the pnAccID is used when doing the booking,
      * such that the booking shows up on the pnAccID's excursion folio, rather than the pnParticipant's
      * excursion folio.
      */
    val psParam = Seq(
      participant.fidelioAccountId.get,
      generateBookingId,
      excursionId,
      numAdultTickets,
      numChildTickets,
      0,
      0,
      "",
      "",
      "",
      "",
      false,
      false,
      0,
      false,
      0,
      0,
      0,
      0,
      false,
      participant.fidelioAccountId.get
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => FidelioBookingResponse.fromJson(js)
      case Failure(ex) =>
        Failure(ex)
    }
  }

  def bookEvent(
      eventId: Int,
      participant: Participant
  ): Future[Try[FidelioBookingResponse]] = {

    val psFunction = FidelioMessageType.EVENTBOOKING

    for {
      // need to retrieve event in order to obtain PCT (Price Category Code)
      // and PGT (Person Group Code) which are required when booking an event
      eventResult <- MessageUtil.retrieveEvent(
        participant.embarkDate.get,
        participant.disembarkDate.get,
        participant.fidelioAccountId.get,
        eventId
      )

      bookingResult <- {
        // for an event, there is no different between the buyer and participant -
        // they are one and the same, so the participant id is used
        /**
          * Fidelio Parameters:
          *  1. pnAccID (Unique Guest ID) = Fidelio Internal Account Number
          *  2. psUniqID (Unique Event Booking ID) = generated value (up to 30 characters in length)
          *  3. pnEvtID (Event ID) = value passed in
          *  4. psPriceCategory (Price Category Code) = value passed in
          *  5. psPersonGroup (Person Group Code) = value passed in
          *  6. pnTicket (Number of Tickets) = value passed in
          *  7. psNotes (Notes) = empty (default)
          *  8. psPin (Pin Code) = empty (default)
          *  9. psSource (Booking Source) = empty (default)
          *  10. pbForceBooking (Whether or not to allow normal posting) = false (normal posting)
          */
        val psParam = Seq(
          participant.fidelioAccountId.get,
          generateBookingId,
          eventId,
          eventResult.get.priceCategoryCode,
          eventResult.get.personGroupCode,
          participant.quantity,
          "",
          "",
          "",
          false
        )

        FidelioServiceHelper.getJSON(psFunction, psParam).map {
          case Success(js) => FidelioBookingResponse.fromJson(js)
          case Failure(ex) => Failure(ex)
        }
      }
    } yield bookingResult
  }

  def bookDining(packageCode: String,
                 restaurantNameCode: String,
                 packageStartDate: String,
                 packageStartTime: String,
                 buyerFidelioAcctId: Int,
                 participant: Participant): Future[Try[NodeSeq]] = {

    Future(
      Try(
        <SWCalendar xmlns="http://tempuri.org/SilverWeb/SilverWeb"></SWCalendar>
      )
    ) // TODO:
  }

  private def generateBookingId: String = {
    UUID.randomUUID().toString
  }

  def createNclSuccessElement(participant: Participant,
                              packageId: Int): NodeSeq = {
    <Success ParticipantID={participant.clientId.toString} PackageID={
      packageId.toString
    }/>
  }

  def createNclFailureElement(participant: Participant,
                              errorMsg: String): NodeSeq = {
    var message = errorMsg

    if (participant.name.isDefined) {
      message = s"${participant.name.get}: ${errorMsg}"
    }

    <Error ParticipantID={participant.clientId.toString} Message={message}/>
  }

  private def getFidelioBuyerAccountId(
      nclRequest: NodeSeq,
      guests: Seq[FidelioClientAccount]
  ): Option[Int] = {
    var buyerFidelioAcctId: Option[Int] = None

    // Seaware client id of the person booking the package (i.e., buyer)
    val buyerClientId = (nclRequest \ "BuyerClientID").text.trim.toInt

    for (guest <- guests) {
      if (buyerClientId == guest.nclClientId) {
        buyerFidelioAcctId = Some(guest.fidelioAcctId)
      }
    }

    buyerFidelioAcctId
  }

  private def buildParticipants(
      nclRequest: NodeSeq,
      guests: Seq[FidelioClientAccount]
  ): Seq[Participant] = {
    val participantNodes = (nclRequest \ "Participants" \ "Participant")

    val participants = ArrayBuffer[Participant]()

    participantNodes foreach { node =>
      // Seaware client id of the person for whom the package is being booked (i.e., participant)
      val clientId = (node \ "ParticipantClientID").text.trim.toInt
      val quantity = (node \ "Quantity").text.trim.toInt

      participants += buildParticipant(clientId, quantity, guests)
    }

    participants.toSeq
  }

  private def buildParticipant(
      clientId: Int,
      quantity: Int,
      guests: Seq[FidelioClientAccount]
  ): Participant = {
    var fidelioAccountId: Option[Int] = None
    var name: Option[String] = None
    var isAdult: Boolean = false
    var embarkDate: Option[String] = None
    var disembarkDate: Option[String] = None

    // loop through the guests for the reservation and
    // determine which one is the participant.  Then obtain the
    // participant's information.
    for (guest <- guests) {
      if (clientId == guest.nclClientId) {

        fidelioAccountId = Some(guest.fidelioAcctId)
        name = Some(s"${guest.name.GivenName.get} ${guest.name.Surname.get}")
        embarkDate = Some(
          DateUtil.formatDate(guest.embarkDate, DateUtil.FORMAT_FIDELIO_DATE)
        )
        disembarkDate = Some(
          DateUtil.formatDate(guest.disembarkDate, DateUtil.FORMAT_FIDELIO_DATE)
        )

        if (guest.minor) {
          isAdult = false
        } else {
          isAdult = true
        }
      }
    }

    Participant(
      clientId,
      fidelioAccountId,
      name,
      isAdult,
      quantity,
      embarkDate,
      disembarkDate
    )
  }
}

case class Participant(clientId: Int,
                       fidelioAccountId: Option[Int],
                       name: Option[String],
                       isAdult: Boolean,
                       quantity: Int,
                       embarkDate: Option[String],
                       disembarkDate: Option[String])

object Participant {
  implicit val participantReads: Reads[Participant] = Json.reads[Participant]
}
