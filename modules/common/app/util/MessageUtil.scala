package modules.data_mapper.util

import java.util.Date
import java.util.concurrent.atomic.AtomicInteger

import model.FidelioAmenity
import modules.common.Settings
import modules.fidelio_io.model.FidelioAccountStatus.FidelioAccountStatus
import modules.fidelio_io.model.FidelioMessageUnit._
import modules.fidelio_io.model.{FidelioAccountIDEntryType, FidelioPostingDetails, _}
import modules.fidelio_io.util.{DateUtil, FidelioFailureException, FidelioServiceHelper}
import org.joda.time.{DateTime, Days}
import play.api.Logger
import play.api.cache._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.xml.NodeSeq

class MessageUtil {}

/**
  * Created by matt on 1/6/16.
  */
object MessageUtil {

  private val logger = Logger(classOf[MessageUtil])

  val cache = new GuiceApplicationBuilder().injector().instanceOf[AsyncCacheApi]

  val PORT_ATSEA = "ATSEA"

  val PACKAGE_EXCURSION = "EXCURSION"
  val PACKAGE_ENTERTAINMENT = "ENTERTAINMENT"
  val PACKAGE_DINING = "DINING"
  val PACKAGE_SPA = "SPA"
  val PACKAGE_TENDER_TICKET = "TENDER_TICKET"

  val STATUS_AVAILABLE = "A"

  val NO_RECORD_FOUND = "No record found"

  val rqnCounter = new AtomicInteger(0)

  /**
    * Generates a unique request number. This should be set as RQN of a request
    * message.
    *
    * @return
    */
  def generateRQN: String = {
    rqnCounter.addAndGet(1).toString
  }

  def retrieveReservationNotCached(
      reservationId: String
  )(implicit ec: ExecutionContext): Future[Try[FidelioReservation]] = {
    getReservationByIdAndStatus(
      reservationId.toInt,
      FidelioAccountStatus.CHECKED_IN
    )
  }

  def retrieveReservation(
      reservationId: String
  )(implicit ec: ExecutionContext): Future[Try[FidelioReservation]] = {
    val cacheKey = s"retrieveReservation-${reservationId}"

    cache.getOrElseUpdate(
      cacheKey,
      Duration(Settings.cacheDefaultTimeout, SECONDS)
    ) {
      retrieveReservationNotCached(reservationId)
    }
  }

  private def getReservationByIdAndStatus(
      reservationId: Int,
      fidelioAccountStatus: FidelioAccountStatus
  )(implicit ec: ExecutionContext): Future[Try[FidelioReservation]] = {

    val psFunction = FidelioMessageType.INQUIRE
    val strReservationId = s"${reservationId}"

    /**
      *  Fidelio Parameters:
      *  1. psSearchStr (Search String) = NCL client id (maps to UXP_A_FRQ_CARDNO in Fidelio)
      *  2. peSearchType (Search String type) = 5 (Search by External identification number (RES_BOOKNR))
      *  3. peAccountStatus (Account Status) = value passed in
      *  4. peAccountType (Account Type) = 0 (All)
      *  5. pbIncludePicture (Include Guest Picture in Return Result) = false
      */
    val psParam = Seq(
      strReservationId,
      FidelioFCUISearchType.EXTERNAL_IDENTIFICATION_NUMBER.id,
      fidelioAccountStatus.id,
      FidelioAccountType.ALL.id,
      false
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => {
        FidelioReservation.fromJson(js) match {
          case Success(r: FidelioReservation) => Success(r)
          case Failure(ex)                    => Failure(ex)
        }
      }
      case Failure(ex) => Failure(ex)
    }
  }

  private def getReservationByCabinAndStatus(
      cabinNumber: String,
      fidelioAccountStatus: FidelioAccountStatus
  )(implicit ec: ExecutionContext): Future[Try[FidelioReservation]] = {
    val psFunction = FidelioMessageType.INQUIRE

    /**
      *  Fidelio Parameters:
      *  1. psSearchStr (Search String) = value passed in
      *  2. peSearchType (Search String type) = 1 (Search by Cabin Number ((RES_CAB))
      *  3. peAccountStatus (Account Status) = value passed in
      *  4. peAccountType (Account Type) = 0 (All)
      *  5. pbIncludePicture (Include Guest Picture in Return Result) = false
      */
    val psParam = Seq(
      cabinNumber,
      FidelioFCUISearchType.CABIN_NUMBER_OR_SYSTEM_ACCOUNT.id,
      fidelioAccountStatus.id,
      FidelioAccountType.ALL.id,
      false
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => {
        FidelioReservation.fromJson(js) match {
          case Success(r: FidelioReservation) => Success(r)
          case Failure(ex)                    => Failure(ex)
        }
      }
      case Failure(ex) => Failure(ex)
    }
  }

  def retrieveReservationByCabin(
      cabinNumber: String
  )(implicit ec: ExecutionContext): Future[Try[FidelioReservation]] = {
    getReservationByCabinAndStatus(cabinNumber, FidelioAccountStatus.CHECKED_IN)
  }

  private def getClientByIdAndStatus(
      clientId: Int,
      fidelioAccountStatus: FidelioAccountStatus
  )(implicit ec: ExecutionContext): Future[Try[FidelioClientAccount]] = {
    buildGuestInquire(clientId, fidelioAccountStatus, false)
  }

  private def getGuestInfoAndPicture(
      clientId: Int,
      fidelioAccountStatus: FidelioAccountStatus
  )(implicit ec: ExecutionContext): Future[Try[String]] = {
    buildGuestInquire(clientId, fidelioAccountStatus, true) map {

      case Success(client) => Success(client.picture.getOrElse(""))
      case Failure(ex)     => Failure(ex)

    }
  }

  private def buildGuestInquire(
      clientId: Int,
      fidelioAccountStatus: FidelioAccountStatus,
      includePicture: Boolean
  )(implicit ec: ExecutionContext) = {

    val psFunction = FidelioMessageType.INQUIRE
    val strClientId = s"${clientId}"

    /**
      *  Fidelio Parameters:
      *  1. psSearchStr (Search String) = NCL client id (maps to UXP_A_FRQ_CARDNO in Fidelio)
      *  2. peSearchType (Search String type) = 12 (Search by Loyalty Number (UXP_A_FRQ_CARDNO))
      *  3. peAccountStatus (Account Status) = value passed in
      *  4. peAccountType (Account Type) = 0 (All)
      *  5. pbIncludePicture (Include Guest Picture in Return Result) = false
      */
    val psParam = Seq(
      strClientId,
      FidelioFCUISearchType.LOYALTY_NUMBER.id,
      fidelioAccountStatus.id,
      FidelioAccountType.ALL.id,
      includePicture
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => {
        val clientAccount = (js \ "sTables").head.as[JsObject]

        FidelioClientAccount.fromJson(clientAccount) match {
          case Success(c: FidelioClientAccount) => Success(c)
          case Failure(ex)                      => Failure(ex)
        }
      }
      case Failure(ex) => Failure(ex)
    }
  }

  private def getClients(
      ids: Set[Int],
      fidelioAccountStatus: FidelioAccountStatus
  )(implicit ec: ExecutionContext): Future[Set[Try[FidelioClientAccount]]] = {
    val clientsF = ids.map(getClientByIdAndStatus(_, fidelioAccountStatus))
    /*
            If at least one client retrieval fails, then Future.sequence will return that failure
            so that successes will be ignored
     */
    Future.sequence(clientsF)
  }

  def retrieveClient(
      clientId: String
  )(implicit ec: ExecutionContext): Future[Try[FidelioClientAccount]] = {
    getClientByIdAndStatus(clientId.toInt, FidelioAccountStatus.CHECKED_IN)
  }

  def retrieveGuestPicture(
      folioId: String
  )(implicit ec: ExecutionContext): Future[Try[String]] = {
    val psFunction = FidelioMessageType.WS_FUNCTION
    val psFunctionName = FidelioMessageType.GUEST_PICTURE

    /**
      * Fidelio Parameters
      * 1. pnAccID = Account Id
      */
    val psParam = Seq(psFunctionName, folioId)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {

      case Success(value) => {

        Try {
          val data = (value \ "sTables" \\ "Table0").head
          (data \ "SEC_IMAGE").as[String]
        }
      }

      case Failure(exception) => {
        Failure(exception)
      }

    }
  }

  def retrieveClientByCardNumber(
      cardNumber: String
  )(implicit ec: ExecutionContext): Future[Try[FidelioClientAccount]] = {
    val psFunction = FidelioMessageType.INQUIRE

    /**
      *  Fidelio Parameters:
      *  1. psSearchStr (Search String) = value passed in
      *  2. peSearchType (Search String type) = 0 (Search by Card identification number (RES_BOARDCC))
      *  3. peAccountStatus (Account Status) = 1 (Checked-In)
      *  4. peAccountType (Account Type) = 0 (All)
      *  5. pbIncludePicture (Include Guest Picture in Return Result) = true
      */
    val psParam = Seq(
      cardNumber,
      FidelioFCUISearchType.CARD_IDENTIFICATION_NUMBER.id,
      FidelioAccountStatus.CHECKED_IN.id,
      FidelioAccountType.ALL.id,
      true
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => {
        val clientAccount = (js \ "sTables").as[JsArray].head.toOption

        clientAccount match {
          case Some(client) => {
            FidelioClientAccount.fromJson(client) match {
              case Success(c: FidelioClientAccount) => Success(c)
              case Failure(ex)                      => Failure(ex)
            }
          }
          case None =>
            Failure(
              new Exception(
                s"Error retrieving client by card number $cardNumber"
              )
            )
        }
      }
      case Failure(ex) => Failure(ex)
    }
  }

  def updateInvoiceStatus(
      accountId: String,
      isPrinting: Boolean,
      email: String
  )(implicit ec: ExecutionContext): Future[Try[JsValue]] = {
    val psFunction = FidelioMessageType.UPDATE_INVOICE_STATUS

    /**
      * Fidelio Parameters
      * 1. pnAccID = Unique Guest Id
      * 2. pbBatchPrint = Print in Quick Billing
      * 3. psEmail = Ship Email Address
      */
    val psParam = Seq(accountId, isPrinting, email)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Failure(exception) => Failure(exception)
      case Success(value) => {
        Success(value)
      }
    }
  }

  def getInvoiceStatus(
      accountId: String
  )(implicit ec: ExecutionContext): Future[Try[InvoiceStatus]] = {
    val psFunction = FidelioMessageType.WS_FUNCTION
    val psFunctionName = FidelioMessageType.GET_INVOICE_STATUS

    /**
      * Fidelio Parameters
      * 1. pnAccID = Account Id
      */
    val psParam = Seq(psFunctionName, accountId)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {

      case Success(value) => {
        Try(InvoiceStatus.fromJson((value \ "sTables" \\ "Table0").head))
      }

      case Failure(exception) => {
        Failure(exception)
      }

    }

  }

  def retrieveCruiseNotCached(
      implicit ec: ExecutionContext
  ): Future[Try[FidelioCruise]] = {
    val psFunction = FidelioMessageType.WS_FUNCTION

    val psFunctionName = FidelioMessageType.SYSMSGREQ

    val psParam = Seq(psFunctionName)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => FidelioCruise.fromJson(js)
      case Failure(ex) => Failure(ex)
    }
  }

  def retrieveCruise(
      implicit ec: ExecutionContext
  ): Future[Try[FidelioCruise]] = {
    val cacheKey = s"retrieveCruise"

    cache.getOrElseUpdate(
      cacheKey,
      Duration(Settings.cacheDefaultTimeout, SECONDS)
    ) {
      retrieveCruiseNotCached
    }
  }

  /**
    * Retrieves the itinerary of the current voyage.
    *
    * @param clientAccount - The Fidelio account id
    * @return The itinerary response.
    */
  def retrieveItineraryNotCached(
      clientAccount: FidelioClientAccount
  )(implicit ec: ExecutionContext): Future[Try[FidelioItinerary]] = {
    val psFunction = FidelioMessageType.WS_FUNCTION
    val psFunctionName = FidelioMessageType.GET_CRUISE_ITINERARY
    val psParam = Seq(psFunctionName, clientAccount.fidelioAcctId)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) =>
        FidelioItinerary
          .fromJson(js, clientAccount.embarkDate, clientAccount.disembarkDate)
      case Failure(ex) => Failure(ex)
    }
  }

  def retrieveItinerary(
      clientAccount: FidelioClientAccount
  )(implicit ec: ExecutionContext): Future[Try[FidelioItinerary]] = {
    val cacheKey = s"retrieveItinerary-${clientAccount.nclClientId}"

    cache.getOrElseUpdate(
      cacheKey,
      Duration(Settings.cacheDefaultTimeout, SECONDS)
    ) {
      retrieveItineraryNotCached(clientAccount)
    }
  }

  def retrieveDiningFolio(
      fidelioAcctId: Int
  )(implicit ec: ExecutionContext): Future[Try[NodeSeq]] = {
    val reqXML = <SWCalendar xmlns="http://tempuri.org/SilverWeb/SilverWeb">
            <TransactionID>{generateRQN}</TransactionID>
            <RequestType>G</RequestType>
            <RequestId>{fidelioAcctId}</RequestId>
        </SWCalendar>

//        SilverWebDriver.sendMessage(reqXML)
    Future(
      Try(
        <SWCalendar xmlns="http://tempuri.org/SilverWeb/SilverWeb"></SWCalendar>
      )
    ) // TODO:
  }

  /**
    * Retrieves the excursion folio (i.e., list of excursions) for a Fidelio account id.
    *
    * @param fidelioAcctId
    * @return excursion folio for a guest
    */
  def retrieveExcursionFolio(
      fidelioAcctId: Int
  )(implicit ec: ExecutionContext): Future[Try[FidelioExcursionFolio]] = {

    val psFunction = FidelioMessageType.EXCFOLIO

    /**
      * Fidelio Parameters:
      * 1. pnAccID (Unique Guest ID) = Fidelio Account ID (value passed in)
      */
    val psParam = Seq(fidelioAcctId)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Failure(ex) => {
        Failure(ex)
      }
      case Success(js) => {
        FidelioExcursionFolio.fromJson(js, fidelioAcctId) match {
          case Success(excFolio: FidelioExcursionFolio) => {
            logger.debug("Parsing of FidelioExcursionFolio was successful.")
            Success(excFolio)
          }
          case Failure(ex) => {
            logger.debug("Error converting from JSON to FidelioExcursionFolio.")
            Failure(ex)
          }
        }
      }
    }
  }

  /**
    * Retrieves the event folio (i.e., list of events) for a Fidelio account id.
    *
    * @param fidelioAcctId
    * @return event folio for a guest
    */
  def retrieveEventFolio(
      fidelioAcctId: Int
  )(implicit ec: ExecutionContext): Future[Try[FidelioEventFolio]] = {
    val psFunction = FidelioMessageType.EVENTFOLIO

    /**
      * Fidelio Parameters:
      * 1. pnAccID (Unique Guest ID) = Fidelio Account ID (value passed in)
      */
    val psParam = Seq(fidelioAcctId)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Failure(ex) => {
        Failure(ex)
      }
      case Success(js) => {
        FidelioEventFolio.fromJson(js, fidelioAcctId) match {
          case Success(evtFolio: FidelioEventFolio) => {
            logger.debug("Parsing of FidelioEventFolio was successful.")
            Success(evtFolio)
          }
          case Failure(ex) => {
            logger.debug("Error converting from JSON to FidelioEventFolio.")
            Failure(ex)
          }
        }
      }
    }
  }

  /**
    * Returns the event folios for all the guests in a reservation
    *
    * @param reservation
    * @return event folios for the guests in a reservation
    */
  def retrieveEventFolios(
      reservation: FidelioReservation
  )(implicit ec: ExecutionContext): Future[Try[Seq[FidelioEventFolio]]] = {
    val guestList = reservation.clients
    val eventFutures = new ArrayBuffer[Future[Try[FidelioEventFolio]]]()

    for (guest <- guestList) {
      val fidelioAccountId = guest.fidelioAcctId
      eventFutures += retrieveEventFolio(fidelioAccountId)
    }

    Future
      .sequence(eventFutures)
      .map(resultList => {
        // See if there are any errors. If so, return the first error
        resultList.find { _.isFailure } match {
          case Some(Failure(ex)) => Failure(ex)
          case _ => {
            // Convert (filter and map) Seq[Try[FidelioEventFolio]] success cases into a Try[Seq[FidelioEventFolio]]
            Success(resultList.collect {
              case Success(r) => r
            })
          }
        }
      })
  }

  /**
    * Returns the excursion folios for all the guests in a reservation
    *
    * @param reservation
    * @return excursion folios for the guests in a reservation
    */
  def retrieveExcursionFolios(
      reservation: FidelioReservation
  )(implicit ec: ExecutionContext): Future[Try[Seq[FidelioExcursionFolio]]] = {
    val guestList = reservation.clients
    val excFutures = new ArrayBuffer[Future[Try[FidelioExcursionFolio]]]()

    for (guest <- guestList) {
      val fidelioAccountId = guest.fidelioAcctId
      excFutures += retrieveExcursionFolio(fidelioAccountId)
    }

    Future
      .sequence(excFutures)
      .map(resultList => {
        // See if there are any errors. If so, return the first error
        resultList.find { _.isFailure } match {
          case Some(Failure(ex)) => Failure(ex)
          case _ => {
            // Convert (filter and map) Seq[Try[FidelioExcursionFolio]] success cases into a Try[Seq[FidelioExcursionFolio]]
            Success(resultList.collect {
              case Success(r) => r
            })
          }
        }
      })
  }

  def retrieveDiningFolios(
      reservation: FidelioReservation
  )(implicit ec: ExecutionContext): Future[Try[Seq[NodeSeq]]] = {
    val guestList = reservation.clients
    val diningFutures = new ArrayBuffer[Future[Try[NodeSeq]]]()

    for (guest <- guestList) {
      val fidelioAccountId = guest.fidelioAcctId
      diningFutures += MessageUtil.retrieveDiningFolio(fidelioAccountId)
    }

    Future
      .sequence(diningFutures)
      .map(resultList => {
        // See if there are any errors. If so, return the first error
        resultList.find { _.isFailure } match {
          case Some(Failure(ex)) => Failure(ex)
          case _ => {
            // Convert (filter and map) Seq[Try[NodeSeq]] success cases into a Try[Seq[NodeSeq]]
            Success(resultList.collect {
              case Success(r) => r
            })
          }
        }
      })
  }

  /**
    * Retrieves the folio for a Fidelio account id minus any package folio items.
    *
    * @param fidelioAcctId
    * @return excursion folio for a guest
    */
  def retrieveFolio(
      fidelioAcctId: Int
  )(implicit ec: ExecutionContext): Future[Try[FidelioFolio]] = {
    // retrieve folio and package folio in parallel
    val folioFuture = getFolioUnfiltered(fidelioAcctId)
    val packageFolioFuture = getPackageFolio(fidelioAcctId)

    for {
      folio <- folioFuture
      packageFolio <- packageFolioFuture
    } yield {
      (folio, packageFolio) match {
        case (Success(f), Success(pf)) => {
          // Filter out any package folio items from the folio,
          // since they don't apply to the guest account (instead are promotional in nature, such as a
          // dining package for 5 restaurants, and apply to a house account).
          // Match items in folio with items in package folio based on the subdepartment
          // and posting transaction id.
          val nonPackageFolioItems = f.items.filter { folioItem =>
            !pf.items.exists(
              pkgFolioItem =>
                pkgFolioItem.subdepartment == folioItem.subdepartment &&
                  pkgFolioItem.postingTransactionId == folioItem.postingTransactionId
            )
          }

          Success(FidelioFolio(nonPackageFolioItems))
        }
        case (e1, e2) => {
          Failure(new Exception(s"Error retrieving folio $e1, $e2"))
        }
      }
    }
  }

  /**
    * Retrieves the folio for a Fidelio account id.
    *
    * @param fidelioAcctId
    * @return excursion folio for a guest
    */
  private def getFolioUnfiltered(
      fidelioAcctId: Int
  )(implicit ec: ExecutionContext): Future[Try[FidelioFolio]] = {
    val psFunction = FidelioMessageType.WS_FUNCTION
    val psFunctionName = FidelioMessageType.FOLIO
    val psParam = Seq(psFunctionName, fidelioAcctId)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) =>
        FidelioFolio.fromJson(js)
      case Failure(ex: FidelioFailureException) => {
        if (NO_RECORD_FOUND == ex.getMessage) {
          Success(FidelioFolio())
        } else {
          Failure(ex)
        }
      }
      case _ =>
        Failure(
          new Exception(
            s"Error retrieving folio for Fidelio account id $fidelioAcctId"
          )
        )
    }
  }

  /**
    * Retrieves the package folio for a Fidelio account id.
    *
    * @param fidelioAcctId
    * @return excursion folio for a guest
    */
  private def getPackageFolio(
      fidelioAcctId: Int
  )(implicit ec: ExecutionContext): Future[Try[FidelioFolio]] = {
    val psFunction = FidelioMessageType.WS_FUNCTION
    val psFunctionName = FidelioMessageType.PACKAGE_FOLIO
    val psParam = Seq(psFunctionName, fidelioAcctId)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => FidelioFolio.fromJson(js)
      case Failure(ex: FidelioFailureException) => {
        if (NO_RECORD_FOUND == ex.getMessage) {
          Success(FidelioFolio())
        } else {
          Failure(ex)
        }
      }
      case _ =>
        Failure(
          new Exception(
            s"Error retrieving package folio for Fidelio account id $fidelioAcctId"
          )
        )
    }
  }

  /**
    * Retrieves all the excursions for a reservation
    *
    * @param reservation
    * @return excursions for the reservation
    */
  def retrieveExcursions(
      reservation: FidelioReservation
  )(implicit ec: ExecutionContext): Future[Try[Seq[FidelioExcursion]]] = {
    retrieveExcursions(
      reservation.clients.head.embarkDate,
      reservation.clients.head.disembarkDate,
      reservation
    )
  }

  /**
    * Retrieves all the excursions for a reservation for the date range specified
    *
    * @param startDate
    * @param endDate
    * @param reservation
    * @return excursions for the reservation for the date range specified
    */
  def retrieveExcursions(
      startDate: Date,
      endDate: Date,
      reservation: FidelioReservation
  )(implicit ec: ExecutionContext): Future[Try[Seq[FidelioExcursion]]] = {
    val fidelioAcctId = retrieveFirstFidelioAccountId(reservation)

    val psFunction = FidelioMessageType.EXCURSIONS

    val strStartDate =
      DateUtil.formatDate(startDate, DateUtil.FORMAT_FIDELIO_DATE)
    val strEndDate = DateUtil.formatDate(endDate, DateUtil.FORMAT_FIDELIO_DATE)

    /**
      * Fidelio Parameters:
      * 1. pnAccID (Unique Guest ID) = Fidelio Account ID (part of FidelioReservation passed in)
      * 2. pdSDate (Excursion start date) = Lower end of date range for searching for excursions
      * 3. pdEDate (Excursion end date) = Upper end of date range for searching for excursions
      * 4. peCodeType (Code Type) = 0 (None) - Not searching by a specific code, so set to None
      * 5. psCode (Code) = empty string
      */
    val psParam = Seq(
      fidelioAcctId,
      strStartDate,
      strEndDate,
      FidelioExcursionCodeType.NONE.id,
      ""
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => {
        val excursions = ArrayBuffer[FidelioExcursion]()

        jsonArrayForEach(
          js \ "sTables" \ "Excursion", { (item: JsValue) =>
            FidelioExcursion.fromJson(item) match {
              case Success(excursion: FidelioExcursion) => {
                logger.debug("Parsing of FidelioExcursion was successful.")

                // Filter out excursions where the excursion number is less than 8 characters
                // long.  These typically correspond with promotions that act as discounts
                // and should not be shown to the guest.
                if (excursion.excursionNumber.length == 8) {
                  excursions.append(excursion)
                }
              }
              case Failure(ex) => {
                logger.debug("Error converting from JSON to FidelioExcursion.")
                Failure(ex)
              }
            }
          }
        )

        Success(excursions)
      }
      case Failure(ex) => {
        Failure(ex)
      }
    }
  }

  /**
    * Retrieves all the events for a reservation
    *
    * @param clientAccount
    * @param reservation
    * @return events for the reservation
    */
  def retrieveEvents(
      clientAccount: FidelioClientAccount,
      reservation: FidelioReservation
  )(implicit ec: ExecutionContext): Future[Try[Seq[FidelioEvent]]] = {
    retrieveEvents(
      clientAccount.embarkDate,
      clientAccount.disembarkDate,
      reservation
    )
  }

  /**
    * Retrieves all the events for a reservation for the date range specified
    *
    * @param startDate
    * @param endDate
    * @param reservation
    * @return events for the reservation
    */
  def retrieveEvents(
      startDate: Date,
      endDate: Date,
      reservation: FidelioReservation
  )(implicit ec: ExecutionContext): Future[Try[Seq[FidelioEvent]]] = {
    val fidelioAcctId = retrieveFirstFidelioAccountId(reservation)

    val strStartDate =
      DateUtil.formatDate(startDate, DateUtil.FORMAT_FIDELIO_DATE)
    val strEndDate = DateUtil.formatDate(endDate, DateUtil.FORMAT_FIDELIO_DATE)

    val psFunction = FidelioMessageType.EVENTS

    /**
      * Fidelio Parameters:
      * 1. pnAccID (Unique Guest ID) = Fidelio Account ID (part of FidelioReservation passed in)
      * 2. pdSDate (Event start date) = Lower end of date range for searching for an event
      * 3. pdEDate (Event end date) = Upper end of date range for searching for an event
      * 4. peCodeType (Code Type) = 0 (None) - Not searching by a specific code, so set to None
      * 5. psCode (Code) = empty string
      */
    val psParam = Seq(
      fidelioAcctId,
      strStartDate,
      strEndDate,
      FidelioEventCodeType.NONE.id,
      ""
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Success(js) => {
        val events = ArrayBuffer[FidelioEvent]()

        jsonArrayForEach(
          js \ "sTables" \ "Event", { (item: JsValue) =>
            FidelioEvent.fromJson(item) match {
              case Success(event: FidelioEvent) => {
                logger.debug("Parsing of FidelioEvent was successful.")
                events.append(event)
              }
              case Failure(ex) => {
                logger.debug("Error converting from JSON to FidelioEvent.")
                Failure(ex)
              }
            }
          }
        )

        Success(events)
      }
      case Failure(ex) => {
        Failure(ex)
      }
    }
  }

  /**
    * Returns the cruise name.
    * The cruise name is derived from the cruise id and name
    * by extracting the text that comes after the first dash (-)
    * (e.g., 1286-Miami - 7 Day).
    *
    * Note that cruise names themselves might also have dashes
    * as part of the name (e.g., Miami - 7 Day).
    *
    * @param cruiseIdAndName
    * @return cruise name
    */
  def getCruiseName(cruiseIdAndName: String): String = {
    val firstDashIndex = cruiseIdAndName.indexOf("-")

    cruiseIdAndName.substring(firstDashIndex + 1).trim()
  }

  /**
    * Returns the event code that Drupal is expecting.
    * The event code is derived from the Fidelio event
    * number by extracting the first 6 characters
    *
    * @param eventNumber
    * @return event code
    */
  def getEventCode(eventNumber: String): String = {
    eventNumber.substring(0, 6)
  }

  /**
    * Returns the excursion code that Drupal is expecting.
    * The excursion code is derived from the Fidelio excursion
    * number by doing the following:
    *  - Extract first 3 letters
    *  - Replace fourth letter with an 'N'
    *  - Extract next 2 digits
    *
    * Fidelio excursion number is assumed to be 8 characters long. The last two characters
    * are discarded by this method.
    *
    * Example: Fidelio excursion number "RTBX0312" is returned as "RTBN03"
    *
    * @param excursionNumber
    * @return excursion code
    */
  def getExcursionCode(excursionNumber: String): String = {
    excursionNumber.substring(0, 3) + "N" + excursionNumber.substring(4, 6)
  }

  def getShipCode(shipName: String): String = {
    val prefix = "Norwegian "

    if (shipName.indexOf(prefix) >= 0) {
      return shipName.substring(prefix.length, shipName.length).toUpperCase
    }

    if (shipName.equalsIgnoreCase("Pride of America")) {
      return "PRIDE_AMER"
    }

    return ""
  }

  /**
    * Returns the deck number from the deck name.
    *
    * @param deckName
    * @return deck number
    */
  def getDeckNumber(deckName: String): String = {
    val index = deckName.indexOf("DCK")

    if (index != -1) {
      // index must be offset by 3 characters,
      // since "DCK" is 3 characters long
      // to get the deck number
      return deckName.substring(index + 3)
    } else {
      return deckName
    }
  }

  /**
    * Returns the port code that Drupal is expecting.
    * The port code is derived from the Fidelio excursion
    * number by extracting the first 3 letters
    *
    * @param eventOrExcursionNumber
    * @return port code
    */
  def getPortCodeFromEventOrExcursionNumber(
      eventOrExcursionNumber: String
  ): String = {
    eventOrExcursionNumber.substring(0, 3)
  }

  /**
    * Returns the port code that Drupal is expecting.
    * Assuming the port is not "AtSea", the port code
    * is derived by extracting the last 3 letters of the port
    * and uppercasing the value.  Otherwise, if it's
    * "AtSea", the port code is "ATSEA".
    *
    * @param port
    * @return port code
    */
  def getPortCodeFromPort(port: String): String = {
    if (PORT_ATSEA.equalsIgnoreCase(port)) {
      port.toUpperCase()
    } else {
      port.substring(port.length() - 3).toUpperCase
    }
  }

  /**
    * Retrieves first Fidelio account id from reservation
    *
    * @param reservation
    * @return first Fidelio account id
    */
  def retrieveFirstFidelioAccountId(reservation: FidelioReservation): Int = {
    reservation.clients.head.fidelioAcctId
  }

  /**
    * Retrieves guest ref number from Fidelio account id for given reservation
    *
    * @param fidelioAccountId
    * @param reservation
    * @return guest ref number
    */
  def findGuestRefNbrForFidelioAcctId(
      fidelioAccountId: String,
      reservation: FidelioReservation
  ): Option[String] = {
    val guests = reservation.clients

    // a dumb sequential number starting at 1
    var guestRefNumber = 1
    var foundGuestRefNumber = false
    var matchedGuestRefNumber: Option[String] = None

    for (guest <- guests) {
      if (!foundGuestRefNumber && fidelioAccountId.equals(guest.fidelioAcctId)) {
        matchedGuestRefNumber = Some(guestRefNumber.toString)
        foundGuestRefNumber = true
      }

      guestRefNumber += 1
    }

    return matchedGuestRefNumber
  }

  def getNumAvailableSeats(pkg: FidelioExcursion): Int = {
    pkg.maximumSeats - pkg.seatsCurrentlyBooked
  }

  def getNumAvailableSeats(pkg: FidelioEvent): Int = {
    pkg.maximumSeats - pkg.seatsCurrentlyBooked
  }

  def isTenderTicket(pkg: FidelioEvent): Boolean = {
    pkg.eventNumber.startsWith("TND")
  }

  def isTenderTicket(pkg: FidelioEventFolioItem): Boolean = {
    pkg.eventNumber.startsWith("TND")
  }

  def retrieveEvent(clientAccount: FidelioClientAccount, packageId: Int)(
      implicit ec: ExecutionContext
  ): Future[Try[FidelioEvent]] = {
    val startDate = DateUtil.formatDate(
      clientAccount.embarkDate,
      DateUtil.FORMAT_FIDELIO_DATE
    )
    val endDate = DateUtil.formatDate(
      clientAccount.disembarkDate,
      DateUtil.FORMAT_FIDELIO_DATE
    )

    retrieveEvent(startDate, endDate, clientAccount.fidelioAcctId, packageId)
  }

  def retrieveEvent(
      startDate: String,
      endDate: String,
      fidelioAccountId: Int,
      packageId: Int
  )(implicit ec: ExecutionContext): Future[Try[FidelioEvent]] = {
    val psFunction = FidelioMessageType.EVENTS

    /**
      * Fidelio Parameters:
      * 1. pnAccID (Unique Guest ID) = Fidelio Account ID (value passed in)
      * 2. pdSDate (Event start date) = Lower end of date range for searching for an event (set to embark date)
      * 3. pdEDate (Event end date) = Upper end of date range for searching for an event (set to disembark date)
      * 4. peCodeType (Code Type) = 3 (Event Unique ID (EvtID)) - Searching by event id
      * 5. psCode (Code) = Value passed in (i.e., packageId)
      */
    val psParam = Seq(
      fidelioAccountId,
      startDate,
      endDate,
      FidelioEventCodeType.UNIQUE_ID.id,
      packageId
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Failure(ex) => {
        Failure(ex)
      }
      case Success(js) => {
        FidelioEvent.fromJson(js \ "sTables" \ "Event") match {
          case Success(event: FidelioEvent) => {
            logger.debug("Parsing of FidelioEvent was successful.")
            Success(event)
          }
          case Failure(ex) => {
            logger.debug("Error converting from JSON to FidelioEvent.")
            Failure(ex)
          }
        }
      }
    }
  }

  def retrieveExcursion(clientAccount: FidelioClientAccount, packageId: Int)(
      implicit ec: ExecutionContext
  ): Future[Try[FidelioExcursion]] = {
    val startDate = DateUtil.formatDate(
      clientAccount.embarkDate,
      DateUtil.FORMAT_FIDELIO_DATE
    )
    val endDate = DateUtil.formatDate(
      clientAccount.disembarkDate,
      DateUtil.FORMAT_FIDELIO_DATE
    )

    val psFunction = FidelioMessageType.EXCURSIONS

    /**
      * Fidelio Parameters:
      * 1. pnAccID (Unique Guest ID) = Fidelio Account ID (part of FidelioClientAccount passed in)
      * 2. pdSDate (Event start date) = Lower end of date range for searching for an excursion (set to embark date)
      * 3. pdEDate (Event end date) = Upper end of date range for searching for an excursion (set to disembark date)
      * 4. peCodeType (Code Type) = 3 (Excursion Unique ID (ExcID)) - Searching by excursion id
      * 5. psCode (Code) = Value passed in (i.e., packageId)
      */
    val psParam = Seq(
      clientAccount.fidelioAcctId,
      startDate,
      endDate,
      FidelioExcursionCodeType.UNIQUE_ID.id,
      packageId
    )

    FidelioServiceHelper.getJSON(psFunction, psParam).map {
      case Failure(ex) => {
        Failure(ex)
      }
      case Success(js) => {
        FidelioExcursion.fromJson(js \ "sTables" \ "Excursion") match {
          case Success(excursion: FidelioExcursion) => {
            logger.debug("Parsing of FidelioExcursion was successful.")
            Success(excursion)
          }
          case Failure(ex) => {
            logger.debug("Error converting from JSON to FidelioExcursion.")
            Failure(ex)
          }
        }
      }
    }
  }

  /**
    * Returns the formatted cruise duration for a client using the PND format. For
    * example: P7D.
    *
    * @param clientAccount - The client account.
    * @return The formatted duration.
    */
  def getCruiseDurationFormatted(
      clientAccount: FidelioClientAccount
  ): String = {
    val duration = Days
      .daysBetween(
        new DateTime(clientAccount.embarkDate),
        new DateTime(clientAccount.disembarkDate)
      )
      .getDays()

    return s"P${duration}D"
  }

  /**
    * Determines if the child path into the Json structure passed in is null, a scalar, or an array.
    * If it's null, then it doesn't execute the lambda passed in at all.  If it's a scalar,
    * it executes the lambda once.  If it's an array, it executes it multiple times.
    *
    * @param childLookup - a JsLookupResult that points to a child (e.g., js \ "sTables" \ "excFolio")
    * @param lambda - the code that you want to execute 0 or more times for the child json
    */
  def jsonArrayForEach(childLookup: JsLookupResult, lambda: (JsValue) => Unit) {

    val childVal = childLookup match {
      case v: JsDefined => Some(v.get)
      case _            => None
    }

    childVal match {
      case Some(vScalar: JsObject) => lambda(vScalar)
      case Some(vArray: JsArray)   => vArray.value.foreach(lambda)
      case _                       => None
    }
  }

  /**
    * Returns a Some(JObject) if the lookup is an object.  Else returns None.
    *
    * @param lookup
    * @return Option[JsObject]
    */
  def jsonObjOpt(lookup: JsLookupResult): Option[JsObject] = {
    val lookupVal = lookup match {
      case v: JsDefined => Some(v.get)
      case _            => None
    }

    lookupVal match {
      case Some(vScalar: JsObject) => Some(vScalar)
      case _                       => None
    }
  }

  def makeGenericPayment(clientId: Int, paymentDetails: FidelioPaymentDetails)(
      implicit ec: ExecutionContext
  ): Future[Try[JsValue]] = {
    val psFunction = FidelioMessageType.GENERIC_PAYMENT
    val strClientId = s"${clientId}"

    /**
      *  Fidelio Parameters:
      *  1. psSearchStr (Search String) = NCL client id (maps to UXP_A_FRQ_CARDNO in Fidelio)
      *  2. peSearchType (Search String type) = 12 (Search by Loyalty Number (UXP_A_FRQ_CARDNO))
      *  3. peAccountStatus (Account Status) = 1 (Checked-In)
      *  4. peAccountType (Account Type) = 0 (All)
      *  5. pbIncludePicture (Include Guest Picture in Return Result) = false
      *  6. psUniquePostingID (Unique Posting ID) = part of payment details passed in
      *  7. psOutletID (Outlet ID) = part of payment details passed in
      *  8. pnValue (Posting Total) = part of payment details passed in
      *  9. psNote (Notes) = part of payment details passed in
      *  10. pnInvoiceWin (Invoice Windows (0 - 3)) = 0
      */
    val psParam = Seq(
      strClientId,
      FidelioFCUISearchType.LOYALTY_NUMBER.id,
      FidelioAccountStatus.CHECKED_IN.id,
      FidelioAccountType.ALL.id,
      false,
      paymentDetails.postingId,
      paymentDetails.outletId,
      paymentDetails.paymentAmount,
      paymentDetails.notes,
      0
    )

    FidelioServiceHelper.getJSON(psFunction, psParam)
  }

  def makeGenericPosting(clientId: String,
                         postingDetails: FidelioPostingDetails)(
      implicit ec: ExecutionContext
  ): Future[Try[JsValue]] = {
    val psFunction = FidelioMessageType.GENERIC_PURCHASE
    val strClientId = clientId //s"${clientId}"

    /**
      *  Fidelio Parameters:
      *  1. psSearchStr (Search String) = NCL client id (maps to UXP_A_FRQ_CARDNO in Fidelio)
      *  2. peSearchType (Search String type) = 12 (Search by Loyalty Number (UXP_A_FRQ_CARDNO))
      *  3. peAccountStatus (Account Status) = 1 (Checked-In)
      *  4. peAccountType (Account Type) = 0 (All)
      *  5. pbIncludePicture (Include Guest Picture in Return Result) = false
      *  6. poPosting (Posting Data) = posting details passed in
      */
    val psParam = Seq(
      strClientId,
      FidelioFCUISearchType.LOYALTY_NUMBER.id,
      FidelioAccountStatus.CHECKED_IN.id,
      FidelioAccountType.ALL.id,
      false,
      Json.toJson(postingDetails).toString
    )

    FidelioServiceHelper.getJSON(psFunction, psParam)
  }

  def uploadReceipt(
      clientAccount: FidelioClientAccount,
      receiptImage: String,
      notes: String
  )(implicit ec: ExecutionContext): Future[Try[JsValue]] = {
    val psFunction = FidelioMessageType.UPDATE_COMMENT

    val date = new Date()
    val requestedDate = DateUtil.formatDate(date, DateUtil.FORMAT_FIDELIO_DATE)
    val resolvedDate = DateUtil.formatDate(date, DateUtil.FORMAT_FIDELIO_DATE)

    // must end in something other than .doc, docx, or .pdf to be considered a picture by Fidelio
    val fileName =
      s"receipt-${clientAccount.reservationId}-${clientAccount.nclClientId}-${requestedDate}.png"

    val psParam = Seq(
      FidelioQuickAssignment.NONE.id, // peQuickAssignment (Quick Assignment)
      0, // pnCplID (Unique Comment ID) = 0 (for add new comment)
      0, // pnCplLinkID (Comment Link ID)
      clientAccount.fidelioAcctId, // pnAccID (Account Type)
      notes, // psComment (Comment Description)
      "", // psCommentCategory (Comment Category)
      0.0, // pnCost (Comment/Resolution Cost)
      "", // psDepartmentInvolve (List of Department Involve Codes)
      "", // psDescCode (Comment/Resolution Description Code)
      notes, // psDesc (Comment Description)
      "", // psExternalCode (External ID)
      fileName, // psFileName (Filename)
      false, // pbInternal (Internal Flag)
      "", // psInvolvePerson (Person who did resolution or reported complaint)
      true, // pbComment (Comment flag, true = Comment or false = Resolution)
      false, // pbSensitive (Sensitive Flag)
      false, // pbUrgent (Urgent Flag)
      receiptImage, // pbyPicture (Document or Picture)
      "", // psReportType (Report Type)
      requestedDate, // pdRequestDate (Requested Resolution Date)
      false, // pbResolved (Resolved - only applies to resolution)
      resolvedDate, // pdResolvedDate (Resolved Date)
      0, // pnVendorID (Vendor ID or Account ID)
      "", // psVid (External Unique ID for use with import)
      0 // pnXDocID (Unique XDOC ID, the table where the picture is stored)
    )

    FidelioServiceHelper.postJSON(psFunction, psParam)
  }

  def makeGenericCrewPosting(
      clientId: String,
      postingDetails: FidelioPostingDetails
  ): Future[Try[FidelioMessage]] = {
    val units = Map(
      ACI -> clientId,
      ACE -> FidelioAccountIDEntryType.INTERNAL_IDENTIFICATION_NUMBER,
      ACS -> FidelioAccountStatus.CHECKED_IN,
      POI -> postingDetails.uniquePostingId,
      POO -> postingDetails.outletId,
      POV -> postingDetails.totalAmount.toString,
      POC -> postingDetails.serviceCharge.toString,
      POT -> postingDetails.tip.toString,
      INF -> postingDetails.notes,
      POX -> postingDetails.tax.toString
    )

//    fidelioDriver.sendMessage(
//      createFidelioRequest(FidelioMessageType.GENERIC_PURCHASE, units))
    ???
  }

  def createFidelioRequest(messageType: String,
                           pUnits: Map[String, String]): FidelioMessage = {
    val units = Map(
      REF -> FidelioMessage.SENDER_REFERENCE,
      RQN -> FidelioMessage.generateRQN,
      DTE -> DateUtil.formatDate(new Date, DateUtil.FORMAT_FIDELIO_DATE)
    )

    new FidelioMessage(messageType, units ++ pUnits)
  }

  def deleteComment(
      commentId: Int
  )(implicit ec: ExecutionContext): Future[Try[JsValue]] = {
    val psFunction = FidelioMessageType.DELETE_COMMENT
    val psParam = Seq(commentId, 0)

    FidelioServiceHelper.postJSON(psFunction, psParam)
  }

  def getAmenity(accountId: Int)(implicit ec: ExecutionContext): Future[Try[Seq[FidelioAmenity]]] = {
    val psFunction = FidelioMessageType.GUEST_AMENITY
    val psParam = Seq(accountId)


    FidelioServiceHelper.getJSON(psFunction, psParam).map {

      case Success(js) => {
        val amenities = ArrayBuffer[FidelioAmenity]()

        jsonArrayForEach(
          js \ "sTables" \ "Table0", { (item: JsValue) =>
            FidelioAmenity.fromJson(item) match {
              case Success(amenity: FidelioAmenity) => {
                logger.debug("Parsing of FidelioAmenity was successful.")
                amenities.append(amenity)
              }
              case Failure(ex) => {
                logger.debug("Error converting from JSON to FidelioAmenity.")
                Failure(ex)
              }
            }
          }
        )

        Success(amenities)
      }
      case Failure(ex) => {
        Failure(ex)
      }
    }

//      case Success(value) => {
//
//        (value \ "sTables" \\ "Table0").head
//          .map(FidelioAmenity.fromJson(_))
//          .getOrElse(
//            Failure(
//              new Exception(
//                s"Couldn't parse Amenity response - $value"
//              )
//            )
//          )
//      }
//
//      case Failure(exception) => {
//        Failure(exception)
//      }
    }


  def getGuestKeyCard(
      accountNumber: String,
      psFunctionName: String = FidelioMessageType.GUEST_KEY_CARD_SCAN
  )(implicit ec: ExecutionContext): Future[Try[FidelioClientKeyCard]] = {
    val psFunction = FidelioMessageType.WS_FUNCTION

    /**
      * Fidelio Parameters
      * 1. pnAccID = Account Id
      */
    val psParam = Seq(psFunctionName, accountNumber)

    FidelioServiceHelper.getJSON(psFunction, psParam).map {

      case Success(value) => {

        (value \ "sTables" \\ "Table0").headOption
          .map(FidelioClientKeyCard.fromJson)
          .getOrElse(
            Failure(
              new Exception(
                s"Couldn't parse Key Card response from WS - $value"
              )
            )
          )
      }

      case Failure(exception) => {
        Failure(exception)
      }

    }

  }
}
