package modules.data_mapper.util

/**
  * Created by matt on 1/11/16.
  */
object CurrencyUtil {

  def convertDollarsToCents(dollars: Double): Integer = {
    val cents = dollars * 100
    cents.toInt
  }

  def formatAmount(price: Double): String = {
    "%.2f".format(price)
  }
}
