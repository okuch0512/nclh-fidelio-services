package modules.fidelio_io.model

import java.text.SimpleDateFormat
import java.util.Date

import modules.fidelio_io.util.DateUtil
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.Try

/*
TITLE MR
FNAME THOMAS
LNAME UZVBIZJCZG
LATITUDES PLATINUM
RES_STATUS CC
GUEST_TYPE P
ASSEMBLY_STATION G7
CABIN 15260
IS_SPA_CABIN 1
IS_HAVEN 1
PACKAGES 3UDP / UBP
CAB_TYPE H9
HAS_PC 0
IS_ADULT 1
AGE 71
SHIP Norwegian Bliss
EDEBARK 20190707000000
 */
case class FidelioClientKeyCard(folioId: String,
                                title: String,
                                firstName: String,
                                lastName: String,
                                latitudes: String,
                                resStatus: String,
                                guestType: String,
                                assemblyStation: String,
                                cabin: String,
                                isSpaCabin: Boolean,
                                isHaven: Boolean,
                                packages: String,
                                cabinType: String,
                                hasPC: Boolean,
                                isAdult: Boolean,
                                age: Int,
                                ship: String,
                                debarkDate: Date,
                                classification: String)

object FidelioClientKeyCard {

  implicit val fidelioClientKeyCardtWriter: Writes[FidelioClientKeyCard] = (
    (JsPath \ "FOLIO").write[String] and
      (JsPath \ "TITLE").write[String] and
      (JsPath \ "FNAME").write[String] and
      (JsPath \ "LNAME").write[String] and
      (JsPath \ "LATITUDES").write[String] and
      (JsPath \ "RES_STATUS").write[String] and
      (JsPath \ "GUEST_TYPE").write[String] and
      (JsPath \ "ASSEMBLY_STATION").write[String] and
      (JsPath \ "CABIN").write[String] and
      (JsPath \ "IS_SPA_CABIN").write[Boolean] and
      (JsPath \ "IS_HAVEN").write[Boolean] and
      (JsPath \ "PACKAGES").write[String] and
      (JsPath \ "CAB_TYPE").write[String] and
      (JsPath \ "HAS_PC").write[Boolean] and
      (JsPath \ "IS_ADULT").write[Boolean] and
      (JsPath \ "AGE").write[Int] and
      (JsPath \ "SHIP").write[String] and
      (JsPath \ "EDEBARK").write[Date] and
      (JsPath \ "CLASSIFICATION").write[String]
  )(unlift(FidelioClientKeyCard.unapply))

  /**
    * represent 'FidelioClientKeyCard' as Json returned from Fidelio calls
    * @param keyCard
    * @return
    */
  def toJson(keyCard: FidelioClientKeyCard): JsValue = {
    JsObject(
      Seq(
        "FOLIO" -> JsString(keyCard.folioId),
        "TITLE" -> JsString(keyCard.title),
        "FNAME" -> JsString(keyCard.firstName),
        "LNAME" -> JsString(keyCard.lastName),
        "LATITUDES" -> JsString(keyCard.latitudes),
        "RES_STATUS" -> JsString(keyCard.resStatus),
        "GUEST_TYPE" -> JsString(keyCard.guestType),
        "ASSEMBLY_STATION" -> JsString(keyCard.assemblyStation),
        "CABIN" -> JsString(keyCard.cabin),
        "IS_SPA_CABIN" -> JsBoolean(keyCard.isSpaCabin),
        "IS_HAVEN" -> JsBoolean(keyCard.isHaven),
        "PACKAGES" -> JsString(keyCard.packages),
        "CAB_TYPE" -> JsString(keyCard.cabinType),
        "HAS_PC" -> JsBoolean(keyCard.hasPC),
        "IS_ADULT" -> JsBoolean(keyCard.isAdult),
        "AGE" -> JsString(keyCard.age.toString),
        "SHIP" -> JsString(keyCard.ship),
        "EDEBARK" -> JsString(
          new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
            .format(keyCard.debarkDate)
        ),
        "CLASSIFICATION" -> JsString(keyCard.classification)
      )
    )
  }

  def fromJson(js: JsValue): Try[FidelioClientKeyCard] = {

    Try {
      val folioId = Try((js \ "FOLIO").as[String]).getOrElse("")
      val title = Try((js \ "TITLE").as[String]).getOrElse("")
      val fname = Try((js \ "FNAME").as[String]).getOrElse("")
      val lname = Try((js \ "LNAME").as[String]).getOrElse("")
      val latitudes = Try((js \ "LATITUDES").as[String]).getOrElse("")
      val resStatus = Try((js \ "RES_STATUS").as[String]).getOrElse("")
      val guestType = Try((js \ "GUEST_TYPE").as[String]).getOrElse("")
      val assembly = Try((js \ "ASSEMBLY_STATION").as[String]).getOrElse("")
      val cabin = Try((js \ "CABIN").as[String]).getOrElse("")
      val isSpa =
        Try((js \ "IS_SPA_CABIN").as[String].equals("1")).getOrElse(false)
      val isHaven =
        Try((js \ "IS_HAVEN").as[String].equals("1")).getOrElse(false)
      val packages = Try((js \ "PACKAGES").as[String]).getOrElse("")
      val cabinType = Try((js \ "CAB_TYPE").as[String]).getOrElse("")
      val hasPc = Try((js \ "HAS_PC").as[String].equals("1")).getOrElse(false)
      val isAdult =
        Try((js \ "IS_ADULT").as[String].equals("1")).getOrElse(false)
      val age = Try((js \ "AGE").as[String]).getOrElse("")
      val ship = Try((js \ "SHIP").as[String]).getOrElse("")
      val debark = Try((js \ "EDEBARK").as[String]).getOrElse("")
      val classification = Try((js \ "CLASSIFICATION").as[String]).getOrElse("")

      FidelioClientKeyCard(
        folioId,
        title,
        fname,
        lname,
        latitudes,
        resStatus,
        guestType,
        assembly,
        cabin,
        isSpa,
        isHaven,
        packages,
        cabinType,
        hasPc,
        isAdult,
        age.toInt,
        ship,
        DateUtil.parseDate(debark, DateUtil.FORMAT_FIDELIO_DATE),
        classification
      )
    }
  }
}
