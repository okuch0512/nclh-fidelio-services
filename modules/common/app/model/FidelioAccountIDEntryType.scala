package modules.fidelio_io.model

/**
  * Created by matt on 12/22/15.
  */
object FidelioAccountIDEntryType {

  val CARD_IDENTIFICATION_NUMBER = "C"
  val CABIN_NUMBER_OR_SYSTEM_ACCOUNT = "A"
  val NAME_OF_PERSON_OR_COMPANY_ACCOUNT = "N"
  val INTERNAL_IDENTIFICATION_NUMBER = "I"
  val GROUP_ID_NUMBER = "G"
  val EXTERNAL_IDENTIFICATION_NUMBER = "E"
  val PHONE_TRUNK_NUMBER = "T"
  val EMAIL_ID = "M"
  val INTERNAL_ID_NUMBER = "H"
  val GUEST_CATEGORY = "X"
  val RFID_UID = "R"
  val LOYALTY_NUMBER = "F"
  val MANNING_NUMBER_SAFETY_NUMBER = "S"
  val PREGENERATED_RANDOM_NUMBER = "P"
}
