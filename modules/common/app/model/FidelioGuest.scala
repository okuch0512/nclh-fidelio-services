package modules.data_mapper.model

import modules.fidelio_io.model.{FidelioEventFolio, FidelioExcursionFolio}

/**
  * Created by matt on 1/29/16.
  */
class FidelioGuest(val fidelioAccountId: Int,
                   val guestRefNumber: Int,
                   val firstName: String,
                   val lastName: String,
                   val title: String,
                   val cabin: String,
                   val gender: String,
                   val telephone: String,
                   val email: String,
                   val address: Address) {

  var excFolio: FidelioExcursionFolio = FidelioExcursionFolio(Seq())
  var eventFolio: FidelioEventFolio = FidelioEventFolio(Seq())
}

case class Address(address1: String,
                   address2: String,
                   city: String,
                   state: String,
                   zip: String,
                   country: String)
