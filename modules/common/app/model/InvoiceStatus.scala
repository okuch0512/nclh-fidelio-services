package modules.fidelio_io.model

import play.api.libs.json.JsValue

import scala.util.Try

/**
  * Created by matt on 1/3/17.
  */
case class InvoiceStatus(isPaperless: Boolean, email: String)

object InvoiceStatus {
  def fromJson(js: JsValue): InvoiceStatus = {

    val isPaperlessText =
      Try((js \ "RES_NOBATCH_INVOICE").as[String]).getOrElse("0")

    val isPaperlessBool = if (isPaperlessText == "1") {
      true
    } else {
      false
    }

    val email = Try((js \ "RES_EMAIL_SHIP").as[String]).getOrElse("")

    InvoiceStatus(isPaperlessBool, email)
  }
}
