package modules.fidelio_io.model

/**
  * Created by matt on 1/10/17.
  */
object FidelioAgeGroup extends Enumeration {

  type FidelioAgeGroup = Value

  val ADULT = Value(0)
  val YOUNG_ADULT = Value(1)
  val MINOR = Value(2)

}
