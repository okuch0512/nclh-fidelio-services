package model

import java.util.Date

import ai.x.play.json.Jsonx

/**
  * See '93. Function: - FCUIGuestInquiry' in
  * 'FCWSFunction Technical Specification 7.30' document.
  *
  *  (All optional fields are optional at Fidelio too)
  */
case class FidelioGuestInfo(
    gnAccID: Int,
    geAccountType: Int,
    gbAllowPosting: Boolean,
    gsName: String,
    gsCabin: Option[String] = None,
    gdEmbDate: Date,
    gdDisDate: Date,
    gdBirthDate: Option[Date] = None,
    gsEMail: Option[String] = None,
    gnBalance: BigDecimal,
    gsFirstName: String,
    gsLastName: String,
    gsSalutation: String,
    gsTitle: String,
    gsGender: String,
    geAgeGroup: Int,
    gsLanguage: String,
    gsResStatus: String,
    gsGroup: Option[String] = None,
    gsGroupShoreID: Option[String] = None,
    gsCategory: Option[String] = None,
    gsAddress: Option[String] = None,
    gsStreet: Option[String] = None,
    gsCity: Option[String] = None,
    gsZip: Option[String] = None,
    gsState: Option[String] = None,
    gsCountry: Option[String] = None,
    gsTel: Option[String] = None,
    gnCreditLimit: Option[BigDecimal] = None,
    gsShipEMail: Option[String] = None,
    gsPin: Option[String] = None,
    gsAuthKey: Option[String] = None,
    gsBookNo: Option[String] = None,
    gsShoreID: Option[String] = None,
    gsVGuestID: Option[String] = None,
    gsAward: Option[String] = None,
    gsBoardCard: Option[String] = None,
    gnRoutedAcc: Option[String] = None,
    gbRoutedEnable: Option[Boolean] = None,
    gnRouteCreditLimit: Option[BigDecimal] = None,
    gsPassportCountry: Option[String] = None,
    gbyPicture: Option[Array[Byte]] = None,
    gsCabinStewardName: Option[String] = None,
    gsHandicap: String,
    gsHandicapRemark: String,
    gsMusterStation: String,
    gbOnboard: Boolean,
    gsSafetyNo: Option[String] = None,
    goSafetyMusterStation: Option[MusterStation] = None,
    goEmergencyMusterStation: Option[Seq[MusterStation]] = None,
    gbNoSafetyDrill: Option[Boolean] = None,
    gePicture: Int,
    gdLastPictureDate: Option[Date] = None,
    gsFreqCardNo: String,
    gsPriceCategory: String,
    gsCabinType: String,
    gnNoneRefundableCredit: Option[BigDecimal] = None,
    gnCurrentCruiseID: Option[Int] = None,
    gsPassportNo: Option[String] = None,
    gsPaymentDep: Option[String] = None,
    gsClassification: Option[String] = None,
    gdSysdate: Date,
    gdCurrentCruiseStartDate: Date,
    gnPGID: Int,
    gsCruiseItnID: String,
    gsResReference: String,
    gsDeck: Option[String] = None,
    gsExternalID: String
)

object FidelioGuestInfo {

  implicit lazy val musterStationFormat = Jsonx.formatCaseClass[MusterStation]
  implicit lazy val gurstInfoFormat = Jsonx.formatCaseClass[FidelioGuestInfo]

}

case class MusterStation(
    gsCode: String,
    gsName: String
)
