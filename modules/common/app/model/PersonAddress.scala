package modules.fidelio_io.model

import play.api.libs.json._

case class PersonAddress(street: String,
                         street2: String,
                         city: String,
                         zip: String,
                         state: String,
                         country: String)

object PersonAddress {
  implicit val addressReads: Reads[PersonAddress] = Json.reads[PersonAddress]

  implicit val addressWrites: Writes[PersonAddress] =
    new Writes[PersonAddress] {
      def writes(d: PersonAddress): JsValue = {
        Json.obj(
          "street" -> d.street,
          "street2" -> d.street2,
          "city" -> d.city,
          "zip" -> d.zip,
          "state" -> d.state,
          "country" -> d.country
        )
      }
    }
}
