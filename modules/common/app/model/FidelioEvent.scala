package modules.fidelio_io.model

import java.util.Date

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.util.DateUtil
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

/**
  * Created by matt on 1/6/17.
  */
case class FidelioEvent(id: Int,
                        eventNumber: String,
                        eventName: String,
                        startDate: Date,
                        endDate: Date,
                        price: Double,
                        priceCategoryCode: String,
                        personGroupCode: String,
                        seatsCurrentlyBooked: Int,
                        maximumSeats: Int,
                        showInITV: Boolean,
                        bookingStatus: String)

object FidelioEvent {

  implicit val fidelioEventReads: Reads[FidelioEvent] = Json.reads[FidelioEvent]

  implicit val fidelioEventWriter: Writes[FidelioEvent] = (
    (JsPath \ "evtid").write[Int] and
      (JsPath \ "evtno").write[String] and
      (JsPath \ "evtname").write[String] and
      (JsPath \ "evtbegdate").write[Date] and
      (JsPath \ "evtenddate").write[Date] and
      (JsPath \ "evtprice").write[Double] and
      (JsPath \ "evtpricecategory").write[String] and
      (JsPath \ "evtpersoncode").write[String] and
      (JsPath \ "evtbooked").write[Int] and
      (JsPath \ "evtmaxseat").write[Int] and
      (JsPath \ "evtshowinitv").write[Boolean] and
      (JsPath \ "evtstatus").write[String]
  )(unlift(FidelioEvent.unapply))

  def fromJson(lookup: JsLookupResult): Try[FidelioEvent] = {
    MessageUtil.jsonObjOpt(lookup) match {
      case Some(js) => fromJson(js)
      case _        => Failure(new Exception("Event not found"))
    }
  }

  def fromJson(js: JsValue): Try[FidelioEvent] = {
    val id = (js \ "evtid").as[Int]

    val eventNumber = (js \ "evtno").as[String].trim
    val eventName = (js \ "evtname").as[String].trim

    val strStartDate = (js \ "evtbegdate").as[BigDecimal].toString

    val startDate =
      DateUtil.parseDate(strStartDate, DateUtil.FORMAT_FIDELIO_DATE)

    val strEndDate = (js \ "evtenddate").as[BigDecimal].toString

    val endDate = DateUtil.parseDate(strEndDate, DateUtil.FORMAT_FIDELIO_DATE)

    val price = (js \ "evtprice").as[Double]

    val priceCategoryCode = (js \ "evtpricecategory").as[String].trim
    val personGroupCode = (js \ "evtpersoncode").as[String].trim

    val seatsCurrentlyBooked = (js \ "evtbooked").as[Int]
    val maximumSeats = (js \ "evtmaxseat").as[Int]

    val showInITV = (js \ "evtshowinitv").as[Boolean]
    val bookingStatus = (js \ "evtstatus").as[String].trim

    Success(
      FidelioEvent(
        id,
        eventNumber,
        eventName,
        startDate,
        endDate,
        price,
        priceCategoryCode,
        personGroupCode,
        seatsCurrentlyBooked,
        maximumSeats,
        showInITV,
        bookingStatus
      )
    )
  }
}
