package modules.fidelio_io.model

import java.text.SimpleDateFormat
import java.util.Date

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.util.DateUtil
import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}

/**
  * Created by matt on 1/5/17.
  */
case class FidelioEventFolio(items: Seq[FidelioEventFolioItem])

object FidelioEventFolio {

  private val logger = Logger(classOf[FidelioEventFolio])

  implicit val fidelioEventFolioWrites = Json.writes[FidelioEventFolio]

  def toJson(folio: FidelioEventFolio): JsValue = {
    JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          IndexedSeq(
            "EvtFolio" ->
              JsArray(folio.items.map(FidelioEventFolioItem.toJson))
          )
        )
      )
    )
  }

  def fromJson(js: JsValue, fidelioAcctId: Int): Try[FidelioEventFolio] = {
    val items = ArrayBuffer[FidelioEventFolioItem]()
    val errors = ArrayBuffer[Throwable]()

    MessageUtil.jsonArrayForEach(
      js \ "sTables" \ "EvtFolio", { (item: JsValue) =>
        val folioItem = FidelioEventFolioItem.fromJson(item, fidelioAcctId)

        folioItem match {
          case Success(folioItem) => items.append(folioItem)
          case Failure(ex) => {
            logger.error(
              s"Error occurred parsing event folio item for Fidelio account id ${fidelioAcctId}",
              ex
            )
            errors.append(ex)
          }
        }
      }
    )

    errors.headOption match {
      case Some(ex) => Failure(ex)
      case _        => Success(FidelioEventFolio(items))
    }
  }
}

case class FidelioEventFolioItem(id: Int,
                                 eventNumber: String,
                                 eventName: String,
                                 startDate: Date,
                                 endDate: Date,
                                 bookingValue: Double,
                                 numTicketsBooked: Int,
                                 fidelioAcctId: Int)

object FidelioEventFolioItem {

  implicit val fidelioEventFolioItemWriter: Writes[FidelioEventFolioItem] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "eventNumber").write[String] and
      (JsPath \ "eventName").write[String] and
      (JsPath \ "startDate").write[Date] and
      (JsPath \ "endDate").write[Date] and
      (JsPath \ "bookingValue").write[Double] and
      (JsPath \ "numTicketsBooked").write[Int] and
      (JsPath \ "fidelioAcctId").write[Int]
  )(unlift(FidelioEventFolioItem.unapply))

  def toJson(folio: FidelioEventFolioItem): JsValue = {
    JsObject(
      Seq(
        "evtid" -> JsString(folio.id.toString()),
        "evtno" -> JsString(folio.eventNumber),
        "evtname" -> JsString(folio.eventName),
        "evtbegdate" -> JsString(
          new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
            .format(folio.startDate)
        ),
        "evtenddate" -> JsString(
          new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
            .format(folio.endDate)
        ),
        "evttotal" -> JsString(folio.bookingValue.toString()),
        "evtqty" -> JsString(folio.numTicketsBooked.toString()),
        "pnAccID" -> JsString(folio.fidelioAcctId.toString())
      )
    )
  }

  def fromJson(js: JsValue, fidelioAcctId: Int): Try[FidelioEventFolioItem] = {
    val id = (js \ "evtid").as[String].trim.toInt

    val eventNumber = (js \ "evtno").as[String].trim
    val eventName = (js \ "evtname").as[String].trim

    val strStartDate = (js \ "evtbegdate").as[String].trim

    val startDate =
      DateUtil.parseDate(strStartDate, DateUtil.FORMAT_FIDELIO_DATE)

    val strEndDate = (js \ "evtenddate").as[String].trim

    val endDate = DateUtil.parseDate(strEndDate, DateUtil.FORMAT_FIDELIO_DATE)

    val bookingValue = (js \ "evttotal").as[String].trim.toDouble
    val numTicketsBooked = (js \ "evtqty").as[String].trim.toInt

    Success(
      FidelioEventFolioItem(
        id,
        eventNumber,
        eventName,
        startDate,
        endDate,
        bookingValue,
        numTicketsBooked,
        fidelioAcctId
      )
    )
  }
}
