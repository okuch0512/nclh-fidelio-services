package modules.fidelio_io.model

import java.text.SimpleDateFormat
import java.util.Date

import modules.fidelio_io.util.DateUtil
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsObject, JsValue, _}

import scala.util.{Success, Try}

/**
  * Created by matt on 12/23/16.
  */
case class FidelioCruise(id: Int,
                         shipName: String,
                         cruiseName: String,
                         itineraryId: Int,
                         onboardCurrency: String,
                         startDate: Date,
                         endDate: Date)

object FidelioCruise {
  implicit val fidelioCruiseWriter: Writes[FidelioCruise] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "shipName").write[String] and
      (JsPath \ "cruiseName").write[String] and
      (JsPath \ "itineraryId").write[Int] and
      (JsPath \ "onboardCurrency").write[String] and
      (JsPath \ "startDate").write[Date] and
      (JsPath \ "endDate").write[Date]
  )(unlift(FidelioCruise.unapply))

  /**
    * {
    *     "bSuccess": true,
    *     "nTotalPage": 0,
    *     "sCrewInvoice": "",
    *     "sErrCode": "",
    *     "sErrMsg": "",
    *     "sErrWord": "",
    *     "sObj": "",
    *     "sPackageInfo": "",
    *     "sTables": {
    *         "Table0": {
    *             "CRUISEENDDATE": "20190707000000",
    *             "CRUISENAME": "Bliss - Seattle to Seattle",
    *             "CRUISESTARTDATE": "20190630000000",
    *             "CURRENCYDEPARTMENTCODE": "990101",
    *             "ITINERARYID": "18190630",
    *             "ONBOARDCURRENCY": "US Dollar",
    *             "SHIPNAME": "Norwegian Bliss",
    *             "SYSTEMCRUISE": "1946",
    *             "SYSTEMDATE": "20190630000000"
    *         }
    *     }
    * }
    */
  def toJson(cruise: FidelioCruise): JsValue = {
    JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          Seq(
            "Table0" -> JsObject(
              Seq(
                "SYSTEMCRUISE" -> JsString(cruise.id.toString()),
                "SHIPNAME" -> JsString(cruise.shipName),
                "CRUISENAME" -> JsString(cruise.cruiseName),
                "ITINERARYID" -> JsString(cruise.itineraryId.toString()),
                "ONBOARDCURRENCY" -> JsString(cruise.onboardCurrency),
                "CRUISESTARTDATE" -> JsString(
                  new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
                    .format(cruise.startDate)
                ),
                "CRUISEENDDATE" -> JsString(
                  new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
                    .format(cruise.endDate)
                )
              )
            )
          )
        )
      )
    )
  }

  def fromJson(js: JsValue): Try[FidelioCruise] = {
    val table = (js \ "sTables" \ "Table0").as[JsObject]

    val id = (table \ "SYSTEMCRUISE").as[String].trim.toInt

    val shipName = (table \ "SHIPNAME").as[String].trim
    val cruiseName = (table \ "CRUISENAME").as[String].trim
    val itineraryId =
      (table \ "ITINERARYID").asOpt[String].getOrElse("0").trim.toInt

    val onboardCurrency = (table \ "ONBOARDCURRENCY").as[String].trim

    val strStartDate = (table \ "CRUISESTARTDATE").as[String].trim

    val startDate =
      DateUtil.parseDate(strStartDate, DateUtil.FORMAT_FIDELIO_DATE)

    val strEndDate = (table \ "CRUISEENDDATE").as[String].trim

    val endDate = DateUtil.parseDate(strEndDate, DateUtil.FORMAT_FIDELIO_DATE)

    Success(
      FidelioCruise(
        id,
        shipName,
        cruiseName,
        itineraryId,
        onboardCurrency,
        startDate,
        endDate
      )
    )
  }
}
