package modules.fidelio_io.model

import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsArray, JsObject, JsValue, _}

import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}

/**
  * Created by matt on 11/14/14.
  */
case class FidelioReservation(id: Int, // maps to RES_BOOKNR in Fidelio
                              clients: Seq[FidelioClientAccount])

object FidelioReservation {

  import FidelioClientAccount._

  private val logger = Logger(classOf[FidelioReservation])

  implicit val fidelioReservationWrites: Writes[FidelioReservation] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "clients").write[Seq[FidelioClientAccount]]
  )(unlift(FidelioReservation.unapply))

  def toJson(reservation: FidelioReservation): JsValue = {
    JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsArray(
          reservation.clients.map(Json.toJson[FidelioClientAccount](_))
        )
      )
    )
  }

  def fromJson(js: JsValue): Try[FidelioReservation] = {
    val clientAccounts = ArrayBuffer[FidelioClientAccount]()
    val errors = ArrayBuffer[Throwable]()

    val tables = (js \ "sTables").as[JsArray]

    tables.value.foreach { clientAccount =>
      {
        val client = FidelioClientAccount.fromJson(clientAccount.as[JsObject])

        client match {
          case Success(client) => clientAccounts.append(client)
          case Failure(ex) => {
            logger.error(s"Error occurred parsing client account", ex)
            errors.append(ex)
          }
        }
      }
    }

    if (clientAccounts.isEmpty) {
      Failure(new Exception("There are no valid clients in the reservation"))
    } else {
      errors.headOption match {
        case Some(ex) => Failure(ex)
        case _ =>
          Success(
            FidelioReservation(
              clientAccounts.head.reservationId,
              clientAccounts
            )
          )
      }
    }
  }
}
