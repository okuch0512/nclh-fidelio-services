package modules.fidelio_io.model

/**
  * This provides name information for a person.
  *

  * @param MiddleName  The middle name of the person name
  * @param SurnamePrefix  e.g &quot;van der&quot;, &quot;von&quot;, &quot;de&quot;
  * @param NameSuffix  Hold various name suffixes and letters (e.g. Jr., Sr., III, Ret., Esq.).
  * @param GivenName  Given name, first name or names
  * @param Surname  Family name, last name.
  * @param NameTitle  Degree or honors (e.g., Ph.D., M.D.)
  * @param NamePrefix  Salutation of honorific. (e.g., Mr. Mrs., Ms., Miss, Dr.)
 **/
case class PersonNameType(GivenName: Option[String] = None,
                          NamePrefix: Option[String] = None,
                          NameTitle: Option[String] = None,
                          MiddleName: Option[String] = None,
                          Surname: Option[String] = None,
                          SurnamePrefix: Option[String] = None,
                          NameSuffix: Option[String] = None)
