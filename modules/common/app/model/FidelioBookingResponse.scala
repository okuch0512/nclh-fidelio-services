package modules.fidelio_io.model

import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Success, Try}

/**
  * Created by matt on 1/28/15.
  */
case class FidelioBookingResponse(fidelioAccountId: Int,
                                  balance: Double,
                                  cruiseId: Int)

object FidelioBookingResponse {

  private val logger = Logger(classOf[FidelioBookingResponse])

  implicit val fidelioBookingResponseWriter: Writes[FidelioBookingResponse] = (
    (JsPath \ "accid").write[Int] and
      (JsPath \ "balance").write[Double] and
      (JsPath \ "cruiseid").write[Int]
  )(unlift(FidelioBookingResponse.unapply))

  def fromJson(js: JsValue): Try[FidelioBookingResponse] = {
    val table = (js \ "sTables" \ "ExcBooking").as[JsObject]

    val fidelioAccountId = (table \ "accid").as[Int]
    val balance = (table \ "balance").as[Double]
    val cruiseId = (table \ "cruiseid").as[Int]

    logger.debug("Parsing of FidelioBookingResponse was successful.")

    Success(FidelioBookingResponse(fidelioAccountId, balance, cruiseId))
  }
}
