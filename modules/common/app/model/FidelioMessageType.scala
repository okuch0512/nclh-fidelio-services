package modules.fidelio_io.model

/**
  * Created by matt on 1/4/17.
  */
object FidelioMessageType {

  val ADD_ROUTING = "AddRouting"
  val INQUIRE = "FCUIGuestInquiry"
  val EVENTS = "GetEvent"
  val EXCURSIONS = "GetExcursion"
  val GET_CRUISE_ITINERARY = "GetPortInfo"
  val EXCFOLIO = "GetExcFolio"
  val EVENTFOLIO = "GetEvtFolio"
  val SYSMSGREQ = "GetSinglePortPerDayCruiseInfo"
  val BOOKING = "ExcBooking"
  val EVENTBOOKING = "EvtBooking"
  val EXTEVENTS = "GetEvent"
  val FOLIO = "GetInvoiceTotal"
  val PACKAGE_FOLIO = "GetPackageInvoiceTotal"
  val WS_FUNCTION = "WSFunction"
  val LOGIN = "Login"
  val GENERIC_PURCHASE = "FCUIPosting"
  val GENERIC_PAYMENT = "FCUIPayment"
  val UPDATE_COMMENT = "UpdateComment"
  val DELETE_COMMENT = "DeleteComment"
  val UPDATE_INVOICE_STATUS = "UpdateInvoiceStatus"
  val GET_INVOICE_STATUS = "GetInvoiceStatus"
  val GUEST_PICTURE = "GuestPicture"
  val GUEST_KEY_CARD_SCAN = "GetGuestKeyByScan"
  val GUEST_KEY_CARD_FOLIO = "GetGuestKeyByFolio"
  val GUEST_AMENITY = "GetGuestAmenity"
}
