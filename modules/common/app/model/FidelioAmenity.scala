package model

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.model.{FidelioClientAccount, FidelioExcursion, FidelioReservation}
import modules.fidelio_io.model.FidelioReservation.logger
import modules.fidelio_io.util.DateUtil
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}

case class FidelioAmenity(qty: String,
                          code: String,
                          descr: String,
                          fiddescr: String,
                          amount: String,
                          deliverLocation: String,
                          dept: String,
                          deptname: String,
                          deliver: String)

object FidelioAmenity {

  implicit val amenityReads: Reads[FidelioAmenity] = (
    (JsPath \ "QTY").read[String] and
      (JsPath \ "CODE").read[String] and
      (JsPath \ "DESCR").read[String] and
      (JsPath \ "FIDDESCR").read[String] and
      (JsPath \ "AMOUNT").read[String] and
      (JsPath \ "DELIVERLOCATION").read[String] and
      (JsPath \ "DEPT").read[String] and
      (JsPath \ "DEPTNAME").read[String] and
      (JsPath \ "DELIVER").read[String]
    )(FidelioAmenity.apply _)

  implicit val amenityWriter: Writes[FidelioAmenity] = (
    (JsPath \ "QTY").write[String] and
    (JsPath \ "CODE").write[String] and
    (JsPath \ "DESCR").write[String] and
    (JsPath \ "FIDDESCR").write[String] and
    (JsPath \ "AMOUNT").write[String] and
    (JsPath \ "DELIVERLOCATION").write[String] and
    (JsPath \ "DEPT").write[String] and
    (JsPath \ "DEPTNAME").write[String] and
    (JsPath \ "DELIVER").write[String]
    )(unlift(FidelioAmenity.unapply))

  def fromJson(lookup: JsLookupResult): Try[FidelioAmenity] = {
    MessageUtil.jsonObjOpt(lookup) match {
      case Some(js) => fromJson(js)
      case _        => Failure(new Exception("Excursion not found"))
    }
  }

  def fromJson(js: JsValue): Try[FidelioAmenity] = {

    Success(
      FidelioAmenity(
        (js \ "QTY").as[String],
        (js \ "CODE").as[String],
        (js \ "DESCR").as[String],
        (js \ "FIDDESCR").as[String],
        (js \ "AMOUNT").as[String],
        (js \ "DELIVERLOCATION").as[String],
        (js \ "DEPT").as[String],
        (js \ "DEPTNAME").as[String],
        (js \ "DELIVER").as[String]
      )
    )
  }

}
