package modules.fidelio_io.model

/**
  * Created by matt on 2/23/16.
  */
object FidelioPostingType {

  val CREDIT = "C"
  val DEBIT = "D"
  val PACKAGE = "P"
}
