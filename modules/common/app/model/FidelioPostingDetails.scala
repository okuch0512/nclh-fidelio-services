package modules.fidelio_io.model

import play.api.libs.functional.syntax._
import play.api.libs.json._

/**
  * Created by matt on 5/22/17.
  */
case class FidelioPostingDetails(uniquePostingId: String,
                                 notes: String,
                                 totalAmount: Double,
                                 tip: Double,
                                 tax: Double,
                                 outletId: String,
                                 allowPosting: Boolean = false,
                                 discount: Double = 0,
                                 serviceCharge: Double = 0,
                                 image: String = "")

object FidelioPostingDetails {
  implicit val fidelioPostingDetailsWriter: Writes[FidelioPostingDetails] = (
    (JsPath \ "uniquePostingId").write[String] and
      (JsPath \ "notes").write[String] and
      (JsPath \ "totalAmount").write[Double] and
      (JsPath \ "tip").write[Double] and
      (JsPath \ "tax").write[Double] and
      (JsPath \ "outletId").write[String] and
      (JsPath \ "allowPosting").write[Boolean] and
      (JsPath \ "discount").write[Double] and
      (JsPath \ "serviceCharge").write[Double] and
      (JsPath \ "image").write[String]
  )(unlift(FidelioPostingDetails.unapply))

  def createPostingDetails(totalAmount: Double,
                           tip: Double,
                           tax: Double,
                           outletId: String,
                           deviceUnitName: String,
                           notes: String,
                           uniquePostingId: String,
                           allowPosting: Boolean,
                           image: String = ""): FidelioPostingDetails = {

    val normalizedPostingId = {
      if (uniquePostingId.isEmpty) {
        s"${System.currentTimeMillis}${deviceUnitName}"
      } else {
        uniquePostingId
      }
    }

    new FidelioPostingDetails(
      normalizedPostingId,
      notes,
      totalAmount,
      tip,
      tax,
      outletId,
      allowPosting,
      image = image
    )
  }
}
