package modules.fidelio_io.model

import java.util.Date

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.util.DateUtil
import play.api.Logger
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}

/**
  * Created by matt on 2/2/17.
  */
case class FidelioFolio(
    items: ArrayBuffer[FidelioFolioItem] = ArrayBuffer[FidelioFolioItem]()
)

object FidelioFolio {

  private val logger = Logger(classOf[FidelioFolio])

  implicit val fidelioFolioWrites = Json.writes[FidelioFolio]

  def fromJson(js: JsValue): Try[FidelioFolio] = {
    val folio = FidelioFolio()
    val errors = ArrayBuffer[Throwable]()

    MessageUtil.jsonArrayForEach(
      js \ "sTables" \ "Table0" \ "items", { (item: JsValue) =>
        val folioItem = FidelioFolioItem.fromJson(item)

        folioItem match {
          case Success(folioItem) =>
            folio.items.append(folioItem)
          case Failure(ex) => {
            logger.error(s"Error occurred parsing folio item", ex)
            errors.append(ex)
          }
        }
      }
    )

    errors.headOption match {
      case Some(ex) => Failure(ex)
      case _        => Success(folio)
    }
  }
}

case class FidelioFolioItem(systemPostingDate: Date,
                            department: String,
                            departmentType: String,
                            subdepartment: String,
                            amount: Double,
                            postingTransactionId: Int,
                            port: Option[String])

object FidelioFolioItem {
  implicit val fidelioFolioItemWriter: Writes[FidelioFolioItem] = (
    (JsPath \ "POS_SDAT").write[Date] and
      (JsPath \ "DEPARTMENT").write[String] and
      (JsPath \ "DEM_FLAG").write[String] and
      (JsPath \ "SUBDEPARTMENT").write[String] and
      (JsPath \ "TOTAL").write[Double] and
      (JsPath \ "POS_TRANS_ID").write[Int] and
      (JsPath \ "PORT").writeNullable[String]
  )(unlift(FidelioFolioItem.unapply))

  def fromJson(js: JsValue): Try[FidelioFolioItem] = {
    val strSystemPostingDate = (js \ "POS_SDAT").as[BigDecimal].toString()

    val systemPostingDate =
      DateUtil.parseDate(strSystemPostingDate, DateUtil.FORMAT_FIDELIO_DATE)

    val department = (js \ "DEPARTMENT").as[String].trim
    val departmentType = (js \ "DEM_FLAG").as[String].trim
    val subdepartment = (js \ "SUBDEPARTMENT").as[String].trim

    val amount = (js \ "TOTAL").as[Double]

//    val amount = if (strAmount.isEmpty) 0 else strAmount.toDouble

    val postingTransactionId = (js \ "POS_TRANS_ID").as[Int]

//    val postingTransactionId =
//      if (strPostingTransactionId.isEmpty) 0 else strPostingTransactionId.toInt

    val port = (js \ "PORT").asOpt[String]

    Success(
      FidelioFolioItem(
        systemPostingDate,
        department,
        departmentType,
        subdepartment,
        amount,
        postingTransactionId,
        port
      )
    )
  }
}
