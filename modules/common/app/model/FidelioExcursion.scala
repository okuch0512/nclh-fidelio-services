package modules.fidelio_io.model

import java.util.Date

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.util.DateUtil
import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.{JsLookupResult, JsPath, JsValue, Writes}

import scala.util.{Failure, Success, Try}

/**
  * Created by matt on 1/5/17.
  */
case class FidelioExcursion(id: Int,
                            excursionNumber: String,
                            excursionName: String,
                            startDate: Date,
                            endDate: Date,
                            adultPrice: Double,
                            childPrice: Double,
                            portName: String,
                            seatsCurrentlyBooked: Int,
                            maximumSeats: Int,
                            showInITV: Boolean,
                            bookingStatus: String)

object FidelioExcursion {

  implicit val fidelioCruiseWriter: Writes[FidelioExcursion] = (
    (JsPath \ "excid").write[Int] and
      (JsPath \ "excno").write[String] and
      (JsPath \ "excname").write[String] and
      (JsPath \ "excbegdate").write[Date] and
      (JsPath \ "excenddate").write[Date] and
      (JsPath \ "excadultprice").write[Double] and
      (JsPath \ "excchildprice").write[Double] and
      (JsPath \ "excport").write[String] and
      (JsPath \ "excbooked").write[Int] and
      (JsPath \ "excmaxseat").write[Int] and
      (JsPath \ "excshowinitv").write[Boolean] and
      (JsPath \ "excadultstatus").write[String]
  )(unlift(FidelioExcursion.unapply))

  def fromJson(lookup: JsLookupResult): Try[FidelioExcursion] = {
    MessageUtil.jsonObjOpt(lookup) match {
      case Some(js) => fromJson(js)
      case _        => Failure(new Exception("Excursion not found"))
    }
  }

  def fromJson(js: JsValue): Try[FidelioExcursion] = {
    val id = (js \ "excid").as[Int]

    val excursionNumber = (js \ "excno").as[String].trim
    val excursionName = (js \ "excname").as[String].trim

    val strStartDate = (js \ "excbegdate").as[BigDecimal].toString

    val startDate =
      DateUtil.parseDate(strStartDate, DateUtil.FORMAT_FIDELIO_DATE)

    val strEndDate = (js \ "excenddate").as[BigDecimal].toString

    val endDate = DateUtil.parseDate(strEndDate, DateUtil.FORMAT_FIDELIO_DATE)

    val adultPrice = (js \ "excadultprice").as[Double]
    val childPrice = (js \ "excchildprice").as[Double]

    val portName = (js \ "excport").asOpt[String].getOrElse("")

    val seatsCurrentlyBooked = (js \ "excbooked").as[Int]
    val maximumSeats = (js \ "excmaxseat").as[Int]

    val showInITV = (js \ "excshowinitv").as[Boolean]
    val bookingStatus = (js \ "excadultstatus").as[String].trim

    Success(
      FidelioExcursion(
        id,
        excursionNumber,
        excursionName,
        startDate,
        endDate,
        adultPrice,
        childPrice,
        portName,
        seatsCurrentlyBooked,
        maximumSeats,
        showInITV,
        bookingStatus
      )
    )
  }
}
