//package model
package modules.fidelio_io.model

import java.net.InetAddress
import java.nio.charset.StandardCharsets
import java.util.concurrent.atomic.AtomicInteger

import play.api.Logger

//import modules.fidelio_io.model.FidelioMessageUnit._

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.util.{Failure, Success, Try}

object FidelioMessage {
  private val logger = Logger(classOf[FidelioMessage])

  val SENDER_REFERENCE = InetAddress.getLocalHost.getHostAddress

  val STX: Byte = 0x02
  //Start transmission
  val ETX: Byte = 0x03
  //End transmission
  val US: Byte = 0x1F //Unit separator

  val rqnCounter = new AtomicInteger(0)

  /**
    * Gathers units from multiple messages and combines them into a single result.
    * This is useful when for example you have to query for clients that are in active or disembarked state
    * and combine them in a single place.
    *
    * @param messages
    * @param units
    * @return
    */
  def getAll(messages: Seq[Try[FidelioMessage]],
             units: Seq[String]): Seq[Map[String, String]] = {
    val finalResult = ArrayBuffer[Map[String, String]]()

    messages.foreach(
      result =>
        result match {
          case Success(msg) =>
            finalResult ++= msg.getAll(units)
          case Failure(e) =>
            logger.error(s"Excluding message from combining due to error", e)
        }
    )

    finalResult
  }

  /**
    * Generates a unique request number. This should be set as RQN of a request
    * message.
    *
    * @return
    */
  def generateRQN: String = {
    FidelioMessage.rqnCounter.addAndGet(1).toString
  }

  /**
    * Returns the checksum of a byte Seq of a Fidelio message.
    * Checksum is calculated for only up to the ETX byte. That means,
    * the Seq may already contain the checksum after the ETX byte.
    * Anything after the ETX byte is ignored. This way we
    * do not need to strip out the checksum value we receive from the response.
    *
    * @param msg
    * @return
    */
  def checksum(msg: Seq[Byte]): Byte = {
    var bFoundETX = false

    msg.foldLeft[Byte](0) { (acc, c) =>
      {
        if (!bFoundETX) {
          if (c == FidelioMessage.ETX) {
            bFoundETX = true
          }
          (acc ^ c).toByte
        } else {
          acc
        }
      }
    }
  }
}

class FidelioMessage() {
  var msgType: String = null
  var units: Map[String, String] = null

  def this(messageType: String, unitsMap: Map[String, String]) {
    this()

    msgType = messageType
    units = unitsMap
  }

  def this(response: Seq[Byte]) {
    this()

    val PARSE_STATE_INIT = 0
    val PARSE_STATE_TYPE = 1
    val PARSE_STATE_NAME = 2
    val PARSE_STATE_VALUE = 3
    val PARSE_STATE_END = 4
    var parseState = PARSE_STATE_INIT
    val buffer = ArrayBuffer[Byte]()
    val thisUnits = scala.collection.mutable.Map[String, String]()
    var name = ""
    var value = ""

    for (b <- response) {
      if (parseState == PARSE_STATE_INIT) {
        assert(b == FidelioMessage.STX, "Message must start with STX.")
        parseState = PARSE_STATE_TYPE
        buffer.clear
      } else if (parseState == PARSE_STATE_TYPE) {
        if (b == FidelioMessage.US) {
          //Done with message type string
          msgType = new String(buffer.toArray)
//                    logger.debug(s"Got message type: ${msgType}")

          buffer.clear
          parseState = PARSE_STATE_NAME
        } else {
          buffer.append(b)
        }
      } else if (parseState == PARSE_STATE_NAME) {
        if (b == '='.toByte) {
          //Done with the name
          name = new String(buffer.toArray)
//                    logger.trace(s"Got name: ${name}")
          buffer.clear
          parseState = PARSE_STATE_VALUE
        } else {
          buffer.append(b)
        }
      } else if (parseState == PARSE_STATE_VALUE) {
        if (b == FidelioMessage.US || b == FidelioMessage.ETX) {
          //Done with the value
          value = new String(buffer.toArray)
//                    logger.trace(s"Got value: ${value}")
          thisUnits(name) = value //Add the value
          buffer.clear

          if (b == FidelioMessage.ETX) {
            //End of values
            parseState = PARSE_STATE_END
          } else {
            //We have more values after this
            parseState = PARSE_STATE_NAME
          }

        } else {
          buffer.append(b)
        }
      }
    }

    units = thisUnits.toMap
  }

  def toByteArray: Array[Byte] = {
    val buffer = ArrayBuffer[Byte]()

    buffer.append(FidelioMessage.STX)
    buffer.appendAll(msgType.getBytes(StandardCharsets.US_ASCII))

    for ((name, value) <- units) {
      buffer.append(FidelioMessage.US)
      buffer.appendAll(s"${name}=${value}".getBytes(StandardCharsets.US_ASCII))
    }

    buffer.append(FidelioMessage.ETX)
    buffer.append(FidelioMessage.checksum(buffer))

    buffer.toArray
  }

  def get(key: String): Option[String] = units.get(key)

  def getOrElse(key: String, defaultValue: String): String =
    units.getOrElse(key, defaultValue)

  /**
    * Returns a list of unit values given a set of unit name keys. Some
    * units are repeated in a message with each unit name having a sequence
    * number appended to it. For example, "ACT1", "ACT2", "ACT3" and so on. This method takes
    * as input a list of unit names without the sequence number. For example "ACT", "ACI", "ENB".
    * For each sequence a map is created where keys are the unit name keys and values are the unit values.
    * Example return will be:
    *
    * Map("ACT" <- "ACT_VAL1", "ACI" <- "ACI_VAL1", "ENB" <- "ENB_VAL1"),
    * Map("ACT" <- "ACT_VAL2", "ACI" <- "ACI_VAL2", "ENB" <- "ENB_VAL2"),
    * Map("ACT" <- "ACT_VAL3", "ACI" <- "ACI_VAL3", "ENB" <- "ENB_VAL3"),
    *
    * It is expected that all keys will have the same number of values. If a key has a value missing
    * for a sequence this method stops there and discards any remaining values for the other keys.
    *
    * @param keys An array of unit names without the sequence number.
    * @return A list maps containing unit name values.
    */
  def getAll(keys: Seq[String]): Seq[Map[String, String]] = {
    val list = new ListBuffer[Map[String, String]]
    var counter = 1
    var hasValue = true

    /*
     * If there is only one value in the list Fidelio does not append the sequence number
     * to the unit name. Let's see if that is the case.
     */
    if (!units.get(keys(0)).isEmpty) {
      //There is only one item in the sequence. Return the units map as is.
      list.append(units)

      return list
    }

    while (hasValue) {
      val map = scala.collection.mutable.Map[String, String]()
      var missingValueCount = 0

      for (key <- keys if hasValue) {
        val value = units.get(s"${key}${counter}")

        value match {
          case Some(v) => map(key) = v
          case _       => missingValueCount += 1
        }
      }

      /*
       * Stop looking for values if none of the keys have any value.
       */
      hasValue = missingValueCount < keys.size

      if (hasValue) {
        list.append(map.toMap)
      }

      counter += 1
    }

    list

  }
}
