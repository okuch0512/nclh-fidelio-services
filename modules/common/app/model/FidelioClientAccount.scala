package modules.fidelio_io.model

import java.text.SimpleDateFormat
import java.util.Date

import modules.fidelio_io.util.DateUtil
import play.api.data.format.Formats.dateFormat
import play.api.data.format.Formatter
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Success, Try}

case class FidelioClientAccount(
    nclClientId: Int, // maps to UXP_A_FRQ_CARDNO in Fidelio
    name: PersonNameType,
    birthDate: Option[Date],
    nationality: String,
    gender: String,
    email: String,
    phone: String,
    address: Option[PersonAddress],
    latitudesLevel: Option[String],
    fidelioAcctId: Int, // aka Guest Id
    reservationId: Int,
    cabinNumber: String,
    deck: String,
    reservationStatus: String,
    minor: Boolean,
    embarkDate: Date,
    disembarkDate: Date,
    balance: Double,
    picture: Option[String]
)

object FidelioClientAccount {

  implicit val dateFormatWrites: Formatter[Date] = dateFormat(
    "dd/MM/yyyy HH:mm:ss"
  )

  implicit val personNameTypeWrites: Writes[PersonNameType] = (
    (JsPath \ "givenName").write[Option[String]] and
      (JsPath \ "namePrefix").write[Option[String]] and
      (JsPath \ "nameTitle").write[Option[String]] and
      (JsPath \ "middleName").write[Option[String]] and
      (JsPath \ "surname").write[Option[String]] and
      (JsPath \ "surnamePrefix").write[Option[String]] and
      (JsPath \ "nameSuffix").write[Option[String]]
  )(unlift(PersonNameType.unapply))

  implicit val personAddressWrites: Writes[PersonAddress] = (
    (JsPath \ "street").write[String] and
      (JsPath \ "street2").write[String] and
      (JsPath \ "city").write[String] and
      (JsPath \ "zip").write[String] and
      (JsPath \ "state").write[String] and
      (JsPath \ "country").write[String]
  )(unlift(PersonAddress.unapply))

  implicit val fidelioClientAccount: Writes[FidelioClientAccount] = (
    (JsPath \ "nclClientId").write[Int] and
      (JsPath \ "name").write[PersonNameType] and
      (JsPath \ "birthDate").write[Option[Date]] and
      (JsPath \ "nationality").write[String] and
      (JsPath \ "gender").write[String] and
      (JsPath \ "email").write[String] and
      (JsPath \ "phone").write[String] and
      (JsPath \ "address").write[Option[PersonAddress]] and
      (JsPath \ "latitudesLevel").write[Option[String]] and
      (JsPath \ "fidelioAcctId").write[Int] and
      (JsPath \ "reservationId").write[Int] and
      (JsPath \ "cabinNumber").write[String] and
      (JsPath \ "deck").write[String] and
      (JsPath \ "reservationStatus").write[String] and
      (JsPath \ "minor").write[Boolean] and
      (JsPath \ "embarkDate").write[Date] and
      (JsPath \ "disembarkDate").write[Date] and
      (JsPath \ "balance").write[Double] and
      (JsPath \ "picture").write[Option[String]]
  )(unlift(FidelioClientAccount.unapply))

  def toJson(account: FidelioClientAccount): JsValue = {
    JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsArray(
          IndexedSeq(
            JsObject(
              Seq(
                "gsFreqCardNo" -> JsString(account.nclClientId.toString),
                "gsFirstName" -> JsString(account.name.GivenName.get.toString),
                "gsLastName" -> JsString(account.name.NamePrefix.get.toString),
                "gsTitle" -> JsString(account.name.Surname.get.toString),
                "gdBirthDate" -> JsString(
                  new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
                    .format(account.birthDate.get)
                ),
                "gsPassportCountry" -> JsString(account.nationality),
                "gsGender" -> JsString(account.gender),
                "gsEMail" -> JsString(account.email),
                "gsAward" -> JsString(account.latitudesLevel.get),
                "gsTel" -> JsString(account.phone),
                "gsAddress" -> JsString(account.address.get.street),
                "gsStreet" -> JsString(account.address.get.street2),
                "gsCity" -> JsString(account.address.get.city),
                "gsState" -> JsString(account.address.get.state),
                "gsZip" -> JsString(account.address.get.zip),
                "gsCountry" -> JsString(account.address.get.country),
                "gnAccID" -> JsNumber(account.fidelioAcctId),
                "gsBookNo" -> JsString(account.reservationId.toString),
                "gsCabin" -> JsString(account.cabinNumber),
                "gsDeck" -> JsString(account.deck),
                "gsResStatus" -> JsString(account.reservationStatus),
                "geAgeGroup" -> JsNumber(0), //account.minor),
                "gdEmbDate" -> JsString(
                  new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
                    .format(account.embarkDate)
                ),
                "gdDisDate" -> JsString(
                  new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
                    .format(account.disembarkDate)
                ),
                "gnBalance" -> JsNumber(account.balance),
                "gbyPicture" -> JsString(account.picture.get.toString)
              )
            )
          )
        )
      )
    )
  }

  def fromJson(clientJS: JsValue): Try[FidelioClientAccount] = {
    val id = (clientJS \ "gsFreqCardNo").as[String].trim.toInt

    val fName = (clientJS \ "gsFirstName").as[String].trim
    val lName = (clientJS \ "gsLastName").as[String].trim
    val title = (clientJS \ "gsTitle").as[String].trim

    val strBirthDate = (clientJS \ "gdBirthDate").as[String].trim

    val birthDate =
      if (strBirthDate.isEmpty) {
        None
      } else {
        Some(DateUtil.parseDate(strBirthDate, DateUtil.FORMAT_FIDELIO_DATE))
      }

    val nationality = (clientJS \ "gsPassportCountry").as[String].trim
    val gender = (clientJS \ "gsGender").as[String].trim
    val email = (clientJS \ "gsEMail").as[String].trim
    val latitudesLevel = (clientJS \ "gsAward").as[String].trim

    val phone = (clientJS \ "gsTel").as[String].trim

    val street = (clientJS \ "gsAddress").as[String].trim
    val street2 = (clientJS \ "gsStreet").as[String].trim
    val city = (clientJS \ "gsCity").as[String].trim
    val state = (clientJS \ "gsState").as[String].trim
    val zip = (clientJS \ "gsZip").as[String].trim
    val country = (clientJS \ "gsCountry").as[String].trim

    var address: Option[PersonAddress] = None

    if (street.trim.isEmpty && street2.trim.isEmpty &&
        city.trim.isEmpty && state.trim.isEmpty &&
        zip.trim.isEmpty && country.trim.isEmpty) {
      address = None
    } else {
      address = Some(PersonAddress(street, street2, city, zip, state, country))
    }

    val fidelioAcctId = (clientJS \ "gnAccID").as[Int]
    val reservationId = (clientJS \ "gsBookNo").as[String].trim.toInt
    val cabinNumber = (clientJS \ "gsCabin").as[String].trim
    val deck = (clientJS \ "gsDeck").as[String].trim

    val reservationStatus = (clientJS \ "gsResStatus").as[String].trim

    val ageGroup = (clientJS \ "geAgeGroup").as[Int]

    val isMinor = (ageGroup == FidelioAgeGroup.MINOR.id)

    val strEmbarkDate = (clientJS \ "gdEmbDate").as[String].trim

    val embarkDate =
      DateUtil.parseDate(strEmbarkDate, DateUtil.FORMAT_FIDELIO_DATE)

    val strDisembarkDate = (clientJS \ "gdDisDate").as[String].trim

    val disembarkDate =
      DateUtil.parseDate(strDisembarkDate, DateUtil.FORMAT_FIDELIO_DATE)

    val balance = (clientJS \ "gnBalance").as[Double]

    val picture = (clientJS \ "gbyPicture").asOpt[String]

    Success(
      FidelioClientAccount(
        id,
        PersonNameType(
          Some(fName),
          None,
          Some(title),
          None,
          Some(lName),
          None,
          None
        ),
        birthDate,
        nationality,
        gender,
        email,
        phone,
        address,
        Option(latitudesLevel),
        fidelioAcctId,
        reservationId,
        cabinNumber,
        deck,
        reservationStatus,
        isMinor,
        embarkDate,
        disembarkDate,
        balance,
        picture
      )
    )
  }
}
