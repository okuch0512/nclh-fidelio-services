package modules.fidelio_io.model

/**
  * Created by matt on 1/28/15.
  */
object FidelioAccountStatus extends Enumeration {

  type FidelioAccountStatus = Value

  val RESERVATIONS = Value(0)
  val CHECKED_IN = Value(1)
  val ACTIVE = Value(2) // Active (Reservations and Checked-in)
  val DISEMBARKED = Value(3)
  val DISEMBARKED_RESIDENTS = Value(4) // Owners - valid only for ResidenSea
  val CHECKED_IN_AND_ALL_RESERVATIONS = Value(5) // Checked-in and all Reservation for the current cruise

}
