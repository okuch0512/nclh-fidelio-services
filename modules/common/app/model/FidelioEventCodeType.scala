package modules.fidelio_io.model

/**
  * Created by matt on 1/28/15.
  */
object FidelioEventCodeType extends Enumeration {

  type FidelioEventCodeType = Value

  val NONE = Value(0)
  val LOCATION_NAME = Value(1)
  val EVENT_NO = Value(2)
  val UNIQUE_ID = Value(3)

}
