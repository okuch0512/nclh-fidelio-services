package modules.fidelio_io.model

/**
  * Created by matt on 1/28/15.
  */
object FidelioExcursionCodeType extends Enumeration {

  type FidelioExcursionCodeType = Value

  val NONE = Value(0)
  val PORT_NAME = Value(1)
  val EXCURSION_NO = Value(2)
  val UNIQUE_ID = Value(3)
  val CATEGORY = Value(4)

}
