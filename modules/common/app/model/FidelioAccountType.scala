package modules.fidelio_io.model

/**
  * Created by matt on 1/28/15.
  */
object FidelioAccountType extends Enumeration {

  type FidelioAccountType = Value

  val ALL = Value(0)
  val PASSENGER = Value(1) // Passenger (Guest)
  val RESIDENT = Value(2) // Resident (Guest)
  val CREW_MEMBER = Value(3)
  val GROUP = Value(4)
  val VISITOR = Value(5)
  val SYSTEM_ACCOUNT = Value(6)
  val STAFF = Value(7)
  val GIFT_CARD = Value(8)

}
