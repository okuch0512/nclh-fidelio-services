package modules.fidelio_io.model

import java.text.SimpleDateFormat
import java.util.Date

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.util.DateUtil
import play.api.Logging
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}

/**
  * Created by matt on 1/5/17.
  */
case class FidelioExcursionFolio(items: Seq[FidelioExcursionFolioItem])

object FidelioExcursionFolio extends Logging {
  implicit val fidelioExcursionFolioWrites = Json.writes[FidelioExcursionFolio]

  def toJson(folio: FidelioExcursionFolio): JsValue = {
    JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          IndexedSeq(
            "ExcFolio" -> JsArray(
              folio.items.map(FidelioExcursionFolioItem.toJson(_))
            )
          )
        )
      )
    )
  }

  def fromJson(js: JsValue, fidelioAcctId: Int): Try[FidelioExcursionFolio] = {
    val items = ArrayBuffer[FidelioExcursionFolioItem]()
    val errors = ArrayBuffer[Throwable]()

    MessageUtil.jsonArrayForEach(
      js \ "sTables" \ "ExcFolio", { (item: JsValue) =>
        val folioItem = FidelioExcursionFolioItem.fromJson(item, fidelioAcctId)

        folioItem match {
          case Success(folioItem) => items.append(folioItem)
          case Failure(ex) => {
            logger.error(
              s"Error occurred parsing excursion folio item for Fidelio account id ${fidelioAcctId}",
              ex
            )
            errors.append(ex)
          }
        }
      }
    )

    errors.headOption match {
      case Some(ex) => Failure(ex)
      case _        => Success(FidelioExcursionFolio(items))
    }
  }

}

case class FidelioExcursionFolioItem(id: Int,
                                     excursionNumber: String,
                                     excursionName: String,
                                     startDate: Date,
                                     endDate: Date,
                                     bookingValue: Double,
                                     numTicketsBooked: Int,
                                     fidelioAcctId: Int)

object FidelioExcursionFolioItem {

  implicit val fidelioExcursionFolioItemWriter
      : Writes[FidelioExcursionFolioItem] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "excursionNumber").write[String] and
      (JsPath \ "excursionName").write[String] and
      (JsPath \ "startDate").write[Date] and
      (JsPath \ "endDate").write[Date] and
      (JsPath \ "bookingValue").write[Double] and
      (JsPath \ "numTicketsBooked").write[Int] and
      (JsPath \ "fidelioAcctId").write[Int]
  )(unlift(FidelioExcursionFolioItem.unapply))

  def toJson(folio: FidelioExcursionFolioItem): JsValue = {
    JsObject(
      Seq(
        "excid" -> JsString(folio.id.toString()),
        "excno" -> JsString(folio.excursionNumber),
        "excname" -> JsString(folio.excursionName),
        "excbegdate" -> JsString(
          new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
            .format(folio.startDate)
        ),
        "excenddate" -> JsString(
          new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
            .format(folio.endDate)
        ),
        "exctotal" -> JsString(folio.bookingValue.toString()),
        "excqty" -> JsString(folio.numTicketsBooked.toString())
      )
    )
  }

  def fromJson(js: JsValue,
               fidelioAcctId: Int): Try[FidelioExcursionFolioItem] = {
    val id = (js \ "excid").as[String].trim.toInt

    val excursionNumber = (js \ "excno").as[String].trim
    val excursionName = (js \ "excname").as[String].trim

    val strStartDate = (js \ "excbegdate").as[String].trim

    val startDate =
      DateUtil.parseDate(strStartDate, DateUtil.FORMAT_FIDELIO_DATE)

    val strEndDate = (js \ "excenddate").as[String].trim

    val endDate = DateUtil.parseDate(strEndDate, DateUtil.FORMAT_FIDELIO_DATE)

    val bookingValue = (js \ "exctotal").as[String].trim.toDouble
    val numTicketsBooked = (js \ "excqty").as[String].trim.toInt

    Success(
      FidelioExcursionFolioItem(
        id,
        excursionNumber,
        excursionName,
        startDate,
        endDate,
        bookingValue,
        numTicketsBooked,
        fidelioAcctId
      )
    )
  }
}
