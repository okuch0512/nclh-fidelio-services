package modules.fidelio_io.model

/**
  * Created by matt on 2/18/17.
  */
object FidelioQuickAssignment extends Enumeration {

  type FidelioQuickAssignment = Value

  val NONE = Value(0)
  val SAME_CABIN = Value(1)
  val SAME_BOOKING = Value(2)
  val SAME_GROUP = Value(3)

}
