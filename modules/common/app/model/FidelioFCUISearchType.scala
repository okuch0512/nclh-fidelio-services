package modules.fidelio_io.model

/**
  * Created by matt on 1/28/15.
  */
object FidelioFCUISearchType extends Enumeration {

  type FidelioFCUIPostingSearchType = Value

  val CARD_IDENTIFICATION_NUMBER = Value(0)
  val CABIN_NUMBER_OR_SYSTEM_ACCOUNT = Value(1)
  val NAME_OF_PERSON_OR_COMPANY_ACCOUNT = Value(2)
  val INTERNAL_IDENTIFICATION_NUMBER = Value(3)
  val GROUP_ID_NUMBER = Value(4)
  val EXTERNAL_IDENTIFICATION_NUMBER = Value(5)
  val PHONE_TRUNK_NUMBER = Value(6)
  val EMAIL_ID = Value(7)
  val INTERNAL_ID_NUMBER = Value(8)
  val GUEST_CATEGORY = Value(9)
  val RFID_UID = Value(10)

  //TODO:  Ask Fidelio to update the documentation, since it states that 11 is the value of the loyalty number
  val LOYALTY_NUMBER = Value(12) // Loyalty Number in Fidelio (i.e., UXP_A_FRQ_CARDNO) is same thing as Client Id in NCL

  //TODO: Find out from Fidelio what the correct values are for the MANNING_NUMBER_SAFETY_NUMBER and PREGENERATED_RANDOM_NUMBER constants
  val MANNING_NUMBER_SAFETY_NUMBER = Value(13)
  val PREGENERATED_RANDOM_NUMBER = Value(14)

}
