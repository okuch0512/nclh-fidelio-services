package modules.fidelio_io.model

import java.text.SimpleDateFormat
import java.util.Date

import modules.data_mapper.util.MessageUtil
import modules.fidelio_io.util.DateUtil
import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.collection.mutable.ArrayBuffer
import scala.util.{Success, Try}

/**
  * Created by matt on 1/3/17.
  */
case class FidelioItinerary(items: Seq[FidelioItineraryItem])

object FidelioItinerary {

  implicit val fidelioItineraryFolioWrites = Json.writes[FidelioItinerary]

  def fromJson(js: JsValue,
               startDate: Date,
               endDate: Date): Try[FidelioItinerary] = {
    val items = ArrayBuffer[FidelioItineraryItem]()

    val table = (js \ "sTables" \ "Table0").as[JsArray]

    table.value.foreach { json =>
      items.append(FidelioItineraryItem.fromJson(json))
    }

    val sortedItems = items.sortBy(_.date)

    val filteredItems = ArrayBuffer[FidelioItineraryItem]()

    sortedItems.foreach { item =>
      {
        if (!item.date.before(startDate) && !item.date.after(endDate)) {
          if (item.date.equals(startDate) && (MessageUtil.PORT_ATSEA
                .equalsIgnoreCase(item.arrivalPortCode))) {
            // sometimes there is an extra itinerary item for the first day of the cruise which is at sea
            // which we choose to ignore
          } else if (item.date.equals(endDate) && items.contains(item)) {
            // sometimes there is a duplicate itinerary item for the last day of the cruise
            // which we choose to ignore
          } else {
            filteredItems.append(item)
          }
        }
      }
    }

    Success(FidelioItinerary(filteredItems))
  }

  def toJson(itinerary: FidelioItineraryItem): JsValue = {
    JsObject(
      Seq(
        "bSuccess" -> JsBoolean(true),
        "sErrMsg" -> JsString(""),
        "sTables" -> JsObject(
          Seq(
            "Table0" -> JsObject(
              Seq(
                "TYP_COMMENT" -> JsString(itinerary.arrivalPortName),
                "SCD_PORT_ID" -> JsString(itinerary.arrivalPortCode),
                "SCD_DATE" -> JsString(
                  new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
                    .format(itinerary.date)
                )
              )
            )
          )
        )
      )
    )
  }

}

case class FidelioItineraryItem(arrivalPortName: String,
                                arrivalPortCode: String,
                                date: Date)

object FidelioItineraryItem {

  implicit val fidelioItineraryItemWrites: Writes[FidelioItineraryItem] = (
    (JsPath \ "TYP_COMMENT").write[String] and
      (JsPath \ "SCD_PORT_ID").write[String] and
      (JsPath \ "SCD_DATE").write[Date]
  )(unlift(FidelioItineraryItem.unapply))

  def fromJson(js: JsValue): FidelioItineraryItem = {
    val arrivalPortName = (js \ "TYP_COMMENT").as[String].trim
    val arrivalPortCode = (js \ "SCD_PORT_ID").as[String].trim

    val strDate = (js \ "SCD_DATE").as[String].trim
    val date = DateUtil.parseDate(strDate, DateUtil.FORMAT_FIDELIO_DATE)

    FidelioItineraryItem(arrivalPortName, arrivalPortCode, date)
  }

  def toJson(itineraryItem: FidelioItineraryItem): JsValue = {
    JsObject(
      Seq(
        "TYP_COMMENT" -> JsString(itineraryItem.arrivalPortName),
        "SCD_PORT_ID" -> JsString(itineraryItem.arrivalPortCode),
        "SCD_DATE" -> JsString(
          new SimpleDateFormat(DateUtil.FORMAT_FIDELIO_DATE)
            .format(itineraryItem.date)
        )
      )
    )
  }

}
