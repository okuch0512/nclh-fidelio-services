package modules.fidelio_io.model

/**
  * Created by matt on 12/22/15.
  *
  * Please refer to the Fidelio Cruise "Universal Interface Technical Specification" for
  * more details on the message units listed below.
  *
  */
object GuestType extends Enumeration {
  val CREW = "C"
  val PASSENGER = "P"
}

object FidelioMessageUnit extends Enumeration {
  val ACE = "ACE" // Account ID Entry Type
  val ACI = "ACI" // Account ID
  val ACS = "ACS" // Account Status
  val ACT = "ACT" // Account Type
  val ADD = "ADD" // Address
  val ADT = "ADT" // Arrival Date
  val APN = "APN" // Arrival Port Name
  val APT = "APT" // Arrival Port
  val ATM = "ATM" // Arrival Time in HHMM
  val AWD = "AWD" // Award Level
  val BAL = "BAL" // Balance
  val BID = "BID" // Booking ID
  val BDT = "BDT" // Excursions Starting Date or Event Starting Date or Booking Date and Time
  val BOI = "BOI" // Unique Excursion Booking ID or Unique Event Booking ID
  val BOQ = "BOQ" // Number of Tickets Booked
  val BOV = "BOV" // Booking Value
  val CAB = "CAB" // Cabin Number
  val CED = "CED" // Cruise End Date
  val COD = "COD" // Code
  val CSD = "CSD" // Current Cruise Start Date
  val CTR = "CTR" // Country Code
  val CTY = "CTY" // City
  val CUR = "CUR" // Seats or Adult Seats Currently Booked or Seats Currently Booked
  val DCT = "DCT" // Departure Country ISO Code
  val DDT = "DDT" // Departure Date
  val DEK = "DEK" // Deck
  val DIS = "DIS" // Disembark date
  val DOB = "DOB" // Date of Birth
  val DPN = "DPN" // Departure Port Name
  val DPT = "DPT" // Departure Port
  val DTE = "DTE" // Date and Time
  val DTM = "DTM" // Departure Time in HHMM
  val EDT = "EDT" // Ending Date and Time or Cruise Dnding Date
  val EID = "EID" // Excursion ID or Event ID
  val EML = "EML" // E-mail address
  val EMB = "EMB" // Embark date
  val ENB = "ENB" // Account Enabled
  val ENO = "ENO" // Excursion Number or Event Number
  val FID = "FID" // Failure ID
  val FLG = "FLG" // Allow posting even though guest has reached his credit limit or posting is disabled
  val FRQ = "FRQ" // Frequent Traveller Card Number
  val FST = "FST" // First Name
  val GND = "GND" // Gender
  val INF = "INF" // Notes or Event Description
  val IMG = "IMG" // Receipt as a string
  val LST = "LST" // Last Name
  val MIN = "MIN" // Minor
  val MAX = "MAX" // Maximum Seats or Maximum Adult Seats or Maximum Seats
  val NAM = "NAM" // Salutation, Forename and Surname or Excursion Name or Event Name
  val NAT = "NAT" // Passport Nationality
  val OTL = "OTL" // 1 Ticket per Person limit is enabled or not
  val PAD = "PAD" // Adult Price
  val PAT = "PAT" // Participant Account ID
  val PCA = "PCA" // Price
  val PCH = "PCH" // Child Price
  val PCT = "PCT" // Price Category or Price Category Code
  val PDT = "PDT" // Posting or Payment Fidelio System Date
  val PGT = "PGT" // Person Group Code
  val POC = "POC" // Service Charge Value
  val POI = "POI" // Unique Posting ID
  val POO = "POO" // Outlet ID
  val POT = "POT" // Tip Value
  val POV = "POV" // Posting Value
  val POX = "POX" // Tax Value
  val PRT = "PRT" // Port Name
  val PTI = "PTI" // Posting Transaction ID
  val QAD = "QAD" // Number of Adult Tickets
  val QCH = "QCH" // Number of Child Tickets
  val QTY = "QTY" // Quantity or Number of Tickets Booked
  val REF = "REF" // Sender Reference Number
  val ROI = "ROI" // Routed folio id
  val RQN = "RQN" // Sender Request Number
  val SCR = "SCR" // Current Cruise ID or Cruise Id & Name [Id–Description] or Cruise ID
  val SDT = "SDT" // Fidelio Cruise System Date or System Date or Starting Date and Time or Cruise Starting Date or Posting Sales Date (aka Posting or Payment of the Server date/time)
  val SHP = "SHP" // Ship’s Name
  val SIT = "SIT" // Show in ITV [External Booking System]
  val SRC = "SRC" // Booking Source
  val STA = "STA" // Reservation Status or Status or Event Status or Booking or Adult Booking Status
  val STR = "STR" // Street
  val STT = "STT" // State
  val TEL = "TEL" // Telephone Number
  val TTL = "TTL" // Title
  val TYP = "TYP" // Code Type
  val VCT = "VCT" // Credit Amount
  val VGD = "VGD" // Unique Guest ID
  val VDT = "VDT" // Debit Amount
  val ZIP = "ZIP" // ZIP Code
}
