package modules.fidelio_io.model

/**
  * Created by matt on 3/31/17.
  */
case class FidelioPaymentDetails(postingId: String,
                                 paymentAmount: Double,
                                 outletId: String,
                                 notes: String)

object FidelioPaymentDetails {

  def createPaymentDetails(totalAmount: Double,
                           outletId: String,
                           deviceUnitName: String,
                           notes: String): FidelioPaymentDetails = {
    val postingId = s"${System.currentTimeMillis}${deviceUnitName}"

    FidelioPaymentDetails(postingId, totalAmount, outletId, notes)
  }
}
