package modules.fidelio_io.service

import java.util.Date

import modules.common.Settings
import modules.common.core.api.communication.HTTPDriver
import modules.fidelio_io.model.FidelioMessageType
import modules.fidelio_io.util.FidelioServiceHelper
import play.api.Logger
import play.api.libs.json.JsArray

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class FidelioLoginService {}

/**
  * Created by matt on 11/13/14.
  */
object FidelioLoginService {

  private val logger = Logger(classOf[FidelioLoginService])

  var lastSessionIdRetrieval = 0L
  var sessionId: String = null

  def getSessionIdCached(implicit ec: ExecutionContext): Future[Try[String]] = {

    val currentDate = (new Date).getTime

    if ((currentDate - lastSessionIdRetrieval) > (Settings.fidelioTokenTimeout * 1000)) {
      logger.debug("Session ID not there or has expired. Getting a new one.")

      /*
                This block of code may be called in parallel by multiple threads,
                but we don't anticipate any problem.  This is because getting a new session id
                does not invalidate the old session id.
       */
      getSessionId
    } else {
      if (sessionId == null) {
        Future(
          Failure(new Exception("Session Id cache is in an invalid state"))
        )
      } else {
        logger.debug("Using cached session Id: " + sessionId)

        Future(Success(sessionId))
      }
    }
  }

  def getSessionId(implicit ec: ExecutionContext): Future[Try[String]] = {

    val baseUrl = Settings.fidelioURL + "/FidelioSPMSWSJsonGet"

    val psFunction = FidelioMessageType.LOGIN
    val psSessionId = ""

    /**
      *  Fidelio Parameters:
      *  1. psLoginName (User Login Name)
      *  2. psPassword (MD5 Hashed Password converted to hex)
      */
    val psParam = Array[Any](Settings.fidelioUsername, Settings.fidelioPassword)

    val url =
      FidelioServiceHelper.buildUrl(baseUrl, psFunction, psParam, psSessionId)

    logger.debug(s"Retrieving sessionId using url: ${url}")

    HTTPDriver.getJSON(url).map {
      _ match {
        case Success(js) => {
          FidelioServiceHelper.validate(js) match {
            case None => {
              val tables = (js \ "sTables").as[JsArray]
              sessionId = tables(0).as[String]
              lastSessionIdRetrieval = (new Date).getTime
              Success(sessionId)
            }
            case Some(ex) => Failure(ex)
          }
        }
        case Failure(ex) => Failure(ex)
      }
    }
  }
}
